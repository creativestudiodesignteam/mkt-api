# API | Portal Maria

Portal Maria development api

## INSTALL THE PROEJECT

Commands for instalation the node_modules

# use

```bash
npm install
```
# OR
```bash
yarn install
```
# START THE PROJECT

```node.js
Yarn dev
```
# OR

```node.js
Npm start
```
# About Development


# Developers the project
# Create Migration for database

```node.js
npm sequelize migration:create --name=create-users

or

yarn sequelize migration:create --name=create-users
```

# Executing the migrations
```node.js
npx sequelize db:migrate

or

yarn sequelize db:migrate
```


[[BACK] Junior Rabelo](https://www.instagram.com/rabelo.junior105/)

[[BACK] Rodrigo Secco](https://www.instagram.com/__secco/)


## Contributing
Pull requests are not welcome. For major changes, open a problem first to discuss what you would like to change.

Update the tests as appropriate.

## License
[CREATIVE](https://www.creativedd.com.br/)


