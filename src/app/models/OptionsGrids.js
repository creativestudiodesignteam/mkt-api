import Sequelize, { Model } from 'sequelize';

class OptionsGrids extends Model {
  static init(sequelize) {
    super.init(
      {
        status: Sequelize.BOOLEAN,
        default: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
    this.belongsTo(models.Options, {
      foreignKey: 'fk_options',
      as: 'options',
    });
    this.belongsTo(models.OptionsNames, {
      foreignKey: 'fk_options_names',
      as: 'options_names',
    });
    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });
  }
}


export default OptionsGrids;
