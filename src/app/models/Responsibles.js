import Sequelize, { Model } from 'sequelize';

class Responsibles extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        cpf: Sequelize.STRING,
        email: Sequelize.STRING,
        telephone: Sequelize.STRING,
        cargo: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'providers',
    });
  }
}

export default Responsibles;
