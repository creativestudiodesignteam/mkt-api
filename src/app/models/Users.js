import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class User extends Model {
	static init(sequelize) {
		super.init(
			{
				email: Sequelize.STRING,
				password: Sequelize.VIRTUAL,
				password_hash: Sequelize.STRING,
				reset_password: Sequelize.STRING,
				date_reset_password: Sequelize.STRING,
				status: Sequelize.BOOLEAN,
				type: Sequelize.BOOLEAN,
				token_email: Sequelize.STRING,
				activation_at: Sequelize.DATE,
				attempts: Sequelize.INTEGER,
				timeout: Sequelize.DATE
			},
			{
				sequelize
			}
		);
		this.addHook('beforeSave', async (user) => {
			if (user.password) {
				user.password_hash = await bcrypt.hash(user.password, 8);
			}
		});
		return this;
	}

	checkPassword(password) {
		return bcrypt.compare(password, this.password_hash);
	}
	static associate(models) {
		this.belongsTo(models.File, {
			foreignKey: 'fk_avatar',
			as: 'avatar'
		});
		this.hasOne(models.Addresses, {
			foreignKey: 'fk_users',
			as: 'addresses'
		});
	}
}

export default User;
