import Sequelize, { Model } from 'sequelize';

class Products extends Model {
  static init(sequelize) {
    super.init(
      {
        /* name description */
        name: Sequelize.STRING,
        description: Sequelize.STRING,
        brand: Sequelize.STRING,
        model: Sequelize.STRING,
        sku: Sequelize.STRING,
        condition: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'fk_thumb',
      as: 'file',
    });
    this.belongsTo(models.Details, {
      foreignKey: 'fk_details',
      as: 'details',
    });
    this.belongsTo(models.Categories, {
      foreignKey: 'fk_categories',
      as: 'categories',
    });
    /*     this.belongsTo(models.Subcategories, {
          foreignKey: 'fk_subcategories',
          as: 'subcategories',
        }); */
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}

export default Products;
