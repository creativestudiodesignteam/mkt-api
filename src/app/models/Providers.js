import Sequelize, { Model } from 'sequelize';
import Responsibles from './Responsibles';

class Providers extends Model {
  static init(sequelize) {

    super.init(
      {
        /* cnpj fantasy_name reason_social state_register county_register telephone site */
        cnpj: Sequelize.STRING,
        fantasy_name: Sequelize.STRING,
        reason_social: Sequelize.STRING,
        state_register: Sequelize.STRING,
        county_register: Sequelize.STRING,
        telephone_commercial: Sequelize.STRING,
        telephone_whatsapp: Sequelize.STRING,
        is_panel_restricted: Sequelize.BOOLEAN,
        token: Sequelize.STRING,
        idcode: Sequelize.INTEGER,
        status: Sequelize.BOOLEAN,
        data_fundacao: Sequelize.STRING,
        qty_funcionarios: Sequelize.INTEGER,
        site: Sequelize.STRING,
        modalidade: Sequelize.STRING
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users'
    });

    this.hasMany(models.ProviderRamos, {
      foreignKey: 'fk_provider',
      as: 'provider_ramos'
    });

    this.hasMany(models.Responsibles, {
      foreignKey: 'fk_provider',
      as: 'responsibles'
    });
  }
}

export default Providers;
