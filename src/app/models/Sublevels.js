import Sequelize, { Model } from 'sequelize';

class Sublevels extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Subcategories, {
      foreignKey: 'fk_subcategories',
      as: 'subcategories',
    });
  }
}

export default Sublevels;
