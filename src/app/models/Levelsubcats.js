import Sequelize, { Model } from 'sequelize';

class Levelsubcats extends Model {
  static init(sequelize) {
    super.init(
      {
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Sublevels, {
      foreignKey: 'fk_sublevels',
      as: 'sublevels',
    });
    this.belongsTo(models.Prodsubcats, {
      foreignKey: 'fk_prodsubcat',
      as: 'prodsubcat',
    });
  }
}


export default Levelsubcats;
