import Sequelize, { Model } from 'sequelize';

class CreditCards extends Model {
  static init(sequelize) {
    super.init(
      {
        token: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        first_digits: Sequelize.STRING,
        holder_name: Sequelize.STRING,
        selected: Sequelize.BOOLEAN,
        type: Sequelize.INTEGER
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}


export default CreditCards;
