import Sequelize, { Model } from 'sequelize';

class Addresses extends Model {
  static init(sequelize) {
    super.init(
      {
        /* name postcode state city neighborhood street number complement */
        name: Sequelize.STRING,
        postcode: Sequelize.STRING,
        state: Sequelize.STRING,
        city: Sequelize.STRING,
        neighborhood: Sequelize.STRING,
        street: Sequelize.STRING,
        number: Sequelize.STRING,
        complement: Sequelize.STRING,
        references: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        select_at: Sequelize.BOOLEAN,
        selected_store: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}

export default Addresses;
