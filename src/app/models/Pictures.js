import Sequelize, { Model } from 'sequelize';

class Pictures extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        path: Sequelize.STRING,
        url: {
          type: Sequelize.VIRTUAL,
          get() {
            return `http://localhost:3331/pictures/${this.path}`;
          },
        },
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
/*     this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    }); */
  }
}

export default Pictures;
