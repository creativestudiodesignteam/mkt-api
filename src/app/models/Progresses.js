import Sequelize, { Model } from 'sequelize';

class Progresses extends Model {
  static init(sequelize) {
    super.init(
      {
        type: Sequelize.STRING,
        situation: Sequelize.STRING,
        date: Sequelize.DATE,
        step: Sequelize.INTEGER,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Sales, {
      foreignKey: 'fk_sales',
      as: 'sales',
    });
  }
}

export default Progresses;
