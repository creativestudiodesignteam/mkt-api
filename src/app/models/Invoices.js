import Sequelize, { Model } from 'sequelize';
require('dotenv')

class Invoices extends Model {
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        path: Sequelize.STRING,
        url: {
          type: Sequelize.VIRTUAL,
          get() {
            return (process.env.API_URL + `invoices/${this.path}`);
          },
        },
        status: Sequelize.BOOLEAN,
      },

      {
        sequelize,
      }
    );
    return this;
  }
}

export default Invoices;
