import Sequelize, { Model } from 'sequelize';

class Details extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        length: Sequelize.STRING,
        height: Sequelize.STRING,
        width: Sequelize.STRING,
        weight_unit: Sequelize.STRING,
        weight: Sequelize.STRING,
        situation: Sequelize.STRING,
        type_packing: Sequelize.STRING,
        diameter: Sequelize.STRING,
        delivery_time: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

export default Details;
