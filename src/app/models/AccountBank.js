import Sequelize, { Model } from 'sequelize';

class AccountBank extends Model {
  static init(sequelize) {

    super.init(
      {
        bank_code: Sequelize.STRING,
        account_type: Sequelize.STRING,
        bank_agency: Sequelize.STRING,
        bank_agency_digit: Sequelize.STRING,
        bank_account: Sequelize.STRING,
        bank_account_digit: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_providers',
      as: 'providers',
    });
  }

}

export default AccountBank;
