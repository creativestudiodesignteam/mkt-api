import Sequelize, { Model } from 'sequelize';

class Galleries extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'fk_files',
      as: 'files',
    });
    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });
  }
}

export default Galleries;
