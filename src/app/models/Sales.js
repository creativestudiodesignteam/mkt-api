import Sequelize, { Model } from 'sequelize';

class Sales extends Model {
  static init(sequelize) {

    super.init(
      {
        hash: Sequelize.STRING,
        amount: Sequelize.INTEGER,
        quota: Sequelize.INTEGER,
        price: Sequelize.INTEGER,
        payment_type: Sequelize.INTEGER,
        status: Sequelize.BOOLEAN,
        created_at: Sequelize.DATE,
        canceled_at: Sequelize.DATE,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });

    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'providers',
    });
    this.belongsTo(models.Payments, {
      foreignKey: 'fk_payments',
      as: 'payments',
    });
    this.belongsTo(models.Fretes, {
      foreignKey: 'fk_fretes',
      as: 'fretes',
    });
    this.belongsTo(models.Invoices, {
      foreignKey: 'fk_invoices',
      as: 'invoices',
    });
    this.belongsTo(models.Addresses, {
      foreignKey: 'fk_addresses',
      as: 'addresses',
    });
    this.belongsTo(models.CreditCards, {
      foreignKey: 'fk_cards',
      as: 'creditcards',
    });
  }

}

export default Sales;
