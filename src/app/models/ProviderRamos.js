import Sequelize, { Model } from 'sequelize';

class ProviderRamos extends Model {
  static init(sequelize) {

    super.init(
      {
        /* cnpj fantasy_name reason_social state_register county_register telephone site */
        name: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'providers',
    });
  }
}

export default ProviderRamos;
