import Sequelize, { Model } from 'sequelize';

class Carts extends Model {
  static init(sequelize) {
    super.init(
      {
        amount: Sequelize.INTEGER,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });

    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'provider',
    });

    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });
  }
}

export default Carts;
