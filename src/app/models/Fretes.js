import Sequelize, { Model } from 'sequelize';

class Fretes extends Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        valor: Sequelize.FLOAT,
        tipo: Sequelize.STRING,
        cod_servico: Sequelize.STRING,
        prazo: Sequelize.STRING,
        status: Sequelize.BOOLEAN,
        tracking: Sequelize.STRING,
        name: Sequelize.STRING,
        postcode: Sequelize.STRING,
        state: Sequelize.STRING,
        city: Sequelize.STRING,
        neighborhood: Sequelize.STRING,
        street: Sequelize.STRING,
        number: Sequelize.STRING,
        complement: Sequelize.STRING,
        references: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });
  }
}

export default Fretes;
