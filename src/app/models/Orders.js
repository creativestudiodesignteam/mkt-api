import Sequelize, { Model } from 'sequelize';

class Orders extends Model {
  static init(sequelize) {
    super.init(
      {
        /* fk_grids fk_users fk_addresses */
        amount: Sequelize.INTEGER,
        price: Sequelize.DOUBLE,
        status: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
/*     this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    }); */
    this.belongsTo(models.Addresses, {
      foreignKey: 'fk_addresses',
      as: 'addresses',
    });

  }
}

export default Orders;
