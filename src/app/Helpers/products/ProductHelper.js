import Products from '../../models/Products';
import Files from '../../models/Files';
import Galleries from '../../models/Galleries';
import Grids from '../../models/Grids';
import OptionsGrids from '../../models/OptionsGrids';
import Options from '../../models/Options';
import OptionsNames from '../../models/OptionsNames';
import Categories from '../../models/Categories';
import Details from '../../models/Details';
import Datasheets from '../../models/Datasheets';

class ProductHelper {
  async galleryFunction(data) {
    const galleryAll = await Galleries.findAll({
      where: { fk_grids: data.fk_grids },
    });
    if (data.gallery.length === 0) {
      await Galleries.destroy({
        where: { fk_grids: data.fk_grids },
      });
    } else {
      const gridsGallery = data.gallery.map(async response => {
        const fileExist = await Files.findOne({
          where: { id: response.fk_files },
        });
        if (fileExist) {
          const galleriesCreate = await Galleries.create({
            fk_files: fileExist.id,
            fk_grids: data.fk_grids,
          });

          const destroyGallery = galleryAll.filter(
            g => g !== galleriesCreate.id
          );
          if (galleriesCreate.id !== parseInt(destroyGallery)) {
            const promiseDestroy = destroyGallery.map(async response => {
              await Galleries.destroy({
                where: { id: parseInt(response.id) },
              });
            });

            await Promise.all(promiseDestroy);
          }
        }
      });

      await Promise.all(gridsGallery);
    }
  }

  async OptionGrids(data) {
    var errors = [];
    if (data.fk_grids) {
      const OptionsGridsAll = await OptionsGrids.findAll({
        where: { fk_grids: data.fk_grids, status: true },
      });

      if (data.options_grids.length === 0) {
        await OptionsGrids.destroy({
          where: { fk_grids: data.fk_grids },
        });
      } else {
        const optionsGrids = data.options_grids.map(async response => {
          if (!response.id) {
            if (response.fk_options) {
              const optionsExists = await Options.findOne({
                where: { id: response.fk_options, status: true },
              });
              if (!optionsExists) {
                errors.push({
                  type: 'error_exists',
                  parameter_name: 'options',
                  message: 'Erro ao atualizar, o atributo do seu produto ',
                });
              }
            }
            if (response.fk_options_names) {
              const optionsNamesExists = await OptionsNames.findOne({
                where: {
                  id: response.fk_options_names,
                  fk_options: response.fk_options,
                  status: true,
                },
              });
              if (!optionsNamesExists) {
                errors.push({
                  type: 'error_exists',
                  parameter_name: 'options_name',
                  message: 'Erro ao atualizar, o atributo do seu produto ',
                });
              }
            }
            if (errors.length === 0) {
              const optionsGrids = await OptionsGrids.create({
                fk_options: response.fk_options,
                fk_options_names: response.fk_options_names,
                fk_grids: data.fk_grids,
                fk_users: data.fk_users,
              });

              const destroyOptionsGrids = OptionsGridsAll.filter(
                g => g !== optionsGrids.id
              );

              if (optionsGrids.id !== parseInt(destroyOptionsGrids)) {
                const promiseDestroy = OptionsGridsAll.map(async response => {
                  await OptionsGrids.destroy({
                    where: { id: parseInt(response.id) },
                  });
                });

                await Promise.all(promiseDestroy);
              }
            }
          }
        });
        await Promise.all(optionsGrids);
      }
    }
    if (errors.length >= 1) {
      return errors;
    }
  }
  async List(data) {
    console.log(data.id);
    const products = await Products.findOne({
      where: { id: data.id, status: true },
      attributes: [
        'id',
        'name',
        'description',
        'sku',
        'brand',
        'model',
        'condition',
        'status',
      ],
      include: [
        {
          model: Categories,
          as: 'categories',
          attributes: ['id', 'name', 'status'],
        },
        {
          model: Files,
          as: 'file',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
        {
          model: Details,
          as: 'details',
          attributes: [
            'id',
            'length',
            'height',
            'width',
            'weight',
            'situation',
            'type_packing',
            'delivery_time',
          ],
        },
      ],
    });

    const {
      id: idProd,
      name: nameProd,
      sku: skuProd,
      categories: categoriesProd,
      description: descriptionProd,
      brand: brandProd,
      model: modelProd,
      condition: conditionProd,
      status: statusProd,
    } = products;

    const dataSheet = await Datasheets.findAll({
      where: { fk_products: data.id, status: true },
      attributes: ['id', 'name', 'description', 'status'],
    });

    const grids = await Grids.findAll({
      where: { fk_products: data.id, status: true },
      attributes: ['id', 'amount', 'price', 'status'],
    });

    const promisesGrids = grids.map(async response => {
      const optionsGrids = await OptionsGrids.findAll({
        where: { fk_grids: response.id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: Options,
            as: 'options',
            attributes: ['id', 'name', 'type', 'status'],
          },
          {
            model: OptionsNames,
            as: 'options_names',
            attributes: ['id', 'name', 'type', 'status'],
          },
        ],
      });

      const gallery = await Galleries.findAll({
        where: { fk_grids: response.id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: Files,
            as: 'files',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
        ],
      });

      return { Grids: response, optionsGrids, gallery };
    });

    const GridsList = await Promise.all(promisesGrids);

    return {
      product: {
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        categories: categoriesProd,
        datasheets: dataSheet,
        GridsList,
      },
    };
  }
}

export default new ProductHelper();
