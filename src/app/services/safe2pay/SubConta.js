require('dotenv');
const safe2pay = require('safe2pay');
// safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY);

async function getSubConta(id) {
  const apiKey = safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY);
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = '';
  await MarketplaceRequest.Get(id)
    .then(function(result) {
      retorno = result;
    })
    .catch(function(error) {
      retorno = error;
    });

  return retorno;
}

async function deleteSubConta(id) {
  const apiKey = safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY);
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = '';
  await MarketplaceRequest.Delete(id)
    .then(function(result) {
      retorno = result;
    })
    .catch(function(error) {
      retorno = error;
    });

  return retorno;
}

async function createSubConta(merchant) {
  const apiKey = safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY);
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = '';
  await MarketplaceRequest.Add(merchant)
    .then(function(result) {
      retorno = result;
    })
    .catch(function(error) {
      retorno = error;
    });

  return retorno;
}

async function updateSubConta(merchant, id) {
  const apiKey = safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY);
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = '';
  await MarketplaceRequest.Update(merchant, id)
    .then(function(result) {
      retorno = result;
    })
    .catch(function(error) {
      retorno = error;
    });

  return retorno;
}

// exports.setSubConta = setSubConta;
exports.getSubConta = getSubConta;
exports.deleteSubConta = deleteSubConta;
exports.createSubConta = createSubConta;
exports.updateSubConta = updateSubConta;
