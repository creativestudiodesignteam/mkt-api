import 'dotenv';

const Correios = require('node-correios');
const { rastro } = require('rastrojs');
const correiosObj = new Correios();
const calcPrecoUrl = 'http://localhost:3336/files/CalcPrecoPrazo.xml';

async function rastreamento(codigo) {
  const track = await rastro.track(codigo);
  return track;
}

async function buscaCEP(cep) {
  let response;
  await correiosObj
    .consultaCEP(cep)
    .then(result => {
      response = result;
    })
    .catch(error => {
      response = error;
    });
  return response;
}

async function calcPrecoPrazoData(
  cod_servico,
  origem,
  destino,
  peso,
  largura,
  altura,
  comprimento,
  valor
) {
  let args = {
    nCdEmpresa: process.env.CORREIOS_COD_EMPRESA,
    sDsSenha: process.env.CORREIOS_SENHA,
    nCdServico: cod_servico,
    sCepOrigem: origem,
    sCepDestino: destino,
    nVlPeso: peso > 0.3 ? peso : 0.3,
    nCdFormato: 1,
    nVlComprimento: comprimento > 16 ? comprimento : 16,
    nVlAltura: altura > 2 ? altura : 2,
    nVlLargura: largura > 11 ? largura : 11,
    sCdMaoPropria: 'N',
    nVlValorDeclarado: (valor / 100).toFixed(2) > 20.05 ? valor / 100 : 20.5,
    sCdAvisoRecebimento: 'N',
    sDtCalculo: '',
  };

  let response;
  try {
        await correiosObj
        .calcPrecoPrazo(args)
        .then(result => {
          response = result[0];
        })
        .catch(error => {
          response = error;
        });
      return response;
    } catch (err){
      return [{
        Erro: true,
        MsgErro: 'Requisição a plataforma dos Correios levou tempo demais, tente novamente mais tarde.'
      }]
    }
  }


exports.calcPrecoPrazoData = calcPrecoPrazoData;
exports.buscaCEP = buscaCEP;
exports.rastreamento = rastreamento;
