import * as Yup from 'yup';
import Datasheets from '../models/Datasheets';
import Products from '../models/Products';


class DatasheetsController {
  async index(req, res) {
    const datasheets = await Datasheets.findAll({
      where: { fk_products: req.params.fk_products, status: true },
      attributes: ['id', 'name', 'description', 'status'],
    });

    return res.json(datasheets);
  }

  async store(req, res) {
    /* cpf telephone gender birthday */
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      description: Yup.string().required(),
    });
    const productsExits = await Products.findOne({ where: { id: req.params.fk_products } });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    if (!productsExits) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'products',
          'message': 'Produto não existe, tente novamente !',
        },
      });
    }
    req.body.fk_products = req.params.fk_products;
    const {
      id,
      name,
      description,
    } = await Datasheets.create(req.body);


    return res.json({
      id,
      name,
      description,
    });
  }

  async update(req, res) {
    const { id } = req.params;
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      description: Yup.string().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const DatasheetsExists = await Datasheets.findOne({
      where: { id, status: true },
    });
    if (!DatasheetsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'datasheets',
          'message': 'Ficha técnica não existe, tente novamente !',
        },
      });
    }

    const {
      name,
      description,
    } = await DatasheetsExists.update(req.body);

    return res.json({
      id,
      name,
      description,
    });
  }

  async delete(req, res) {
    const { id } = req.params;

    const DatasheetsExists = await Datasheets.findOne({
      where: { id, status: true },
    });

    if (!DatasheetsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'datasheets',
          'message': 'Ficha técnica não existe, tente novamente !',
        },
      });
    }

    await DatasheetsExists.destroy();

    return res.json({ msg: 'Ficha técnica deletada' });

  }
}

export default new DatasheetsController();
