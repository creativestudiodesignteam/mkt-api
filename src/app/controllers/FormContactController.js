import * as Yup from 'yup';
import FormContacts from '../models/FormContacts';

class FormContactController {
    async store(req, res) {
        try {
            const schema = Yup.object().shape({
                name: Yup.string().required('Por favor, preencha o campo de nome'),
                tel: Yup.string().required('Por favor, preencha o campo de telefone'),
                email: Yup.string().required('Por favor, preencha o campo de email'),
                message: Yup.string().required('Por favor, preencha o campo de mensagem'),
            });

            await schema.validate(req.body)

            const form_contact = await FormContacts.create(req.body);
            return res.json({ name: form_contact.name, tel: form_contact.tel, email: form_contact.email, message: form_contact.message });

        } catch ({ message, ...error }) {
            if (error.name === 'ValidationError') {
                return res.json({
                    type: error.type,
                    parameter_name: error.path,
                    error: error.errors.toString()
                })
            }
            return res.json({ error: message })

        }
    }
}

export default new FormContactController();