import * as Yup from 'yup';
import Sublevels from '../models/Sublevels';
/*import Files from '../models/Files'; */

class SublevelsController {
  async index(req, res) {
    const { fk_subcategories } = req.params
    const sublevels = await Sublevels.findAll({
      where: { status: true, fk_subcategories },
      attributes: ['id', 'name'],
    })

    return res.json(sublevels);
  }
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      fk_subcategories: Yup.number().integer().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }

    const {
      id,
      name,
    } = await Sublevels.create(req.body);


    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }
    const SublevelsExists = await Sublevels.findOne({
      where: { id, status: true },
    });
    if (!SublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevels",
          "message": "Sub categoria não existe."
        }
      })
    }
    const {
      name,
    } = await SublevelsExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params
    const SublevelsExists = await Sublevels.findOne({
      where: { id, status: true },
    });
    if (!SublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevels",
          "message": "Sub nivel não existe."
        }
      });
    }

    await SublevelsExists.destroy();

    return res.json({ msg: 'Sub nivel deletado' })

  }
}

export default new SublevelsController();
