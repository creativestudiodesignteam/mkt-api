import Invoices from '../models/Invoices'

class InvoiceController {
  async create(req, res) {
    try {
      const { filename: path, originalname: name } = req.file;

      const { id, name: namecreate, path: pathcreate, url } = await Invoices.create({
        name,
        path,
      })

      return res.json({ id, name: namecreate, path: pathcreate, url })
    } catch ({ error, message }) {
      console.log('error')
      return res.status(404).json(message)
    }
  }
}

export default new InvoiceController();
