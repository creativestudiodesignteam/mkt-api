import * as Yup from 'yup';
import Providers from '../models/Providers';
import Users from '../models/Users';
import Files from '../models/Files';
import { cnpj } from 'cpf-cnpj-validator';
import ProviderRamos from '../models/ProviderRamos';
const isValidateCnpj = cnpj;

class ProvidersController {
  async index(req, res) {
    const { type } = await Users.findByPk(req.userId);
    if (!type) {
      return res.status(401).json({ error: "is not permission access because is type user" })
    }
    const providers = await Providers.findOne({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'cnpj', 'fantasy_name', 'reason_social', 'state_register', 'county_register',
        'telephone_commercial', 'telephone_whatsapp', 'data_fundacao', 'qty_funcionarios', 'site', 'modalidade'],
      include: [
        {
          model: Users,
          as: 'users',
          attributes: ['id', 'email'],
          include: [
            {
              model: Files,
              as: 'avatar',
              attributes: ['id', 'path', 'url'],
            },
          ],
          model: ProviderRamos,
          as: 'provider_ramos',
          attributes: ['id', 'name']
        },
      ],
    })

    return res.json(providers);
  }
  async store(req, res) {
    /* cnpj fantasy_name reason_social state_register county_register   */

    const schema = Yup.object().shape({
      cnpj: Yup.string().required(),
      fantasy_name: Yup.string().required(),
      reason_social: Yup.string().required(),
      state_register: Yup.string().required(),
      county_register: Yup.string(),
      telephone_commercial: Yup.string(),
      telephone_whatsapp: Yup.string(),
      data_fundacao: Yup.string(),
      qty_funcionarios: Yup.string(),
      site: Yup.string(),
      modalidade: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }

    const validate = isValidateCnpj.isValid(req.body.cnpj);
    if (!validate) {
      return res.json({
        error: {
          "type": "invalid_parameter",
          "parameter_name": "cnpj",
          "message": "Cnpj inválido !"
        }
      })
    }
    req.body.cnpj = isValidateCnpj.format(req.body.cnpj);

    const cnpjExists = await Providers.findOne({
      where: { cnpj: req.body.cnpj },
    });

    if (cnpjExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "cnpj",
          "message": "CNPJ já existe."
        }
      });
    }

    /* cnpj fantasy_name reason_social state_register county_register */
    const {
      id,
      cnpj,
      fantasy_name,
      reason_social,
      state_register,
      county_register,
      telephone_commercial,
      telephone_whatsapp,
      data_fundacao,
      qty_funcionarios,
      site,
      modalidade
    } = await Providers.create(req.body);

    return res.json({
      id,
      cnpj,
      fantasy_name,
      reason_social,
      state_register,
      county_register,
      telephone_commercial,
      telephone_whatsapp,
      data_fundacao,
      qty_funcionarios,
      site,
      modalidade
    });
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      cnpj: Yup.string(),
      fantasy_name: Yup.string(),
      reason_social: Yup.string(),
      state_register: Yup.string(),
      county_register: Yup.string(),
      telephone_commercial: Yup.string(),
      telephone_whatsapp: Yup.string(),
      data_fundacao: Yup.string(),
      qty_funcionarios: Yup.string(),
      site: Yup.string(),
      modalidade: Yup.string(),
    });
    const providers = await Providers.findByPk(id);

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "cnpj",
          "message": "CNPJ já existe."
        }
      });
    }

    if (id !== Providers.id) {
      const ProvidersExists = await Providers.findOne({
        where: { id },
      });
      if (!ProvidersExists) {
        return res.json({
          error: {
            "type": "not_exists",
            "parameter_name": "provider",
            "message": "Fornecedor não existe, tente novamente !"
          }
        });
      }
    }
    if (req.body.cnpj) {
      return res.json({
        error: {
          "type": "not_authorized",
          "parameter_name": "cnpj",
          "message": "Você não tem autorização para atualizar seu cnpjs !"
        }
      })
    }

    const {
      cnpj,
      fantasy_name,
      reason_social,
      state_register,
      county_register,
      telephone_commercial,
      telephone_whatsapp,
      data_fundacao,
      qty_funcionarios,
      site,
      modalidade
    } = await providers.update(req.body);

    return res.json({
      id,
      cnpj,
      fantasy_name,
      reason_social,
      state_register,
      county_register,
      telephone_commercial,
      telephone_whatsapp,
      data_fundacao,
      qty_funcionarios,
      site,
      modalidade
    });
  }

  async delete(req, res) {
    const { id } = req.params;
    const ProvidersExists = await Providers.findOne({
      where: { id, status: true },
    });

    if (!ProvidersExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "provider",
          "message": "Fornecedor não existe."
        }
      });
    }
    const { fk_users } = ProvidersExists;
    const UsersExists = await Users.findOne({
      where: { id: fk_users, status: true },
    });
    if (!UsersExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "user",
          "message": "Usuário não existe."
        }
      });
    }

    await ProvidersExists.update({ status: false });
    await UsersExists.update({ status: false });


    return res.json({ msg: 'Usuário desabilitado' })

  }
}

export default new ProvidersController();
