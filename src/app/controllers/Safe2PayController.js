import Sales from '../models/Sales';
import Payments from '../models/Payments';
import Progresses from '../models/Progresses';
import { format } from 'date-fns'

import Queue from '../../lib/Queue';


class Safe2PayController {

  /**
   * 1	Pendente - Iniciado uma transação pelo comprador, porém até o momento o Safe2Pay não recebeu nenhuma informação sobre o pagamento.	Boleto e Bitcoin
   * 2	Processamento	- Esta transação está em processamento e em breve deve retornar o status final da situação do pagamento.	Todos
   * 3	Autorizado - A transação foi paga pelo comprador e o Safe2Pay já recebeu uma confirmação da instituição financeira responsável pelo processamento.	Todos
   * 5	Em disputa - Dentro do prazo de liberação da transação o comprador abriu uma disputa para fins de contestação da transação.	Cartão de Crédito
   * 6	Devolvido - A transação foi devolvida, assim o valor desta retornou para o comprador.	Cartão de Crédito
   * 7	Baixado	- O boleto bancário foi baixado automáticamente pela instituição financeira após 29 dias de seu vencimento.	Boleto
   * 8	Recusado - A transação foi recusada pela operadora do cartão de crédito.	Cartões de Crédito e Débito
   * 11	Liberado - A transação foi liberada por um usuário com perfil financeiro, entretanto até o momento não foi paga.	Boleto
   * 12	Em cancelamento - A transação está em cancelamento até o período de baixa	Boleto
   * 13	Chargeback - A transação não foi reconhecida pelo titular do cartão	Cartões de Crédito e Débito
   * 14	Pré-Autorizado - A transação foi pré-autorizada no cartão de crédito do cliente. É necessário realizar a captura para finalizar a cobrança.	Cartões de Crédito
   * @param req
   * @param res
   * @return boolean
   */
  async webhook(req, res) {
    let body = req.body;

    let status = body.TransactionStatus.Id;
    var paymentStatus = false

    if (status === 3) {
      paymentStatus = true
    }

    let payment = await Payments.create({
      code: body.IdTransaction,
      method: body.PaymentMethod.Id,
      references: 'safe2pay',
      situations: body.TransactionStatus.Name,
      status: paymentStatus,
      url: ''
    });

    const salesModel = await Sales.findAll({
      where: { hash: body.IdTransaction.toString() },
      attributes: ['id', 'hash', 'fk_users'],
      include: {
        model: 'Users',
        as: 'users',
        attributes: ['id', 'email', 'type']
      }
    });

    for (const element of salesModel) {
      let sale = await Sales.findOne({
        where: { id: element.id },
      });

      await sale.update({
        fk_payments: payment.id,
        status: paymentStatus
      });

      let name = '';
      if (userModel.type) {
        const providerModel = await Providers.findOne({
          where: { fk_users: element.fk_users },
          attributes: ['fantasy_name'],
        });

          name = providerModel.fantasy_name

      } else {
        const customerModel = await Customers.findOne({
          where: { fk_users: element.fk_users },
          attributes: ['name']
        });

          name = customerModel.name
      }

      if (paymentStatus === 3) {
        await Progresses.create({
          type: 'payment',
          situation: 'Pagamento Aprovado',
          date: Date.now(),
          step: 1,
          fk_sales: element.id,
          status: true
        })

        await Queue.add('OrderUpdateMail', {
          users: {
            name: name,
            email: element.users.email,
          },
          order: {
            order: element.hash,
            status: "teve o pagamento aprovado",
          },
        });

      } else if (paymentStatus === 8) {
        await Progresses.create({
          type: 'payment',
          situation: 'Pagamento Recusado',
          date: Date.now(),
          step: 1,
          fk_sales: element.id,
          status: false
        })

        await Queue.add('OrderUpdateMail', {
          users: {
            name: name,
            email: element.users.email,
          },
          order: {
            order: element.hash,
            status: "teve o pagamento recusado",
          },
        });

      }
    }

    // TODO - Notificar cliente sobre alteração no status do pedido


    return res.json(payment);
  };
}

export default new Safe2PayController();
