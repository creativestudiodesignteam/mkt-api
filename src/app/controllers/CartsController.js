import * as Yup from 'yup';
import Providers from '../models/Providers';
import Carts from '../models/Carts';
import Users from '../models/Users';
import Products from '../models/Products';
import Grids from '../models/Grids';
import Files from '../models/Files';
import Addresses from '../models/Addresses';

class CartsController {
  async index(req, res) {
    const carts = await Carts.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'amount', 'status'],
      include: [
        {
          model: Grids,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: Products,
              as: 'products',
              attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
              include: [
                {
                  model: Files,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
      ],
    });

    return res.json(carts);
  }

  async store(req, res) {
    req.body.fk_users = req.userId;

    const { fk_grids } = req.body;
    const schema = Yup.object().shape({
      amount: Yup.number()
        .integer()
        .required('Digite a quantidade da compra'),
    });

    await schema.validate(req.body).catch(function(err) {
      return res.status(400).json({
        error: {
          type: 'validation_invalid_user',
          parameter_name: 'yup',
          message: err.errors.toString(),
        },
      });
    });

    const usersExists = await Users.findOne({
      where: { id: req.userId, status: true },
    });

    if (!usersExists) {
      return res.status(400).json({
        error: {
          type: 'not_exists',
          parameter_name: 'user',
          message: 'Usuário inválido !',
        },
      });
    }

    const GridsExists = await Grids.findOne({
      where: { id: fk_grids, status: true },
      include: [
        {
          model: Users,
          as: 'user',
          attributes: ['id', 'email'],
        },
      ],
    });

    if (!GridsExists) {
      return res.status(400).json({
        error: {
          type: 'not_exists',
          parameter_name: 'grids',
          message: 'A versão do seu produto não existe ou está indisponivel !',
        },
      });
    }
    const GridsUserExist = await Grids.findOne({
      where: { id: fk_grids, fk_users: req.userId, status: true },
    });
    if (GridsUserExist) {
      return res.status(400).json({
        error: {
          type: 'not_permission',
          parameter_name: 'products',
          message: 'Você não pode adicionar seu próprio produto ao carrinho!',
        },
      });
    }

    const ProvidersList = await Providers.findOne({
      where: { fk_users: GridsExists.user.id, status: true },
    });

    req.body.fk_provider = ProvidersList.id;

    const CartsGridsExists = await Carts.findOne({
      where: { fk_grids, fk_users: req.userId, status: true },
    });

    if (CartsGridsExists) {
      if (req.body.amount > parseInt(GridsExists.amount)) {
        const { id } = await CartsGridsExists.update({
          amount: parseInt(GridsExists.amount),
        });

        return res.json(
          await Carts.findOne({
            where: { id, status: true },
            attributes: ['id', 'amount', 'status'],
            include: [
              {
                model: Grids,
                as: 'grids',
                attributes: ['id', 'status'],
                include: [
                  {
                    model: Products,
                    as: 'products',
                    attributes: [
                      'id',
                      'name',
                      'brand',
                      'model',
                      'sku',
                      'status',
                    ],
                    include: [
                      {
                        model: Files,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                      },
                    ],
                  },
                ],
              },
            ],
          })
        );
      } else {
        const { id } = await CartsGridsExists.update({
          amount: parseInt(req.body.amount) + parseInt(CartsGridsExists.amount),
        });
        return res.json(
          await Carts.findOne({
            where: { id, status: true },
            attributes: ['id', 'amount', 'status'],
            include: [
              {
                model: Grids,
                as: 'grids',
                attributes: ['id', 'amount', 'price', 'status'],
                include: [
                  {
                    model: Products,
                    as: 'products',
                    attributes: [
                      'id',
                      'name',
                      'brand',
                      'model',
                      'sku',
                      'status',
                    ],
                    include: [
                      {
                        model: Files,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                      },
                    ],
                  },
                ],
              },
            ],
          })
        );
      }
    }

    if (req.body.amount > parseInt(GridsExists.amount)) {
      const { id } = await CartsGridsExists.update({
        amount: parseInt(GridsExists.amount),
      });

      return res.json(
        await Carts.findOne({
          where: { id },
          attributes: ['id', 'amount', 'status'],
          include: [
            {
              model: Grids,
              as: 'grids',
              attributes: ['id', 'amount', 'price', 'status'],
              include: [
                {
                  model: Products,
                  as: 'products',
                  attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                  include: [
                    {
                      model: Files,
                      as: 'file',
                      attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                  ],
                },
              ],
            },
          ],
        })
      );
    }

    const { id } = await Carts.create(req.body);

    return res.json(
      await Carts.findOne({
        where: { id },
        attributes: ['id', 'amount', 'status'],
        include: [
          {
            model: Grids,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: Products,
                as: 'products',
                attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
        ],
      })
    );
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      amount: Yup.number().integer(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const CartsExists = await Carts.findOne({
      where: { id },
    });

    if (!CartsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'carts',
          message: 'Carrinho não existe, tente novamente !',
        },
      });
    }

    const GridsExists = await Grids.findOne({
      where: { id: CartsExists.fk_grids, status: true },
    });

    if (!GridsExists) {
      return res.json({
        error: {
          type: 'inválid_parameters',
          message: 'Grade do produto não existe, tente novamente.',
        },
      });
    }

    const amountGrids = await Grids.findOne({
      where: { id: GridsExists.id },
    });

    if (req.body.amount > parseInt(amountGrids.amount)) {
      return res.json(
        await CartsExists.update({
          amount: parseInt(amountGrids.amount),
        })
      );
    }

    await CartsExists.update(req.body);

    return res.json(
      await Carts.findOne({
        where: { id },
        attributes: ['id', 'amount', 'status'],
        include: [
          {
            model: Grids,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: Products,
                as: 'products',
                attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
        ],
      })
    );
  }

  async delete(req, res) {
    const { id } = req.params;

    const CartsExists = await Carts.findOne({
      where: { id, status: true },
    });

    if (!CartsExists) {
      return res.json({
        error: {
          type: 'inválid_parameters',
          message: 'Carrinho do produto não existe, tente novamente.',
        },
      });
    }

    await CartsExists.destroy();

    return res.json({ msg: 'Deletado com sucesso' });
  }

  async addressSelectedUpdate(req, res) {
    const { id } = req.params;
    const AddressExists = await Addresses.findOne({
      where: { fk_users: req.userId, select_at: true },
    });

    if (AddressExists) {
      await AddressExists.update({ select_at: false });
    }

    const NewAddressExists = await Addresses.findOne({
      where: { id: id, fk_users: req.userId, status: true },
    });

    if (NewAddressExists) {
      await NewAddressExists.update({ select_at: true });
      return res.json(NewAddressExists);
    } else {
      return res.json({
        error: {
          type: 'update_addresses',
          parameter_name: 'addresses',
          message: 'Erro ao atualizar o carrinho.',
        },
      });
    }
  }
}

export default new CartsController();
