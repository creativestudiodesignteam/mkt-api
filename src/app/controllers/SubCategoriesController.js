import * as Yup from 'yup';
import Subcategories from '../models/Subcategories';
import Users from '../models/Users';
/*import Files from '../models/Files'; */

class SubcategoriesController {
  async index(req, res) {
    const { fk_categories } = req.params
    const subcategories = await Subcategories.findAll({
      where: { status: true, fk_categories },
      order: [['name', 'asc']],
      attributes: ['id', 'name'],
    })

    return res.json(subcategories);
  }


  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      fk_categories: Yup.number().integer().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }

    const {
      id,
      name,
    } = await Subcategories.create(req.body);


    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }
    const SubcategoriesExists = await Subcategories.findOne({
      where: { id, status: true },
    });
    if (!SubcategoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub categoria não existe."
        }
      })
    }
    const {
      name,
    } = await SubcategoriesExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params
    const SubcategoriesExists = await Subcategories.findOne({
      where: { id, status: true },
    });
    if (!SubcategoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub categoria não existe."
        }
      });
    }

    await SubcategoriesExists.destroy();

    return res.json({ msg: 'Sub categoria deletada' })

  }
}

export default new SubcategoriesController();
