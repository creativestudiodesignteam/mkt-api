import * as Yup from 'yup';
import Categories from '../models/Categories';
import Products from '../models/Products';
import Files from '../models/Files';
import Grids from '../models/Grids';
import Subcategories from '../models/Subcategories';

import { fn, col } from 'sequelize';

class CategoriesController {
  async index(req, res) {
    const categories = await Categories.findAll({
      where: { status: true },
      order: [['name', 'asc']],
      attributes: ['id', 'name'],
    });

    return res.json(categories);
  }

  async findSubcategoriesByCategories(req, res) {
    try {
      const categories = await Categories.findAll({
        where: { status: true },
        order: [['name', 'asc']],
        attributes: ['id', 'name'],
      });

      const promisesGrids = categories.map(async response => {
        const subcategories = await Subcategories.findAll({
          where: { fk_categories: response.id, status: true },
          order: [['name', 'asc']],
          attributes: ['id', 'name'],
        });

        return { id: response.id, name: response.name, subcategories };
      });
      const all = await Promise.all(promisesGrids);

      return res.json(all);
    } catch ({ message, error }) {
      return res.json(message);
    }
  }

  async findProductByCategories(req, res) {
    try {
      const { fk_categories } = req.params;

      const categoriesExists = await Categories.findOne({
        where: { id: fk_categories, status: true },
      });

      if (!categoriesExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'categories',
            message: 'Categoria não existe, tente novamente !',
          },
        });
      }
      /*
            const { page = 1 } = req.query; */

      const product = await Products.findAll({
        /*         limit: 20, */
        order: ['created_at'],
        order: [['name', 'asc']],
        /*         offset: (page - 1) * 20, */
        where: { fk_categories, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'brand',
          'sku',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url'],
          },
        ],
      });

      if (!product) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'products in categories',
            message: 'Não existe produtos cadastrados nessa categoria !',
          },
        });
      }
      const promisesGrids = product.map(async response => {
        const gridsList = await Grids.findAll({
          where: { fk_products: response.id },
          attributes: [
            [fn('min', col('price')), 'minPrice'],
            [fn('max', col('price')), 'maxPrice'],
            [fn('sum', col('amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const { id, name } = await Categories.create(req.body);

    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const CategoriesExists = await Categories.findOne({
      where: { id, status: true },
    });
    if (!CategoriesExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'categories',
          message: 'Categoria não existe, tente novamente !',
        },
      });
    }
    const { name } = await CategoriesExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params;
    const CategoriesExists = await Categories.findOne({
      where: { id, status: true },
    });
    if (!CategoriesExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'categories',
          message: 'Categoria não existe, tente novamente !',
        },
      });
    }

    await CategoriesExists.update({ status: false });

    return res.json({ msg: 'Categories is disabled' });
  }
}

export default new CategoriesController();
