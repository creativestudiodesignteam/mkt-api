import CreditCards from '../models/CreditCards';
import * as Yup from 'yup';
import Carts from '../models/Carts';
import Grids from '../models/Grids';
import Addresses from '../models/Addresses';

const Payload = require('../services/safe2pay/Transaction');

class CardsController {
  async index(req, res) {
    const creditCards = await CreditCards.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: [
        'id',
        'status',
        'fk_users',
        'first_digits',
        'holder_name',
        'selected',
        'type',
      ],
    });

    return res.json(creditCards);
  }

  async find(req, res) {
    const { id } = req.params;
    const creditCards = await CreditCards.findOne({
      where: { id: id, fk_users: req.userId, status: true },
      attributes: [
        'id',
        'first_digits',
        'holder_name',
        'type',
      ],
    });

    return res.json(creditCards);
  }

  async post(req, res) {
    const schema = Yup.object().shape({
      card_holder: Yup.string().required(),
      card_number: Yup.string().required(),
      expiration_date: Yup.string().required(),
      security_code: Yup.string().required(),
      type: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    let cardNumber = req.body.card_number;
    let first4Digits = cardNumber.substring(0, 4);

    let type = parseInt(req.body.type);
    let holder = req.body.card_holder;
    let expiration_date = req.body.expiration_date;
    let security_code = req.body.security_code;

    let token = await Payload.generateToken(
      holder,
      cardNumber,
      expiration_date,
      security_code,
      type,
      first4Digits,
      req.userId
    );

    if (token.token_created === true) {
      return res.json(token.card);
    } else {
      let retorna;
      if (token.error) {
        retorna = {
          error: {
            type: 'save_card',
            parameter_name: 'credit_cards',
            message: token.error,
          },
        };
      } else {
        retorna = {
          error: {
            type: 'save_card',
            parameter_name: 'credit_cards',
            message: 'Erro ao salvar cartão, tente novamente',
          },
        };
      }
      return res.json(retorna);
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    const creditCards = await CreditCards.findOne({
      where: { id, status: true, fk_users: req.userId },
    });

    if (!creditCards) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'credit_cards',
          message: 'Cartão não existe, tente novamente!',
        },
      });
    }

    await creditCards.update({ status: false });

    return res.json({ msg: 'Cartão desabilitado' });
  }

  async cardSelectedUpdate(req, res) {
    const { id } = req.body;

    const CardExists = await CreditCards.findOne({
      where: { fk_users: req.userId, selected: true },
    });

    if (CardExists) {
      await CardExists.update({ selected: false });
    }

    const NewCardExists = await CreditCards.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (NewCardExists) {
      await NewCardExists.update({ selected: true });
      return res.json(NewCardExists);
    } else {
      return res.json({
        error: {
          type: 'update_credit_card',
          parameter_name: 'creditcard',
          message: 'Erro ao atualizar o carrinho.',
        },
      });
    }
  }

  async installments(req, res) {
    const cartModel = await Carts.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['fk_grids', 'amount'],
    });

    let totalPrice = 0;
    for (const element of cartModel) {
      const productsModel = await Grids.findOne({
        where: { id: element.fk_grids, status: true },
        attributes: ['id', 'fk_products', 'price'],
      });

      let productPrice =
        parseInt(productsModel.price) * parseInt(element.amount);
      totalPrice = totalPrice + productPrice;
    }
    let retorno = await Payload.getInstallments(totalPrice).then(data => {
      console.log('date----------', data);
    });

    // if (totalPrice > 0) {
    // } else {
    //   return {
    //     type: 'not_exists',
    //     parameter: 'price',
    //     error: 'Valor inválido, verifique seu carrinho.',
    //   };
    // }
    // console.log(retorno);
    return retorno;
  }
}

export default new CardsController();
