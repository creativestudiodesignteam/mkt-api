import Carts from '../models/Carts';
import Addresses from '../models/Addresses';
import Providers from '../models/Providers';
import * as Yup from 'yup';
import Fretes from '../models/Fretes';
import Details from '../models/Details';
import Products from '../models/Products';
import Grids from '../models/Grids';
import Sales from '../models/Sales';
import Progresses from '../models/Progresses';
import { Op } from 'sequelize';
import { CronJob } from 'cron';



import ProductHelper from '../Helpers/products/ProductHelper';
import { Model } from 'mongoose';

const Correios = require('../services/correios/Correios');
/**
 * 04014 = SEDEX à vista
 04065 = SEDEX à vista pagamento na entrega
 04510 = PAC à vista
 04707 = PAC à vista pagamento na entrega
 40169 = SEDEX12 ( à vista e a faturar)
 40215 = SEDEX 10 (à vista e a faturar)
 40290 = SEDEX Hoje Varejo
 * @type {string}
 */
class CorreiosController {
  
  async rastreamento(req, res) {
    const job = new CronJob('0 */30 * * * *', async function() {
        
      const SalesModel = await Sales.findAll({
        where: { status: true },
        attributes: ['id'],
        include: {
          model: Fretes,
          as: 'fretes',
          attributes: ['id', 'tipo', 'prazo', 'tracking'],
          where: {
            tracking: {
              [Op.ne]: null,
            },
          },
        },
      });

      if (!SalesModel) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'sales',
            message: 'Venda não existe.',
          },
        });
      }

      for (const [i, el] of SalesModel.entries()) {
         const ProgressesModel = await Progresses.findAll({
           where: { fk_sales: el.id, step: 3, status: true }
         });

         if (!ProgressesModel || ProgressesModel.length === 0){

          let rastreio = await Correios.rastreamento(el.fretes.tracking);

          if (rastreio[0].isDelivered) {

            await Progresses.create({
              type: 'shipping',
              situation: 'Pedido Entregue',
              date: Date.now(),
              step: 3,
              fk_sales: el.id,
              status: true,
            });

          }
        }

      }
    });

    job.start();

    return res.json([]);
  }

  async buscaCep(req, res) {
    const { cep } = req.params;
    let cepCorreios = await Correios.buscaCEP({ cep: cep });
    return res.json(cepCorreios);
  }

  async calculoPrazoProduto(req, res) {
    const schema = Yup.object().shape({
      fk_grids: Yup.string().required(),
      product: Yup.string().required(),
      postcode: Yup.string().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Preencheu todos os campos certinhos? Tente novamente',
        },
      });
    }
    const codServico = Array();
    codServico['sedex'] = '04014';
    codServico['pac'] = '04510';

    const ProductsModel = await Products.findOne({
      where: { id: req.body.product, status: true },
      attributes: ['id', 'fk_details', 'fk_users'],
      include: {
        model: Details,
        as: 'details',
        attributes: [
          'id',
          'weight',
          'length',
          'width',
          'height',
          'type_packing',
          'delivery_time',
        ],
      },
    });
    if (!ProductsModel) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'product',
          message: 'Produto não existe.',
        },
      });
    }
    const GridsModel = await Grids.findOne({
      where: {
        id: req.body.fk_grids,
        fk_products: req.body.product,
        status: true,
      },
      attributes: ['id', 'price'],
    });
    if (!GridsModel) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'fk_grids',
          message: 'Grids não existe.',
        },
      });
    }

    const AddressesModel = await Addresses.findOne({
      where: {
        fk_users: ProductsModel.fk_users,
        status: true,
        selected_store: true,
      },
      attributes: ['id', 'postcode'],
    });
    if (!AddressesModel) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'address',
          message: 'Endereço do fornecedor não existe.',
        },
      });
    }

    let retorno = Array();
    let cont = 0;

    for (const item in codServico) {
      let frete = await Correios.calcPrecoPrazoData(
        codServico[item],
        AddressesModel.postcode,
        req.body.postcode,
        ProductsModel.details.weight,
        ProductsModel.details.length,
        ProductsModel.details.height,
        ProductsModel.details.width,
        GridsModel.price
      );
      // TO-DO validar resposta
      if (!frete) {
        return res.json({
          error: {
            type: 'timeout',
            message:
              'Requisição a plataforma dos Correios levou tempo demais, tente novamente mais tarde.',
          },
        });
      }
      if (parseInt(frete.Erro) >= '0') {
        retorno[cont] = {
          type: item,
          shipping_time: frete.PrazoEntrega,
          price: (parseFloat(frete.Valor).toFixed(2) * 100),
        };
      } else {
        let errorMsg = 'Verifique seus dados e tente novamente.';
        if (frete.MsgErro) {
          errorMsg = frete.MsgErro;
        }
        if (retorno.length === 0) {
          return res.json({
            error: {
              type: 'shipping_error',
              parameter_name: 'shipping',
              message: errorMsg,
            },
          });
        }
      }
      cont++;
    }
    return res.json(retorno);
  }

  async calculoFrete(req, res) {
    const codServico = Array();
    codServico['sedex'] = '04014';
    codServico['pac'] = '04510';
    const cartModel = await Carts.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'amount', 'fk_grids', 'fk_provider', 'fk_users'],
    });
    if (!cartModel.length) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'cart',
          message: 'Carrinho não existe.',
        },
      });
    }

    const AddressUser = await Addresses.findOne({
      where: { fk_users: req.userId, select_at: true, status: true },
      attributes: ['postcode'],
    });
    if (!AddressUser) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'address',
          message: 'Usuário não possui cep cadastrado.',
        },
      });
    }

    let cep = AddressUser.postcode;
    let cepUsuario = cep.replace(/[^\d]+/g, '');

    let retorno = Array();
    let cont = 0;

    for (const [i, element] of cartModel.entries()) {
      const provider = await Providers.findOne({
        where: { id: element.fk_provider, status: true },
        attributes: ['fk_users'],
      });

      const addressProvider = await Addresses.findOne({
        where: { fk_users: provider.fk_users, selected_store: true, status: true },
        attributes: ['postcode'],
      });
      if (!addressProvider) {
        return res.json({
          error: {
            type: 'not_exists_parameter',
            parameter_name: 'address',
            message: 'Fornecedor não possui cep cadastrado.',
          },
        });
      }
      const GridsModel = await Grids.findOne({
        where: {
          id: element.fk_grids,
          status: true,
        },
        attributes: ['id', 'price', 'fk_products'],
      });
      if (!GridsModel) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'fk_grids',
            message: 'Grids não existe.',
          },
        });
      }

      const ProductsModel = await Products.findOne({
        where: { id: GridsModel.fk_products, status: true },
        attributes: ['id', 'fk_details', 'fk_users'],
        include: {
          model: Details,
          as: 'details',
          attributes: [
            'id',
            'weight',
            'length',
            'width',
            'height',
            'type_packing',
            'delivery_time',
          ],
        },
      });
      if (!ProductsModel) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'product',
            message: 'Produto não existe.',
          },
        });
      }

      let cep = addressProvider.postcode;
      let cepProvider = cep.replace(/[^\d]+/g, '');
      for (const item in codServico) {
        let frete = await Correios.calcPrecoPrazoData(
          codServico[item],
          cepProvider,
          cepUsuario,
          ProductsModel.details.weight,
          ProductsModel.details.length,
          ProductsModel.details.height,
          ProductsModel.details.width,
          GridsModel.price
        );
        // TO-DO validar resposta
        if (!frete) {
          return res.json({
            error: {
              type: 'timeout',
              message:
                'Requisição a plataforma dos Correios levou tempo demais, tente novamente mais tarde.',
            },
          });
        }
        if (parseInt(frete.Erro) >= '0') {
          retorno[cont] = {
            id: ProductsModel.id,
            type: item,
            shipping_time: frete.PrazoEntrega,
            price: (parseFloat(frete.Valor).toFixed(2) * 100),
          };
        } else {
          let errorMsg = 'Verifique seus dados e tente novamente.';
          if (frete.MsgErro) {
            errorMsg = frete.MsgErro;
          }
          if (retorno.length === 0) {
            return res.json({
              error: {
                type: 'shipping_error',
                parameter_name: 'shipping',
                message: errorMsg,
              },
            });
          }
        }
        cont++;
      }      
    }
    let result = [];

    retorno.forEach((hash => a => {
      if (!hash[a.type]) {
          hash[a.type] = { type: a.type, price: 0, shipping_time: 0 };
          result.push(hash[a.type]);
      }
      hash[a.type].price += a.price;
      hash[a.type].shipping_time = Math.max(hash[a.type].shipping_time, a.shipping_time);
  })(Object.create(null)));

    return res.json(result);
  }

  async storeFrete(req, res) {
    const schema = Yup.object().shape({
      price: Yup.string().required(),
      type: Yup.string().required(),
      shipping_time: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Preencheu todos os campos certinhos? Tente novamente',
        },
      });
    }


    Fretes.update(
      { status: false },
      { where: { fk_users: req.userId, status: true } }
    )

    const codServico = Array();
    codServico['sedex'] = '04014';
    codServico['pac'] = '04510';

    const {
      id,
      price,
      type,
      cod_service,
      shipping_time,
      fk_users,
      status,
    } = await Fretes.create({
      valor: req.body.price,
      tipo: req.body.type,
      cod_servico: codServico[req.body.type],
      prazo: req.body.shipping_time,
      fk_users: req.userId,
    });
    return res.json({
      id,
      price,
      type,
      cod_service,
      shipping_time,
      fk_users,
      status,
    });
  }
}

export default new CorreiosController();
