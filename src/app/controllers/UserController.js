import * as Yup from 'yup';

import User from '../models/Users';
import Customers from '../models/Customers';
import Providers from '../models/Providers';
import Addresses from '../models/Addresses';
import Files from '../models/Files';
import { cpf, cnpj } from 'cpf-cnpj-validator';
import Responsibles from '../models/Responsibles';
import ProviderRamos from '../models/ProviderRamos';
const isValidateCpf = cpf;
const isValidateCnpj = cnpj;

import { createHash } from 'crypto';

import Queue from '../../lib/Queue';

const Helper = require('../services/helper/Helper');
const safe2pay = require('safe2pay');
const SubContaSafe2pay = require('../services/safe2pay/SubConta');

class UserController {
  async checkEmail(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email('e-mail invalido')
        .required('E-mail é obrigatório!'),
    });

    await schema.validate(req.body).catch(function (err) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: err.errors.toString(),
        },
      });
    });

    const userExists = await User.findOne({ where: { email: req.body.email } });
    if (userExists) {
      return res.status(200).json({
        status: true,
      });
    }
    return res.status(400).json({
      status: false,
    });
  }

  async checkCNPJ(req, res) {
    const validateProvider = isValidateCnpj.isValid(req.body.cnpj);
    if (!validateProvider) {
      return res.status(400).json({
        error: {
          type: 'invalid_parameter',
          parameter_name: 'cnpj',
          message: 'Cnpj inválido!',
        },
      });
    }
    req.body.cnpj = isValidateCnpj.format(req.body.cnpj);

    const cnpjExists = await Providers.findOne({
      where: { cnpj: req.body.cnpj, status: true },
    });

    if (cnpjExists) {
      return res.status(400).json({
        success: {
          type: 'exists',
          status: false,
          message: 'CNPJ já existe.',
        },
      });
    }

    return res.json({
      success: {
        status: true,
        message: 'CNPJ disponível',
      },
    });
  }

  async index(req, res) {
    const userExists = await User.findOne({
      where: { id: req.userId, status: true },
      attributes: ['id', 'email', 'activation_at', 'status', 'type'],
    });
    let dataUsers;
    if (!userExists) {
      return res.status(404).json({ error: 'Usuario inexistente' });
    }

    if (!userExists.type) {
      dataUsers = await Customers.findOne({
        where: { fk_users: userExists.id },
        attributes: ['id', 'name', 'cpf', 'rg', 'telephone', 'birthday'],
        include: [
          {
            model: User,
            as: 'users',
            attributes: ['id', 'email'],
            include: [
              {
                model: Files,
                as: 'avatar',
                attributes: ['id', 'path', 'url'],
              },
            ],
          },
        ],
      });

      return res.json(dataUsers);
    } else {
      dataUsers = await Providers.findOne({
        where: { fk_users: userExists.id },
        attributes: [
          'id',
          'cnpj',
          'fantasy_name',
          'idcode',
          'reason_social',
          'state_register',
          'county_register',
          'telephone_commercial',
          'telephone_whatsapp',
          'data_fundacao',
          'qty_funcionarios',
          'site',
          'modalidade',
        ],
        include: [
          {
            model: User,
            as: 'users',
            attributes: ['id', 'email'],
            include: [
              {
                model: Files,
                as: 'avatar',
                attributes: ['id', 'path', 'url'],
              },
            ],
          },
          {
            model: Responsibles,
            as: 'responsibles',
            attributes: ['id', 'name', 'cpf', 'email', 'telephone', 'cargo'],
            where: { status: true },
          },
        ],
      });

      const providerRamos = await ProviderRamos.findAll({
        where: { fk_provider: dataUsers.id, status: true },
        attributes: ['id', 'name'],
      });

      const {
        id,
        cnpj,
        fantasy_name,
        idcode,
        reason_social,
        state_register,
        county_register,
        telephone_commercial,
        telephone_whatsapp,
        data_fundacao,
        qty_funcionarios,
        site,
        modalidade,
      } = dataUsers;

      return res.json({
        user: userExists,
        provider: {
          id,
          cnpj,
          fantasy_name,
          idcode,
          reason_social,
          state_register,
          county_register,
          telephone_commercial,
          telephone_whatsapp,
          data_fundacao,
          qty_funcionarios,
          site,
          modalidade,
          avatar: dataUsers.users.avatar,
          user: dataUsers.user,
          responsible: dataUsers.responsibles,
          provider_ramos: providerRamos,
        },
      });
    }
  }

  async store(req, res) {
    try {
      // ============= USER ==============
      const schema = Yup.object().shape({
        email: Yup.string().email().required('E-mail é obrigatório!'),
        password: Yup.string().required('Senha é obrigatória!').min(6),
        confirmPassword: Yup.string().when('password', (password, field) =>
          password
            ? field
              .required('Confirmação de senha é obrigatoria!')
              .oneOf([Yup.ref('password')], 'As senhas devem corresponder')
            : field
        ),
        type: Yup.boolean().required('Tipo de conta é obrigatório!'),
      });


      await schema.validate(req.body).catch(function (err) {
        throw {
          toError: function () {
            return {
              type: 'validation_invalid_user',
              parameter_name: 'yup',
              error: err.errors.toString(),
            };
          },
        };
      });
      req.body.email = req.body.email.toLowerCase()
      const userExists = await User.findOne({
        where: { email: req.body.email },
      });


      if (userExists) {
        return res.status(400).json({
          error: {
            type: 'already_exists',
            parameter_name: 'user',
            message: 'Usuário já existe, faça seu login!',
          },
        });
      }

      if (!req.body.type) {
        /* ===================CUSTOMER========================== */
        const schemaCustomers = Yup.object().shape({
          name: Yup.string().required('Por favor digite seu nome'),
          cpf: Yup.string().required('Por favor digite seu cpf'),
          rg: Yup.string(),
          telephone: Yup.string(),
          birthday: Yup.string(),
        });

        await schemaCustomers.validate(req.body.data).catch(function (err) {
          return res.json({
            error: {
              type: 'validation_invalid_user_customer',
              parameter_name: 'yup',
              message: err.errors.toString(),
            },
          });
        });

        const dateNow = `${Date.now()}`;

        const token_email = createHash('md5').update(dateNow).digest('hex');

        req.body.status = false;

        req.body.token_email = token_email;

        const createUsers = await User.create(req.body);

        req.body.data.fk_users = createUsers.id;

        if (req.body.data.addresses) {
          const schemaAddresses = Yup.object().shape({
            name: Yup.string().required('Nome do endereço é obrigatório!'),
            postcode: Yup.string().required('CEP é obrigatório!'),
            state: Yup.string().required('Estado do endereço é obrigatório!'),
            city: Yup.string().required('Cidade do endereço é obrigatória!'),
            neighborhood: Yup.string().required(
              'Bairro do endereço é obrigatório!'
            ),
            street: Yup.string().required('Rua do endereço é obrigatória!'),
            number: Yup.string().required('Número do endereço é obrigatório!'),
            complement: Yup.string(),
            references: Yup.string(),
          });

          await schemaAddresses
            .validate(req.body.data.addresses)
            .catch(function (err) {
              return res.json({
                error: {
                  type: 'validation_invalid_addresses',
                  parameter_name: 'yup',
                  message: err.errors.toString(),
                },
              });
            });
          req.body.data.addresses.fk_users = createUsers.id;
          req.body.data.addresses.select_at = true;
        }
        const validateCustomer = isValidateCpf.isValid(req.body.data.cpf);

        if (!validateCustomer) {
          return res.json({
            error: {
              type: 'invalid_parameter',
              parameter_name: 'cpf',
              message: 'Cpf inválido !',
            },
          });
        }

        req.body.data.cpf = isValidateCnpj.format(req.body.data.cpf);

        const cpfExists = await Customers.findOne({
          where: { cpf: req.body.data.cpf },
        });

        if (cpfExists) {
          return res.json({
            error: {
              type: 'exists',
              parameter_name: 'cpf',
              message: 'CPF já existe.',
            },
          });
        }
        if (req.body.data.addresses) {
          const addresses = await Addresses.create(req.body.data.addresses);
        }
        const customerCreate = await Customers.create(req.body.data);

        // realiza o disparo apenas se tudo for salvo e nada falhou
        await Queue.add('CustomerMail', {
          users: {
            name: customerCreate.name,
            email: createUsers.email,
          },
          config: {
            token: token_email,
          },
        });

        return res.json(
          await Customers.findOne({
            where: { id: customerCreate.id, status: true },
            attributes: [
              'id',
              'name',
              'cpf',
              'rg',
              'telephone',
              'birthday',
              'status',
            ],
            include: [
              {
                model: User,
                as: 'users',
                attributes: ['id', 'email'],
                include: [
                  {
                    model: Files,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url'],
                  },
                ],
              },
            ],
          })
        );
      } else {
        // ============= PROVIDER ==============
        const schemaProvider = Yup.object().shape({
          cnpj: Yup.string().required('CNPJ é obrigatório!'),
          county_register: Yup.string(),
          fantasy_name: Yup.string().required('Nome fantasia é obrigatório!'),
          reason_social: Yup.string().required('razão social é obrigatória!'),
          state_register: Yup.string(),
          telephone_commercial: Yup.string(),
          telephone_whatsapp: Yup.string(),
          data_fundacao: Yup.string(),
          qty_funcionarios: Yup.string(),
          site: Yup.string(),
          modalidade: Yup.string(),
        });

        await schemaProvider.validate(req.body.data).catch(function (err) {
          return res.json({
            error: {
              type: 'validation_invalid_provider',
              parameter_name: 'yup',
              message: err.errors.toString(),
            },
          });
        });

        const validateProvider = isValidateCnpj.isValid(req.body.data.cnpj);

        if (!validateProvider) {
          return res.json({
            error: {
              type: 'invalid_parameter',
              parameter_name: 'cnpj',
              message: 'Cnpj inválido!',
            },
          });
        }

        req.body.data.cnpj = isValidateCnpj.format(req.body.data.cnpj);

        const cnpjExists = await Providers.findOne({
          where: { cnpj: req.body.data.cnpj },
        });

        if (cnpjExists) {
          return res.json({
            error: {
              type: 'exists',
              parameter_name: 'cnpj',
              message: 'CNPJ já existe.',
            },
          });
        }

        // ============= ENDEREÇO ==============
        const schemaAddresses = Yup.object().shape({
          name: Yup.string().required('Nome do endereço é obrigatório!'),
          postcode: Yup.string().required('CEP é obrigatório!'),
          state: Yup.string().required('Estado do endereço é obrigatório!'),
          city: Yup.string().required('Cidade do endereço é obrigatória!'),
          neighborhood: Yup.string().required(
            'Bairro do endereço é obrigatório!'
          ),
          street: Yup.string().required('Rua do endereço é obrigatória!'),
          number: Yup.string().required('Número do endereço é obrigatório!'),
          complement: Yup.string(),
          references: Yup.string(),
        });

        await schemaAddresses
          .validate(req.body.data.addresses)
          .catch(function (err) {
            return res.json({
              error: {
                type: 'validation_invalid_addresses',
                parameter_name: 'yup',
                message: err.errors.toString(),
              },
            });
          });

        req.body.data.addresses.select_at = true;
        req.body.data.addresses.selected_store = true;

        // ============= RESPONSAVEL ==============
        const schemaResponsible = Yup.object().shape({
          name: Yup.string().required('Nome do responsável é obrigatório!'),
          cpf: Yup.string().required('CPF do responsável é obrigatório!'),
          email: Yup.string()
            .email()
            .required('E-mail do responsável é obrigatório!'),
          telephone: Yup.string().required(
            'Telefone do responsável é obrigatório!'
          ),
          cargo: Yup.string().required('Cargo do responsável é obrigatório!'),
        });

        await schemaResponsible
          .validate(req.body.data.responsible)
          .catch(function (err) {
            return res.json({
              error: {
                type: 'validation_invalid_responsible',
                parameter_name: 'yup',
                message: err.errors.toString(),
              },
            });
          });

        // ============= RAMOS ==============
        if (!req.body.data.ramos) {
          return res.json({
            error: {
              type: 'validation_invalid_ramos',
              parameter_name: 'yup',
              message: 'Informe ao menos 1 ramo de atuação',
            },
          });
        }

        // ============= BANCO ==============
        const schemaBank = Yup.object().shape({
          BankAgency: Yup.string().required('Agência bancária é obrigatória!'),
          BankAgencyDigit: Yup.string().required(
            'Dígito da agência bancária é obrigatória!'
          ),
          BankAccount: Yup.string().required('Conta bancária é obrigatória!'),
          BankAccountDigit: Yup.string().required(
            'Agência da conta bancária é obrigatória!'
          ),
        });

        await schemaBank.validate(req.body.data.BankData).catch(function (err) {
          return res.json({
            error: {
              type: 'validation_invalid_bank',
              parameter_name: 'yup',
              message: err.errors.toString(),
            },
          });
        });

        const schemaBankType = Yup.object().shape({
          Code: Yup.string().required('Tipo da conta bancária é obrigatória!'),
        });

        await schemaBankType
          .validate(req.body.data.BankData.AccountType)
          .catch(function (err) {
            return res.json({
              error: {
                type: 'validation_invalid_bank_account_type',
                parameter_name: 'yup',
                message: err.errors.toString(),
              },
            });
          });

        const schemaBankCode = Yup.object().shape({
          Code: Yup.string().required('Código do banco é obrigatório'),
        });
        await schemaBankCode
          .validate(req.body.data.BankData.Bank)
          .catch(function (err) {
            return res.json({
              error: {
                type: 'validation_invalid_bank_code',
                parameter_name: 'yup',
                message: err.errors.toString(),
              },
            });
          });
        const dateNow = `${Date.now()}`;

        const token_email = createHash('md5').update(dateNow).digest('hex');

        req.body.status = false;
        req.body.token_email = token_email;

        const createUsers = await User.create(req.body);

        req.body.data.fk_users = createUsers.id;
        req.body.data.addresses.fk_users = createUsers.id;

        const addresses = await Addresses.create(req.body.data.addresses);
        const providersCreate = await Providers.create(req.body.data);

        req.body.data.responsible.fk_provider = providersCreate.id;



        const responsible = await Responsibles.create(
          req.body.data.responsible
        );

        let ramos = req.body.data.ramos;

        await ramos.map(async function (valor) {
          await ProviderRamos.create({
            name: valor,
            status: true,
            fk_provider: providersCreate.id,
          });
        });

        /*  const MarketplaceRequest = safe2pay.api.MarketplaceRequest; */

        const data = {
          Name: providersCreate.fantasy_name,
          CommercialName: providersCreate.reason_social,
          Identity: providersCreate.cnpj,
          ResponsibleName: responsible.name,
          ResponsibleIdentity: responsible.cpf,
          Email: createUsers.email,
          BankData: {
            Bank: {
              Code: req.body.data.BankData.Bank.Code,
            },
            AccountType: {
              Code: req.body.data.BankData.AccountType.Code,
            },
            BankAgency: req.body.data.BankData.BankAgency,
            BankAgencyDigit: req.body.data.BankData.BankAgencyDigit,
            BankAccount: req.body.data.BankData.BankAccount,
            BankAccountDigit: req.body.data.BankData.BankAccountDigit,
          },
          Address: {
            ZipCode: addresses.postcode,
            Street: addresses.street,
            Number: addresses.number,
            Complement: addresses.complement,
            District: addresses.neighborhood,
            CityName: addresses.city,
            StateInitials: addresses.state,
            CountryName: 'Brasil',
          },
          MerchantSplit: [
            {
              PaymentMethodCode: '1',
              IsSubaccountTaxPayer: false,
              Taxes: [
                {
                  TaxTypeName: '1',
                  Tax: '15.00',
                },
              ],
            },
            {
              PaymentMethodCode: '3',
              IsSubaccountTaxPayer: false,
              Taxes: [
                {
                  TaxTypeName: '1',
                  Tax: '15.00',
                },
              ],
            },
            {
              PaymentMethodCode: '4',
              IsSubaccountTaxPayer: false,
              Taxes: [
                {
                  TaxTypeName: '1',
                  Tax: '15.00',
                },
              ],
            },
          ],
        };

        /* const SubAccount = await MarketplaceRequest.Add(data); */
        const SubAccount = await SubContaSafe2pay.createSubConta(data)
        const providerFindByPk = await Providers.findByPk(providersCreate.id);

        if (SubAccount.HasError || SubAccount.statusCode > 400) {
          // ============= REMOVE TUDO ==============
          createUsers.destroy();
          const providerRamos = await ProviderRamos.findAll({
            where: { fk_provider: providersCreate.id, status: true },
          })
          await providerRamos.map(async function (valor) {
            let providerDestroy = await ProviderRamos.findByPk(valor.id);
            await providerDestroy.destroy();
          });

          return res.json({
            error: {
              "type": "safe2pay",
              "parameter_name": "safe2pay - " + SubAccount.ErrorCode,
              "message": SubAccount.Error
            },
          });
        }

        await providerFindByPk.update({ idcode: SubAccount.ResponseDetail.Id, token: SubAccount.ResponseDetail.Token })
        console.log(SubAccount)
        const providerList = await Providers.findOne({
          where: { id: providersCreate.id },
          attributes: [
            'id',
            'cnpj',
            'fantasy_name',
            'reason_social',
            'state_register',
            'county_register',
            'telephone_commercial',
            'telephone_whatsapp',
            'data_fundacao',
            'qty_funcionarios',
            'site',
            'modalidade',
          ],
          include: [
            {
              model: User,
              as: 'users',
              attributes: ['id', 'email'],
              include: [
                {
                  model: Files,
                  as: 'avatar',
                  attributes: ['id', 'path', 'url'],
                },
              ],
            },
          ],
        });

        // realiza o disparo apenas se tudo for salvo e nada falhou
        await Queue.add('WelcomeMail', {
          users: {
            name: providerList.fantasy_name,
            email: createUsers.email,
          },
          config: {
            token: token_email,
            url: process.env.FRONT_URL,
          },
        });

        return res.json({ providerList });
      }
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async update(req, res) {
    /* ===================USER========================== */

    const user = await User.findByPk(req.userId);

    const schema = Yup.object().shape({
      email: Yup.string().email(),
      oldPassword: Yup.string().min(6),
      password: Yup.string()
        .min(6)
        .when('oldPassword', (oldPassword, field) =>
          oldPassword ? field.required('Digite sua senha antiga') : field
        ),
      confirmPassword: Yup.string().when('password', (password, field) =>
        password
          ? field
            .required('por favor, confirme sua nova senha')
            .oneOf([Yup.ref('password')], 'As senhas devem corresponder')
          : field
      ),
    });

    await schema.validate(req.body).catch(function (err) {
      return res.json({
        error: {
          type: 'validation_invalid_users',
          parameter_name: 'yup',
          message: err.errors.toString(),
        },
      });
    });

    if (req.body.password) {
      if (!(await user.checkPassword(req.body.oldPassword))) {
        return res.status(401).json({
          error: {
            type: 'incorrect_password',
            parameter_name: 'password',
            message: 'Senha incorreta.',
          },
        });
      } else {
        if (req.body.password !== req.body.confirmPassword) {
          return res.status(401).json({
            error: {
              type: 'incorrect_password',
              parameter_name: 'password',
              message: 'Senhas não conferem, tente novamente.',
            },
          });
        }
      }
    }

    if (!req.body.type) {
      if (req.body.data.cpf) {
        return res.json({
          error: {
            type: 'not_authorized',
            parameter_name: 'cpf',
            message: 'Você não tem autorização para atualizar seu cpf !',
          },
        });
      }

      const customer = await Customers.findOne({
        where: { fk_users: req.userId },
      });

      await user.update(req.body);
      await customer.update(req.body.data);

      return res.json({
        success: {
          status: true,
          message: 'Atualizado com sucesso!',
        },
      });
    } else {
      // =============PROVIDER ==============
      if (req.body.data.cnpj) {
        return res.json({
          error: {
            type: 'not_authorized',
            parameter_name: 'cnpj',
            message: 'Você não tem autorização para atualizar seu cnpj !',
          },
        });
      }

      await user.update(req.body);

      const provider = await Providers.findOne({
        where: { fk_users: user.id },
      });
      await provider.update(req.body.data);

      const responsible = await Responsibles.findOne({
        where: { fk_provider: provider.id },
      });

      await responsible.update({
        name: req.body.data.responsible.name,
        cpf: req.body.data.responsible.cpf,
        telephone: req.body.data.responsible.telephone,
        cargo: req.body.data.responsible.cargo,
      });

      const ramosModel = await ProviderRamos.findAll({
        where: { fk_provider: provider.id, status: true },
        attributes: ['id'],
      });

      for (const valor of ramosModel) {
        let ramo = await ProviderRamos.findByPk(valor.id);
        await ramo.update({ status: false });
      }

      let ramos = req.body.data.ramos;
      await ramos.map(async function (valor) {
        await ProviderRamos.create({
          name: valor,
          status: true,
          fk_provider: provider.id,
        });
      });
    }

    await user.update(req.body);

    return res.json({
      success: {
        status: true,
        message: 'Atualizado com sucesso!',
      },
    });
  }

  async activate(req, res) {
    try {
      const { token } = req.body;
      const user = await User.findOne({ where: { token_email: token } });

      if (user) {
        await user.update({
          status: true,
          activation_at: Date.now(),
          token_email: null,
        });
        return res.json({
          status: true,
          message: 'Conta ativada com sucesso',
        });
      }
      if (!user) {
        return res.json({
          error: {
            type: 'token',
            parameter_name: 'token_invalido',
            message: 'Token inválido.',
          },
        });
      }
    } catch (error) {
      return res.json({
        error: {
          type: 'error',
          parameter_name: 'erro_intern',
          message: 'Algo de inesperado aconteceu, tente novamente.',
        },
      });
    }
  }

  async resendTokenActivate(req, res) {
    let nameSend;
    const userExists = await User.findOne({
      where: { id: req.userId },
    });
    if (!userExists) {
      return res.status(400).json({
        error: {
          type: 'not_exists',
          parameter_name: 'user',
          message: 'Usuário não existe, efetue seu cadastro!',
        },
      });
    }
    if (userExists.activation_at) {
      return res.status(400).json({
        error: {
          type: 'activate_at_already',
          parameter_name: 'activate_at',
          message: 'Você já está ativo!',
        },
      });
    }

    if (!userExists.token_email) {
      const token_email = await bcrypt.hash(userExists.email, 6);
      const userEdit = await userExists.update({
        token_email,
      });
      if (userExists.type) {
        const { fantasy_name } = await Providers.findOne({
          where: { fk_users: userExists.id },
        });
        nameSend = fantasy_name;
      } else {
        const { name } = await Customers.findOne({
          where: { fk_users: userExists.id },
        });
        nameSend = name;
      }

      await Queue.add('WelcomeMail', {
        users: { name: nameSend, email: userExists.email },
        config: {
          token: token_email,
          url: process.env.FRONT_URL,
        },
      });
    }
    if (userExists.type) {
      const { fantasy_name } = await Providers.findOne({
        where: { fk_users: userExists.id },
      });
      nameSend = fantasy_name;
    } else {
      const { name } = await Customers.findOne({
        where: { fk_users: userExists.id },
      });
      nameSend = name;
    }

    await Queue.add('WelcomeMail', {
      users: { name: nameSend, email: userExists.email },
      config: {
        token_email: userExists.token_email,
        url: process.env.FRONT_URL,
      },
    });

    return res.json({ message: 'Verifique seu email, para ativação da conta' });
  }
}

export default new UserController();
