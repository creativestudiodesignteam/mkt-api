import * as Yup from 'yup';
import Products from '../models/Products';
import Users from '../models/Users';
import Categories from '../models/Categories';
import Subcategories from '../models/Subcategories';
import Details from '../models/Details';
import Prodsubcats from '../models/Prodsubcats';
import Grids from '../models/Grids';
import Datasheets from '../models/Datasheets';
import OptionsGrids from '../models/OptionsGrids';
import OptionsNames from '../models/OptionsNames';
import Options from '../models/Options';
import Files from '../models/Files';
import Galleries from '../models/Galleries';
import Sales from '../models/Sales';
import Sublevels from '../models/Sublevels';
import Carts from '../models/Carts';
import Levelsubcats from '../models/Levelsubcats';
import { fn, col, Op, where } from 'sequelize';

import ProductHelper from '../Helpers/products/ProductHelper';
const LimitersDetails =
{
  /* PACOTES E CAIXAS */
  "1": {
    min: {
      length: 16,
      width: 11,
      height: 2,
      sum: 29
    },
    max: {
      length: 100,
      width: 100,
      height: 100,
      sum: 200
    },


  },
  /* ROLOS */
  "2": {
    min: {

      /* Comprimento */
      length: 18,
      /* Diametro */
      diameter: 5,
      sum: 28
    },
    max: {
      /* Comprimento */
      length: 100,
      /* Diametro */
      Diameter: 91,
      sum: 200
    },
  },
  "3": {
    min: {
      length: 16,
      width: 11,
      sum: 27
    },
    max: {
      length: 60,
      width: 60,
      sum: 120
    },
  }
}


/*
        length
        height
        width
        type_packing
*/
class ProductsController {

  async index(req, res) {
    /* name description */
    console.log('aobaaaa')
    const products = await Products.findAll({
      order: [['created_at', 'asc']],
      where: { fk_users: req.userId, status: true },
      attributes: [
        'id',
        'name',
        'sku',
        'description',
        'brand',
        'model',
        'condition',
        'status',
      ],
      include: [
        {
          model: Categories,
          as: 'categories',
          attributes: ['id', 'name', 'status'],
        },
        {
          model: Files,
          as: 'file',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
        {
          model: Details,
          as: 'details',
          attributes: [
            'id',
            'length',
            'height',
            'width',
            'weight',
            'diameter',
            'situation',
            'type_packing',
            'delivery_time',
          ],
        },
      ],
    });
    const promisesGrids = products.map(async response => {
      const gridsList = await Grids.findAll({
        where: { fk_products: response.id, status: true },
        attributes: [
          [fn('min', col('price')), 'minPrice'],
          [fn('max', col('price')), 'maxPrice'],
          [fn('sum', col('amount')), 'amount_stock'],
        ],
      });

      const {
        id,
        name,
        description,
        sku,
        brand,
        model,
        condition,
        status,
        categories,
        file,
        details,
      } = response;
      return {
        id,
        name,
        description,
        sku,
        brand,
        model,
        condition,
        status,
        categories,
        thumb: file,
        details,
        infos: gridsList[0],
      };
    });

    const grids = await Promise.all(promisesGrids);

    return res.json(grids);
  }

  async findByIdV2(req, res) {
    try {
      const { id } = req.params;

      const products = await Products.findOne({
        where: { id, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: Details,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight',
              'diameter',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(404).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }
      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        details: detailsProd,
        description: descriptionProd,
        status: statusProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        file: fileProd,
      } = products;

      const datasheet = await Datasheets.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const grids = await Grids.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const optionsGrids = await OptionsGrids.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Options,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: OptionsNames,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await Galleries.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Files,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });

        return { Grids: response, optionsGrids, gallery };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        thumb: { id: fileProd.id, url: fileProd.url },
        categories: categoriesProd,
        details: detailsProd,
        datasheet: datasheet,
        grids: GridsList,
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async findById(req, res) {
    try {
      const { id } = req.params;

      const products = await Products.findOne({
        where: { id, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: Details,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight',
              'diameter',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(400).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }

      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        description: descriptionProd,
        condition: conditionProd,
        status: statusProd,
        details,
        file: files,
      } = products;

      const datasheet = await Datasheets.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const grids = await Grids.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const optionsGrids = await OptionsGrids.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Options,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: OptionsNames,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await Galleries.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Files,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });

        return { Grids: response, optionsGrids, gallery };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        product: {
          id: idProd,
          name: nameProd,
          description: descriptionProd,
          condition: conditionProd,
          statusProd,
          thumb: { id: files.id, url: files.url },
          details,
          categories: categoriesProd,
          datasheet,
          GridsList,
        },
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async related(req, res) {
    try {
      const { fk_products, limit } = req.params;

      const productsList = await Products.findOne({
        where: { id: fk_products, status: true },
        order: [['created_at', 'asc']],
        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
        ],
      });

      const categoriesproductsList = await Products.findAll({
        where: {
          id: {
            [Op.ne]: fk_products,
          },
          status: true,
          fk_categories: productsList.categories.id,
        },
        attributes: ['id', 'name', 'description', 'status'],
        limit: limit,
        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: Details,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight',
              'diameter',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      const promisesGrids = categoriesproductsList.map(async response => {
        const gridsList = await Grids.findAll({
          where: { fk_products: response.id, status: true },
          attributes: [
            [fn('min', col('price')), 'minPrice'],
            [fn('max', col('price')), 'maxPrice'],
            [fn('sum', col('amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, error }) {
      return res.json({ error: message });
    }
  }

  async findByIdReqUser(req, res) {
    try {
      const { id } = req.params;

      const products = await Products.findOne({
        order: [['created_at', 'asc']],
        where: { id, fk_users: req.userId, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],

        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: Details,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight',
              'diameter',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(400).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }

      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        details: detailsProd,
        description: descriptionProd,
        status: statusProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        file: fileProd,
      } = products;

      const dataSheet = await Datasheets.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const prodsubcats = await Prodsubcats.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: Subcategories,
            as: 'subcategories',
            attributes: ['id', 'name', 'status', 'status'],
          },
        ],
      });

      const grids = await Grids.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const options = await OptionsGrids.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Options,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: OptionsNames,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await Galleries.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: Files,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });
        /*         const { id: idOptions, name: nameOptions, type: typeOptions, status: statusOptions } = options
                console.log(options.options_names) */

        const promisesGalery = gallery.map(async response => {
          return {
            id: response.id,
            status: response.status,
            files: {
              id: response.files.id,
              url: response.files.url,
            },
          };
        });

        const galleryList = await Promise.all(promisesGalery);

        return {
          id: response.id,
          amount: response.amount,
          price: response.price,
          options_grids: options,
          gallery: galleryList,
        };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        thumb: { id: fileProd.id, url: fileProd.url },
        categories: categoriesProd,
        details: detailsProd,
        datasheet: dataSheet,
        subcategories: prodsubcats,
        grids: GridsList,
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async store(req, res) {
    try {
      const { products, subcategories, details, grids, datasheet } = req.body;
      /* ======= VALIDAÇÃO ========= */
      //Validando os detalhes e o produto

      const schema = Yup.object().shape({
        details: Yup.object().shape({
          length: Yup.string(),
          height: Yup.string(),
          width: Yup.string(),
          diameter: Yup.string(),
          weight: Yup.string().required('Por favor digite o peso do produto'),
          type_packing: Yup.string().required(
            'Por favor selecione o tipo de pacote'
          ),
          delivery_time: Yup.string(),
        }),

        products: Yup.object().shape({
          name: Yup.string().required('Por favor digite o nome do produto'),
          description: Yup.string().required(
            'Por favor digite a descrição do produto'
          ),
          brand: Yup.string(),
          model: Yup.string(),
          condition: Yup.string(),
          sku: Yup.string(),
        }),
      });

      await schema.validate({ products, details });

      const { min, max } = LimitersDetails[details.type_packing]

      if (details.type_packing === 1) {
        if (parseFloat(details.length.replace(',', '.')) < min.length || parseFloat(details.length.replace(',', '.')) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseFloat(details.height.replace(',', '.')) < min.height || parseFloat(details.height.replace(',', '.')) > max.height) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'height',
                error: 'Limites de dimensões do altura são inválidas',
              };
            },
          };
        }
        if (parseFloat(details.width.replace(',', '.')) < min.width || parseFloat(details.width.replace(',', '.')) > max.width) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do largura são inválidas',
              };
            },
          };
        }
      }

      if (details.type_packing === 2) {
        const sumTotal = parseFloat(details.length.replace(',', '.')) + (2 * parseFloat(details.diameter.replace(',', '.')))
        console.log(sumTotal)

        if (parseFloat(details.length.replace(',', '.')) < min.length || parseFloat(details.length.replace(',', '.')) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseFloat(details.diameter.replace(',', '.')) < min.diameter || parseFloat(details.diameter.replace(',', '.')) > max.diameter) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do diametro são inválidas',
              };
            },
          };
        }
        if (sumTotal < min.sum || sumTotal > max.sum) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do produto são inválidas',
              };
            },
          };
        }
      }

      if (details.type_packing === 3) {
        if (parseFloat(details.length.replace(',', '.')) < min.length || parseFloat(details.length.replace(',', '.')) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseFloat(details.width.replace(',', '.')) < min.width || parseFloat(details.width.replace(',', '.')) > max.width) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do largura são inválidas',
              };
            },
          };
        }
      }

      //Validando se existe grid no payload
      if (!grids || !grids.length > 0) {
        throw {
          toError: function () {
            return {
              type: 'grids_general',
              parameter_name: 'grids',
              error: 'Por favor cadastre os atributos do produto',
            };
          },
        };
      }

      //validando dados das grids
      const gridsMap = grids.map(async response => {
        //Validando os arquivos das grids
        const schemaGrids = Yup.object().shape({
          grids: Yup.object().shape({
            amount: Yup.number()
              .integer()
              .required('Por favor digite o estoque do produto'),
            price: Yup.number()
              .integer()
              .required('Por favor digite o valor do produto'),
          }),
        });
        await schemaGrids.validate({ grids: response });

        //Tetando se os arquivos de fotos existem
        const galleryMap = response.gallery.map(async responseGallery => {
          const fileExists = await Files.findOne({
            where: { id: responseGallery.fk_files },
          });
          if (!fileExists) {
            throw {
              toError: function () {
                return {
                  type: 'not_exists_file',
                  parameter_name:
                    'grids.gallery.id=' + responseGallery.fk_files,
                  error: 'Por favor verifique os arquivos de imagens',
                };
              },
            };
          }
          return responseGallery;
        });

        await Promise.all(galleryMap);

        if (response.options_grids.length === 0) {
          throw {
            toError: function () {
              return {
                type: 'not_exists_optionsGrids',
                parameter_name:
                  'optionsGrids',
                error: 'Por favor insira caracteristicas no seu produto',
              };
            },
          };
        }

        const optionsgridsMap = response.options_grids.map(
          async responseOptions => {

            if (!responseOptions.fk_options) {
              throw {
                toError: function () {
                  return {
                    type: 'not_exists_options',
                    parameter_name:
                      'options',
                    error: 'Por favor insira caracteristicas no seu produto',
                  };
                },
              };
            }
            if (!responseOptions.fk_options_names) {
              throw {
                toError: function () {
                  return {
                    type: 'not_exists_options_names',
                    parameter_name:
                      'options_names',
                    error: 'Por favor insira caracteristicas no seu produto',
                  };
                },
              };
            }


            const optionsExists = await Options.findOne({
              where: { id: responseOptions.fk_options },
            });

            if (!optionsExists) {
              throw {
                toError: function () {
                  return {
                    type: 'not_exists_options',
                    parameter_name:
                      'grids.options.id=' + responseOptions.fk_options,
                    error: 'Por favor verifique os atributos do seu produto',
                  };
                },
              };
            }
            const optionsNamesExists = await OptionsNames.findOne({
              where: {
                id: responseOptions.fk_options_names,
                fk_options: responseOptions.fk_options,
                fk_users: req.userId,
                status: true,
              },
            });

            if (!optionsNamesExists) {
              throw {
                toError: function () {
                  return {
                    type: 'not_exists_options',
                    parameter_name:
                      'grids.options_names.id=' + responseOptions.fk_options_names,
                    error: 'Por favor verifique os atributos do seu produto',
                  };
                },
              };
            }

            return responseOptions;
          }
        );

        await Promise.all(optionsgridsMap);

        return response;
      });
      const gridsPromise = await Promise.all(gridsMap);

      if (datasheet) {
        const datasheetMap = datasheet.map(async response => {
          const schema = Yup.object().shape({
            datasheet: Yup.object().shape({
              name: Yup.string().required(
                'Por favor, digite o titulo da ficha técnica'
              ),
              description: Yup.string().required(
                'Por favor, digite o conteúdo da ficha técnica'
              ),
            }),
          });
          await schema.validate({ datasheet: response });
        });

        await Promise.all(datasheetMap);
      }
      if (!subcategories || !subcategories.length > 0) {
        throw {
          toError: function () {
            return {
              type: 'subcategories_general',
              parameter_name: 'subcategories',
              error: 'Por favor insira as subcategorias do seu produto',
            };
          },
        };
      }

      if (products.fk_categories) {
        const subcategoriesMap = subcategories.map(async response => {
          const subcategoriesExists = await Subcategories.findOne({
            where: {
              id: response.fk_subcategories,
              fk_categories: products.fk_categories,
              status: true,
            },
          });
          if (!subcategoriesExists) {
            throw {
              toError: function () {
                return {
                  type: 'subcategories_not_exists',
                  parameter_name:
                    'subcategories.id=' + response.fk_subcategories,
                  error: 'Sua sub categoria não existe, tente novamente',
                };
              },
            };
          }
        });
        await Promise.all(subcategoriesMap);
      }

      /* ============ INICIO DO CADASTRO============= */

      const { id: idDetails } = await Details.create(req.body.details);
      products.fk_users = req.userId;
      products.fk_details = idDetails;
      const { id: idProduct } = await Products.create(products);

      if (subcategories) {
        const subcategoriesCreate = subcategories.map(async response => {
          await Prodsubcats.create({
            fk_subcategories: response.fk_subcategories,
            fk_products: idProduct,
          });
        });
        await Promise.all(subcategoriesCreate);
      }

      const gridsCreate = grids.map(async response => {
        const { id: idGrid } = await Grids.create({
          amount: response.amount,
          price: response.price,
          fk_products: idProduct,
          fk_users: req.userId,
        });

        const galleryCreate = response.gallery.map(async response => {
          await Galleries.create({
            fk_files: response.fk_files,
            fk_grids: idGrid,
          });
        });

        const optionsGridsCreate = response.options_grids.map(
          async response => {
            await OptionsGrids.create({
              fk_options: response.fk_options,
              fk_options_names: response.fk_options_names,
              fk_users: req.userId,
              fk_grids: idGrid,
            });
          }
        );
        await Promise.all(optionsGridsCreate);
      });
      await Promise.all(gridsCreate);

      if (datasheet) {
        const datasheetCreate = datasheet.map(async response => {
          await Datasheets.create({
            name: response.name,
            description: response.description,
            fk_products: idProduct,
          });

        });
        await Promise.all(datasheetCreate);
      }

      return res.json({
        status: 'success',
        message: 'Cadastro efetuado com sucesso',
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async update(req, res) {
    try {
      const { id } = req.params;
      const errors = [];
      const { products, subcategories, details, grids, datasheet } = req.body;

      /* ======= VALIDAÇÃO ========= */
      //Validando os detalhes e o produto
      const schema = Yup.object().shape({
        details: Yup.object().shape({
          length: Yup.string(),
          height: Yup.string(),
          width: Yup.string(),
          weight: Yup.string(),
          type_packing: Yup.string(),
          situation: Yup.string(),
          delivery_time: Yup.string(),
        }),
        products: Yup.object().shape({
          name: Yup.string(),
          description: Yup.string(),
          brand: Yup.string(),
          model: Yup.string(),
          condition: Yup.string(),
          sku: Yup.string(),
        }),
      });

      await schema.validate({ products, details });

      const { min, max } = LimitersDetails[details.type_packing]

      if (details.type_packing === 1) {
        if (parseInt(details.length) < min.length || parseInt(details.length) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseInt(details.height) < min.height || parseInt(details.height) > max.height) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'height',
                error: 'Limites de dimensões do altura são inválidas',
              };
            },
          };
        }
        if (parseInt(details.width) < min.width || parseInt(details.width) > max.width) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do largura são inválidas',
              };
            },
          };
        }
      }

      if (details.type_packing === 2) {
        const sumTotal = parseFloat(details.length) + (2 * parseFloat(details.diameter))

        if (parseInt(details.length) < min.length || parseInt(details.length) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseInt(details.diameter) < min.diameter || parseInt(details.diameter) > max.diameter) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do diametro são inválidas',
              };
            },
          };
        }
        if (sumTotal < min.sum || sumTotal > max.sum) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do produto são inválidas',
              };
            },
          };
        }
      }

      if (details.type_packing === 3) {
        if (parseInt(details.length) < min.length || parseInt(details.length) > max.length) {
          throw {
            toError: function () {
              return {
                type: 'details_length',
                parameter_name: 'length',
                error: 'Limites de dimensões do comprimento são inválidas',
              };
            },
          };
        }
        if (parseInt(details.width) < min.width || parseInt(details.width) > max.width) {
          throw {
            toError: function () {
              return {
                type: 'details_height',
                parameter_name: 'width',
                error: 'Limites de dimensões do largura são inválidas',
              };
            },
          };
        }
      }

      const productsExists = await Products.findOne({
        where: { id, status: true, fk_users: req.userId },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: Categories,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: Details,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });

      if (!productsExists) {
        throw {
          toError: function () {
            return {
              type: 'not_exists',
              parameter_name: 'products',
              error: 'O produto selecionado não existe !',
            };
          },
        };
      }

      if (products) {
        await productsExists.update(products);
      }

      if (details) {
        if (!productsExists.details) {
          const { id: fk_details } = await Details.create({
            length: details.length,
            height: details.height,
            width: details.width,
            weight: details.weight,
            situation: details.situation,
            type_packing: details.type_packing,
            delivery_time: details.delivery_time,
          });
          // console.log(fk_details);
          await productsExists.update({ fk_details });
        }

        await productsExists.details.update(details);
      }

      if (datasheet) {
        if (datasheet.length === 0) {
          await Datasheets.destroy({
            where: { fk_products: id },
          });
        } else {
          const datasheetsfindAll = await Datasheets.findAll({
            where: { fk_products: id },
          });
          const datasheetsAll = datasheetsfindAll.map(async datasheetResp => {
            return datasheetResp.id;
          });

          const datasheetAlls = await Promise.all(datasheetsAll);

          const datasheetMap = datasheet.map(async response => {
            if (response.id) {
              const datasheetExists = await Datasheets.findByPk(response.id);
              await datasheetExists.update({
                name: response.name,
                description: response.description,
              });
            } else {
              const createDatasheet = await Datasheets.create({
                name: response.name,
                description: response.description,
                fk_products: id,
              });

              const destroyDatasheets = datasheetAlls.filter(
                g => g !== createDatasheet.id
              );
              if (createDatasheet.id !== parseInt(destroyDatasheets)) {
                const promiseDestroy = destroyDatasheets.map(async response => {
                  await Datasheets.destroy({
                    where: { id: parseInt(response) },
                  });
                });

                await Promise.all(promiseDestroy);
              }
            }
          });
          await Promise.all(datasheetMap);
        }
      }

      if (subcategories) {
        if (subcategories.length === 0) {
          await Prodsubcats.destroy({
            where: { fk_products: id },
          });
        } else {
          const subcategoriesfindAll = await Prodsubcats.findAll({
            where: { fk_products: id },
          });
          const subcategoriesAll = subcategoriesfindAll.map(async response => {
            return response.id;
          });

          const subcategoriesAlls = await Promise.all(subcategoriesAll);

          const subcategoriesBody = subcategories.map(async response => {
            if (!response.id) {
              const subcategoriesExists = await Subcategories.findOne({
                where: {
                  id: response.fk_subcategories,
                  fk_categories: productsExists.fk_categories,
                },
              });
              if (subcategoriesExists) {
                const createSubProd = await Prodsubcats.create({
                  fk_subcategories: response.fk_subcategories,
                  fk_products: id,
                });

                const destroyProdsub = subcategoriesAlls.filter(
                  g => g !== createSubProd.id
                );

                if (createSubProd.id !== parseInt(destroyProdsub)) {
                  const promiseDestroy = destroyProdsub.map(async res => {
                    console.log(res);
                    await Prodsubcats.destroy({
                      where: { id: parseInt(res) },
                    });
                  });

                  await Promise.all(promiseDestroy);
                }
              } else {
                /* alert = { msg: 'Subcategoria não existe' } */
                errors.push({
                  type: 'not_exists',
                  parameter_name: 'subcategories',
                  message:
                    'Sub categoria selecionada não faz parte da categoria, tente novamente ou contate o suporte',
                });
              }
            }
            return response.id;
          });
          await Promise.all(subcategoriesBody);
        }
      }
      if (grids) {
        const gridsfindAll = await Grids.findAll({
          where: { fk_products: id, status: true },
        });
        const gridssAll = gridsfindAll.map(async response => {
          return response.id;
        });

        const gridsAlls = await Promise.all(gridssAll);

        if (grids.length === 0) {
          //DESTRUIR SAPORRA TODA
        } else {
          const gridsDatasheet = grids.map(async response => {
            if (response.id) {
              const gridsExists = await Grids.findOne({
                where: { id: response.id, status: true },
              });
              if (!gridsExists) {
                const gridCreate = await Grids.create({
                  amount: response.amount,
                  price: response.price,
                  fk_products: id,
                  fk_users: req.userId,
                });
                const destroyGrids = gridsAlls.filter(
                  g => g !== gridsUpdate.id
                );

                if (gridCreate.id !== parseInt(destroyGrids)) {
                  const promiseDestroy = destroyGrids.map(async res => {
                    const salesExists = await Sales.findAll({
                      where: { fk_grids: res },
                    });

                    //Apgando
                    if (salesExists.length < 0) {
                      await OptionsGrids.destroy({
                        where: { fk_grids: res, status: true },
                      });

                      await Galleries.destroy({
                        where: { fk_grids: res, status: true },
                      });
                      await Grids.destroy({
                        where: { id: res, status: true },
                      });
                    } else {
                      await Promise.all(
                        await OptionsGrids.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );

                      await Promise.all(
                        await Galleries.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );

                      await Promise.all(
                        await Grids.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );
                    }
                  });

                  await Promise.all(promiseDestroy);
                }

                ProductHelper.galleryFunction({
                  fk_grids: gridCreate.id,
                  gallery: response.gallery,
                });

                ProductHelper.OptionGrids({
                  fk_users: req.userId,
                  fk_grids: gridCreate.id,
                  options_grids: response.options_grids,
                });
              } else {
                const gridsUpdate = await gridsExists.update({
                  amount: response.amount,
                  price: response.price,
                });

                await ProductHelper.galleryFunction({
                  fk_grids: gridsUpdate.id,
                  gallery: response.gallery,
                });

                await ProductHelper.OptionGrids({
                  fk_grids: gridsUpdate.id,
                  options_grids: response.options_grids,
                });
              }
            } else {
              const gridCreate = await Grids.create({
                amount: response.amount,
                price: response.price,
                fk_products: id,
                fk_users: req.userId,
              });
              const destroyGrids = gridsAlls.filter(g => g !== gridCreate.id);

              if (gridCreate.id !== parseInt(destroyGrids)) {
                const promiseDestroy = destroyGrids.map(async res => {
                  const salesExists = await Sales.findAll({
                    where: { fk_grids: res },
                  });
                  //Apagando
                  if (salesExists.length < 0) {
                    await OptionsGrids.destroy({
                      where: { fk_grids: res, status: true },
                    });

                    await Galleries.destroy({
                      where: { fk_grids: res, status: true },
                    });
                    await Grids.destroy({
                      where: { id: res, status: true },
                    });
                  } else {
                    await Promise.all(
                      await OptionsGrids.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        console.log('aoooo');
                        await response.update({
                          status: false,
                        });
                      })
                    );

                    await Promise.all(
                      await Galleries.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        await response.update({
                          status: false,
                        });
                      })
                    );

                    await Promise.all(
                      await Grids.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        await response.update({
                          status: false,
                        });
                      })
                    );
                  }
                });

                await Promise.all(promiseDestroy);
              }

              ProductHelper.galleryFunction({
                fk_grids: gridCreate.id,
                gallery: response.gallery,
              });
              ProductHelper.OptionGrids({
                fk_grids: gridCreate.id,
                options_grids: response.options_grids,
              });
            }
          });
          await Promise.all(gridsDatasheet);
        }
      }

      return res.json(await ProductHelper.List({ id }));
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    const ProductsExists = await Products.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (!ProductsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'products',
          message: 'Produto não existe, tente novamente !',
        },
      });
    }
    // ==========Puxando subprods =========
    const prodsubExists = await Prodsubcats.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });
    // ==========Puxando details =========
    const detailsExists = await Details.findOne({
      where: { id: ProductsExists.fk_details, status: true },
    });
    // ==========Puxando datasheet =========
    const datasheetExists = await Datasheets.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });
    // ==========Puxando grids =========
    const gridsExists = await Grids.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });

    if (prodsubExists) {
      const promisesprodsubExists = prodsubExists.map(async prodsub => {
        await prodsub.update({ status: false });
      });
      await Promise.all(promisesprodsubExists);
    }

    if (ProductsExists.fk_thumb) {
      const fk_thumbExists = await Files.findOne({
        where: { id: ProductsExists.fk_thumb },
      });

      if (fk_thumbExists) {
        await fk_thumbExists.update({ status: false });
      }
    }

    if (gridsExists) {
      const promisesCategories = gridsExists.map(async grids => {
        if (grids) {
          await grids.update({ status: false });
        }
      });
      await Promise.all(promisesCategories);
    }

    if (detailsExists) {
      await detailsExists.update({ status: false });
    }

    if (datasheetExists) {
      const promisesDatasheet = datasheetExists.map(async datasheet => {
        if (datasheet) {
          await datasheet.update({ status: false });
        }
      });

      await Promise.all(promisesDatasheet);
    }

    if (ProductsExists) {
      await ProductsExists.update({ status: false });
    }

    return res.json({ msg: 'Produto desabilitado' });
  }
}

export default new ProductsController();
