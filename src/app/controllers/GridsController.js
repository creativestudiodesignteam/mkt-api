import { Op, } from 'sequelize'
import * as Yup from 'yup';
import Grids from '../models/Grids';
import Users from '../models/Users';
import Products from '../models/Products';


class GridsController {
  async index(req, res) {
    const { fk_products } = req.params
    const grids = await Grids.findAll({
      where: {
        fk_products, status: true,
      },
    })

    return res.json(grids);
  }

  async store(req, res) {
    req.body.fk_users = req.userId;

    const schema = Yup.object().shape({
      amount: Yup.number().integer().required(),
      price: Yup.number().required(),
      fk_products: Yup.number(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      })
    }

    const productsExists = await Products.findByPk(req.body.fk_products);

    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produtos não existe, tente novamente !"
        }
      })
    }

    const { id, amount, price } = await Grids.create(req.body);
    return res.json({
      id,
      amount,
      price
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      amount: Yup.number().integer(),
      price: Yup.number().required(),
      fk_products: Yup.number(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      })
    }

    const productsExists = await Products.findByPk(req.body.fk_products);

    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produtos não existe, tente novamente !"
        }
      })
    }
    const GridsExists = await Grids.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (!GridsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "grids",
          "message": "Relacionamento da grade não existe, tente novamente !"
        }
      })
    }
    const {
      amount,
      price
    } = await GridsExists.update(req.body);

    return res.json(
      {
        amount,
        price
      }
    );
  }

  async delete(req, res) {
    const { id } = req.params
    const GridsExists = await Grids.findOne({
      where: { id, fk_users: req.userId, status: true },
    });
    if (!GridsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "grids",
          "message": "Relacionamento da grade não existe, tente novamente !"
        }
      })
    }

    await GridsExists.update({ status: false });

    return res.json({ msg: 'Grade está desabilitada' })

  }
}

export default new GridsController();
