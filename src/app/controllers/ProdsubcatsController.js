import * as Yup from 'yup';
import Products from '../models/Products';
import Subcategories from '../models/Subcategories';
import Prodsubcats from '../models/Prodsubcats';
import Grids from '../models/Grids';
import Files from '../models/Files';
import { fn, col } from 'sequelize'
import Categories from '../models/Categories';

class ProdsubcatsController {
  async index(req, res) {

    const prodsubcats = await Prodsubcats.findAll({
      where: { fk_subcategories: req.params.fk_subcategories, status: true },
      include: [
        {
          model: Products,
          as: 'products',
          attributes: ['id', 'name', 'sku', 'description', 'brand', 'model', 'status'],
          include: [
            {
              model: Files,
              as: 'file',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
            {
              model: Categories,
              as: 'categories',
              attributes: ['id', 'name', 'status'],
            },
          ]
        },
      ]
    })

    const promisesGrids = prodsubcats.map(async response => {
      const gridsList = await Grids.findAll({
        where: { fk_products: response.products.id },
        attributes: [
          [fn('min', col('price')), 'minPrice'],
          [fn('max', col('price')), 'maxPrice'],
          [fn('sum', col('amount')), 'amount_stock']
        ]
      })

      const { id, name, description, sku, brand, model, status, categories, file, details } = response.products
      return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

    })

    const grids = await Promise.all(promisesGrids)

    return res.json(grids);
  }

  async store(req, res) {
    const subcatregoriesExists = await Subcategories.findOne({
      where: { id: req.body.fk_subcategories, status: true }
    })
    const prodsubcatregoriesExists = await Prodsubcats.findOne({
      where: { fk_subcategories: req.body.fk_subcategories, fk_products: req.body.fk_products, status: true }
    })
    if (prodsubcatregoriesExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria já está relacionada a este produto."
        }
      })
    }
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria não existe, tente novamente !"
        }
      })
    }
    const productsExists = await Products.findOne({
      where: { id: req.body.fk_products, status: true }
    })
    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produto não existe, tente novamente !"
        }
      })
    }

    const {
      id,
    } = await Prodsubcats.create(req.body);


    return res.json(
      await Prodsubcats.findOne({
        where: { id, status: true },
        include: [
          {
            model: Subcategories,
            as: 'subcategories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Products,
            as: 'products',
            attributes: ['id', 'name', 'description', 'brand', 'model', 'status'],
          }
        ]
      })
    );
  }

  async update(req, res) {
    const { id } = req.params

    const prodsubcatregoriesExists = await Prodsubcats.findOne({
      where: { id, status: true }
    })
    if (!prodsubcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcatregories",
          "message": "Sub Categoria não está relacionada ao produto, tente novamente !"
        }
      })
    }
    const subcatregoriesExists = await Subcategories.findOne({
      where: { id: req.body.fk_subcategories, status: true }
    })
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria não existe, tente novamente !"
        }

      })
    }
    const productsExists = await Products.findOne({
      where: { id: req.body.fk_products }
    })
    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produto não existe, tente novamente !"
        }
      })
    }

    await prodsubcatregoriesExists.update(req.body);

    return res.json(
      await Prodsubcats.findOne({
        where: { id },
        include: [
          {
            model: Subcategories,
            as: 'subcategories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: Products,
            as: 'products',
            attributes: ['id', 'name', 'description', 'brand', 'model', 'status'],
          }
        ]
      }));
  }

  async delete(req, res) {
    const { id } = req.params
    const ProdsubcatsExists = await Prodsubcats.findOne({
      where: { id, status: true },
    });
    if (!ProdsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "provider",
          "message": "Relação entre produto e sub categoria não existe, tente novamente !"
        }
      });
    }

    await ProdsubcatsExists.destroy();

    return res.json({ msg: 'Vinculo entre sub categoria e produto foi excluida' })
  }
}

export default new ProdsubcatsController();
