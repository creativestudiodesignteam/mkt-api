import Sales from '../models/Sales';
import OptionsGrids from '../models/OptionsGrids';
import Grids from '../models/Grids';
import Products from '../models/Products';
import Files from '../models/Files';
import Providers from '../models/Providers';
import Payments from '../models/Payments';
import Fretes from '../models/Fretes';
import Addresses from '../models/Addresses';
import Invoices from '../models/Invoices';
import CreditCards from '../models/CreditCards';
import { format } from 'date-fns';
import pt from 'date-fns/locale/pt-BR';
import User from '../models/Users';
import Customers from '../models/Customers';
import Options from '../models/Options';
import OptionsNames from '../models/OptionsNames';
import Progresses from '../models/Progresses';

const Payload = require('../services/safe2pay/Transaction');

class SalesController {
  async my_sales(req, res) {
    const provider = await Providers.findOne({
      where: { fk_users: req.userId }
    })
    const sales = await Sales.findAll({
      where: { fk_provider: provider.id },
      order: [['created_at', 'desc']],
      attributes: [
        'id',
        'hash',
        'amount',
        'quota',
        'price',
        'status',
        'canceled_at',
        'created_at',
      ],
      include: [
        {
          model: Grids,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: Products,
              as: 'products',
              attributes: ['id', 'name', 'status'],
              include: [
                {
                  model: Files,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: Providers,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name'],
        },
        {
          model: Payments,
          as: 'payments',
          attributes: [
            'id',
            'code',
            'method',
            'url',
            'status',
            'situations',
            'updated_at',
          ],
        },
        {
          model: CreditCards,
          as: 'creditcards',
          attributes: ['id', 'holder_name', 'first_digits']
        },
        {
          model: Fretes,
          as: 'fretes',
          attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo', 'tracking', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number'],
        },
        {
          model: Invoices,
          as: 'invoices',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
      ],
    });

    const salesPromise = sales.map(async response => {
      const {
        id,
        hash,
        amount,
        quota,
        price,
        status,
        created_at,
        canceled_at,
        grids,
        providers,
        creditcards,
        payments,
        fretes,
        invoices,
      } = response;
      const progress = await Progresses.findAll({
        where: { fk_sales: response.id },
        attributes: ['id', 'type', 'situation', 'date', 'step']
      })
      let lastProgress
      if (progress.length > 0) {
        const aux = progress[progress.length - 1];
        lastProgress = aux.situation;
      } else {
        lastProgress = 'Pedido em Espera'
      }
      const formatteCreated_at = format(created_at, "dd'/'MM'/'yyyy", {
        locale: pt,
      });


      let payment;
      if (payments.method === '1') {
        payment = {
          payment_method: payments.method,
          bankSlip: payments.url,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      } else {
        payment = {
          payment_method: payments.method,
          holder_name: creditcards.holder_name,
          first_digits: creditcards.first_digits,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      }


      return {
        id,
        hash,
        code: hash + '-' + id + ' #' + grids.id,
        price,
        total: parseInt(price) + parseInt(fretes.valor),
        data: formatteCreated_at,
        status,
        canceled_at,
        nfe: invoices ? invoices.url : null,
        products: {
          id: grids.products.id,
          name: grids.products.name,
          status: grids.products.status,
          thumb: grids.products.file.url,
        },
        payments: payment,
        tracking: fretes.tracking,
        situations: lastProgress,
      };


    });
    const promise = await Promise.all(salesPromise);

    /* hash, amount, quota, price, status, created_at, canceled_at, grids, providers, payments, fretes, */

    return res.json(promise);
  }

  async my_requests(req, res) {
    const sales = await Sales.findAll({
      where: { fk_users: req.userId },
      order: [['created_at', 'desc']],
      attributes: [
        'id',
        'hash',
        'amount',
        'quota',
        'price',
        'status',
        'canceled_at',
        'created_at',
      ],
      include: [
        {
          model: Grids,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: Products,
              as: 'products',
              attributes: ['id', 'name', 'status'],
              include: [
                {
                  model: Files,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: Providers,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name'],
        },
        {
          model: Payments,
          as: 'payments',
          attributes: [
            'id',
            'code',
            'url',
            'method',
            'situations',
            'status',
            'updated_at',
          ]
        },
        {
          model: CreditCards,
          as: 'creditcards',
          attributes: ['id', 'holder_name', 'first_digits']
        },
        {
          model: Fretes,
          as: 'fretes',
          attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo', 'tracking', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number'],
        },
        {
          model: Invoices,
          as: 'invoices',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
      ],
    });

    const salesPromise = sales.map(async response => {
      const {
        id,
        hash,
        amount,
        quota,
        price,
        status,
        created_at,
        canceled_at,
        grids,
        providers,
        payments,
        creditcards,
        fretes,
        invoices,
      } = response;
      const progress = await Progresses.findAll({
        where: { fk_sales: response.id },
        attributes: ['id', 'type', 'situation', 'date', 'step']
      })
      const progressPromise = progress.map(async response => {
        const dateFormat = format(response.date, "dd'/'MM'/'yyyy 'às' hh':'mm", {
          locale: pt,
        });

        return ({
          id: response.id,
          type: response.type,
          situation: response.situation,
          date: dateFormat,
          step: response.step,
        })
      })

      const progressResolve = await Promise.all(progressPromise)

      let lastProgress
      if (progress.length > 0) {
        const aux = progress[progress.length - 1];
        lastProgress = aux.situation;
      } else {
        lastProgress = 'Pedido em Espera'
      }

      const formatteDate = format(created_at, "dd'/'MM'/'yyyy", {
        locale: pt,
      });

      let payment;
      if (payments.method === '1') {
        payment = {
          payment_method: payments.method,
          bankSlip: payments.url,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      } else {
        payment = {
          payment_method: payments.method,
          holder_name: creditcards.holder_name,
          first_digits: creditcards.first_digits,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      }

      return {
        id,
        hash,
        code: hash + '-' + id + ' #' + grids.id,
        price,
        total: parseInt(price) + parseInt(fretes.valor),
        data: formatteDate,
        status,
        canceled_at,
        nfe: invoices ? invoices.url : null,
        products: {
          id: grids.products.id,
          name: grids.products.name,
          status: grids.products.status,
          thumb: grids.products.file.url,
        },
        payments: payment,
        tracking: fretes.tracking,
        situations: lastProgress,
        progress: progressResolve
      };
    });

    const salesResolve = await Promise.all(salesPromise)

    return res.json(salesResolve);
  }

  async detail(req, res) {
    try {
      const { id } = req.params;
      const salesDetail = await Sales.findOne({
        where: { id, fk_users: req.userId },
        attributes: [
          'id',
          'hash',
          'amount',
          'quota',
          'price',
          'status',
          'canceled_at',
          'created_at',
        ],
        include: [
          {
            model: Grids,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: Products,
                as: 'products',
                attributes: ['id', 'name', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
          {
            model: Providers,
            as: 'providers',
            attributes: ['id', 'cnpj', 'fantasy_name'],
          },
          {
            model: Payments,
            as: 'payments',
            attributes: [
              'id',
              'code',
              'method',
              'situations',
              'status',
              'references',
              'url',
              'updated_at',
            ],
          },
          {
            model: Fretes,
            as: 'fretes',
            attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo', 'tracking', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number'],
          },
          {
            model: Addresses,
            as: 'addresses',
            attributes: [
              'id',
              'name',
              'postcode',
              'state',
              'city',
              'neighborhood',
              'street',
              'number',
              'complement',
              'references',
              'status',
              'select_at',
            ],
          },
          {
            model: CreditCards,
            as: 'creditcards',
            attributes: ['id', 'holder_name', 'first_digits', 'status'],
          },
        ],
      });

      if (!salesDetail) {
        throw {
          toError: function () {
            return {
              type: 'sales_invalid',
              parameter_name: 'sales_not_exists',
              error: 'Pedido não existe, tente novamente !',
            };
          },
        };
      }

      const {
        hash,
        amount,
        quota,
        price,
        status,
        created_at,
        canceled_at,
        grids,
        providers,
        addresses,
        creditcards,
        payments,
        fretes,
      } = salesDetail;
      const progress = await Progresses.findAll({
        where: { fk_sales: salesDetail.id },
        attributes: ['id', 'type', 'situation', 'date', 'step']
      })


      let lastProgress
      if (progress.length > 0) {
        const aux = progress[progress.length - 1];
        lastProgress = aux.situation;
      } else {
        lastProgress = 'Pedido em Espera'
      }


      let payment;
      if (payments.method === '1') {
        payment = {
          payment_method: payments.method,
          bankSlip: payments.url,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      } else {
        payment = {
          payment_method: payments.method,
          holder_name: creditcards.holder_name,
          first_digits: creditcards.first_digits,
          situations: payments.situations,
          updated_at: payments.updated_at
        }
      }
      const progressPromise = progress.map(async response => {
        const dateFormat = format(response.date, "dd'/'MM'/'yyyy 'às' hh':'mm", {
          locale: pt,
        });

        return ({
          id: response.id,
          type: response.type,
          situation: response.situation,
          date: dateFormat,
          step: response.step,
        })
      })
      const progressResolve = await Promise.all(progressPromise)

      return res.json({
        id,
        hash,
        code: hash + '-' + id + ' #' + grids.id,
        amount,
        quota,
        price,
        total: parseInt(price) + parseInt(fretes.valor),
        status,
        date: format(created_at, "dd'/'MM'/'yyyy", {
          locale: pt,
        }),
        canceled_at,
        situation: lastProgress,
        products: {
          id: grids.products.id,
          name: grids.products.name,
          status: grids.products.status,
          thumb: grids.products.file.url,
          grid: {
            id: grids.id,
            price: grids.price,
            status: grids.status,
          },
        },
        shipping: {
          id: fretes.id,
          value: fretes.valor,
          type: fretes.tipo,
          code: fretes.cod_servico,
          deadline: fretes.prazo,
          tracking: fretes.tracking,
        },
        payments: payment,
        addresses: {
          name: fretes.name,
          postcode: fretes.postcode,
          state: fretes.state,
          city: fretes.city,
          neighborhood: fretes.neighborhood,
          street: fretes.street,
          number: fretes.number,
        },
        progress: progressResolve,

      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async detailSales(req, res) {
    try {
      const { id } = req.params;
      const userProvider = await Providers.findOne({
        where: { fk_users: req.userId }
      })

      if (!userProvider) {
        throw {
          toError: function () {
            return {
              type: 'user_invalid',
              parameter_name: 'provider_not_exists',
              error: 'Você não tem permissão para acessar estes dados !',
            };
          },
        };
      }

      const salesDetail = await Sales.findOne({
        where: { id, fk_provider: userProvider.id },
        attributes: [
          'id',
          'hash',
          'amount',
          'quota',
          'price',
          'status',
          'canceled_at',
          'created_at',
        ],
        include: [
          {
            model: Grids,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: Products,
                as: 'products',
                attributes: ['id', 'name', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
          {
            model: User,
            as: 'users',
            attributes: ['id', 'email', 'type'],
          },
          {
            model: Providers,
            as: 'providers',
            attributes: ['id', 'cnpj', 'fantasy_name'],
          },
          {
            model: Payments,
            as: 'payments',
            attributes: [
              'id',
              'code',
              'method',
              'situations',
              'status',
              'references',
              'url',
              'created_at',
            ],
          },
          {
            model: Fretes,
            as: 'fretes',
            attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo', 'tracking', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number'],
          },
          {
            model: Addresses,
            as: 'addresses',
            attributes: [
              'id',
              'name',
              'postcode',
              'state',
              'city',
              'neighborhood',
              'street',
              'number',
              'complement',
              'references',
              'status',
              'select_at',
            ],
          },
          {
            model: CreditCards,
            as: 'creditcards',
            attributes: ['id', 'holder_name', 'first_digits'],
          },
        ],
      });

      if (!salesDetail) {
        throw {
          toError: function () {
            return {
              type: 'sales_invalid',
              parameter_name: 'sales_not_exists',
              error: 'Pedido não existe, tente novamente !',
            };
          },
        };
      }

      const {
        hash,
        amount,
        quota,
        price,
        status,
        created_at,
        canceled_at,
        grids,
        providers,
        addresses,
        creditcards,
        payments,
        fretes,
        users
      } = salesDetail;

      let shipping = fretes;
      const progress = await Progresses.findAll({
        where: { fk_sales: id },
        attributes: ['id', 'type', 'situation', 'date', 'step']
      })
      const progressPromise = progress.map(async response => {
        const dateFormat = format(response.date, "dd'/'MM'/'yyyy 'às' hh':'mm", {
          locale: pt,
        });

        return ({
          id: response.id,
          type: response.type,
          situation: response.situation,
          date: dateFormat,
          step: response.step,
        })
      })
      const progressResolve = await Promise.all(progressPromise)

      let lastProgress

      if (progress.length > 0) {
        const aux = progress[progress.length - 1];
        lastProgress = aux.situation;
      } else {
        lastProgress = 'Pedido em Espera'
      }


      let purchaser;

      if (!users.type) {
        purchaser = await Customers.findOne({
          where: { fk_users: users.id },
          attributes: ['id', 'name', 'cpf']
        })
      } else {
        purchaser = await Providers.findOne({
          where: { fk_users: users.id },
          attributes: ['id', 'fantasy_name', 'cnpj']
        })
      }

      const optionsGrids = await OptionsGrids.findAll({
        where: { fk_grids: grids.id },
        attributes: [],
        include: [
          {
            model: Options,
            as: 'options',
            attributes: ['id', 'name'],
          },
          {
            model: OptionsNames,
            as: 'options_names',
            attributes: ['id', 'name'],
          }
        ]

      })

      return res.json({
        id,
        hash,
        code: hash + '-' + id + ' #' + grids.id,
        amount,
        price,
        total: parseInt(price) + parseInt(fretes.valor),
        status,
        situations: lastProgress,
        date: format(created_at, "dd'/'MM'/'yyyy", {
          locale: pt,
        }),
        canceled_at,
        buyer: {
          name: (users.type ? purchaser.fantasy_name : purchaser.name),
          doc: (users.type ? 'CNPJ ' + purchaser.cnpj : 'CPF ' + purchaser.cpf)
        },
        products: {
          id: grids.products.id,
          name: grids.products.name,
          thumb: grids.products.file.url,
          options_grids: optionsGrids
        },
        addresses: {
          name: fretes.name,
          postcode: fretes.postcode,
          state: fretes.state,
          city: fretes.city,
          neighborhood: fretes.neighborhood,
          street: fretes.street,
          number: fretes.number,
        },
        shipping,
        progress: progressResolve
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async addNfe(req, res) {
    try {
      const { fk_sales } = req.params;
      const { fk_invoices } = req.body;

      const checkSales = await Sales.findByPk(fk_sales);

      if (!checkSales) {
        throw {
          toError: function () {
            return {
              type: 'sales_invalid',
              parameter_name: 'sales_not_exists',
              error: 'Pedido inválido, tente novamente !',
            };
          },
        };
      }

      const checkInvoices = await Invoices.findOne({
        where: { id: fk_invoices },
      });
      if (!checkInvoices) {
        throw {
          toError: function () {
            return {
              type: 'invoices_invalid',
              parameter_name: 'nfe_not_exists',
              error: 'Nota fiscal inválida, tente novamente !',
            };
          },
        };
      }

      const updating = await checkSales.update({
        fk_invoices: checkInvoices.id,
      });

      return res.json(updating);
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async delete(req, res) {
    const { hash } = req.params;

    const salesExists = await Sales.findOne({
      where: { hash: hash, status: true },
    });

    const paymentExists = await Payments.findOne({
      where: { code: hash, status: true },
    });

    if (!salesExists || !paymentExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'sales',
          message: 'Erro ao cancelar venda!',
        },
      });
    }

    const cancelSafe2Pay = await Payload.cancelTransaction(
      hash,
      paymentExists.method
    );


    if (cancelSafe2Pay.HasError) {
      return res.json({
        error: {
          type: 'payment_error',
          parameter_name: 'sale',
          message: cancelSafe2Pay.Error,
        },
      });
    }

    await salesExists.update({ status: false });

    return res.json({ msg: 'venda cancelada' });
  }

  async addTracking(req, res) {
    const { id } = req.params;
    const { tracking } = req.body;
    const provider = await Providers.findOne({
      where: { fk_users: req.userId }
    })
    if (!provider) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'provider',
          message: 'Fornecedor não existe, tente novamente!',
        },
      });
    }
    const sales = await Sales.findOne({
      where: { id, status: true, fk_provider: provider.id },
    });

    if (!sales) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'sales',
          message: 'Venda não existe, tente novamente!',
        },
      });
    }

    const fretes = await Fretes.findOne({
      where: { id: sales.fk_fretes },
    });

    if (!fretes) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'tracking',
          message: 'Frete não existe, tente novamente!',
        },
      });
    }

    await fretes.update({ tracking: tracking, status: true });
    await Progresses.create({
      type: 'send_transporter',
      situation: 'Enviado a transportadora',
      date: Date.now(),
      step: 3,
      fk_sales: id,
      status: true
    })
    return res.json({ msg: 'Código de rastreio atualizado' });

  }
}

export default new SalesController();
