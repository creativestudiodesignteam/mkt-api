import Users from '../models/Users';
import Products from '../models/Products';
import Categories from '../models/Categories';
import Subcategories from '../models/Subcategories';
import Prodsubcats from '../models/Prodsubcats';
import Grids from '../models/Grids';
import Files from '../models/Files';
import { fn, col } from 'sequelize';
import * as Yup from 'yup';

class HomeController {
  async findAll(req, res) {
    try {
      const categoriesList = await Categories.findAll({
        limit: 5,
        where: {
          status: true,
        },
      });
      const promisesSubCategories = categoriesList.map(async categories => {
        const subCategories = await Subcategories.findAll({
          raw: true,
          attributes: ['id', 'name', 'status'],
          where: {
            status: true,
            fk_categories: categories.id,
          },
        });
        const promisesProdSub = subCategories.map(async subcategories => {
          const prodsubcats = await Prodsubcats.findAll({
            limit: 6,
            /* raw: true, */
            attributes: ['id'],
            where: {
              status: true,
              fk_subcategories: subcategories.id,
            },
            attributes: ['id'],
            include: [
              {
                model: Products,
                as: 'products',
                where: { status: true },
                attributes: ['id', 'name', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url'],
                  },
                ],
              },
            ],
          });

          const promisesProdutos = prodsubcats.map(async response => {
            const gridsProducts = await Grids.findOne({
              attributes: [
                [fn('min', col('price')), 'minPrice'],
                [fn('max', col('price')), 'maxPrice'],
              ],
              where: {
                status: true,
                fk_products: response.products.id,
              },
            });
            gridsProducts;
            return {
              products: { id: prodsubcats.id, product: productsprodsubcats.id },
            };
          });

          const grids = await Promise.all(promisesProdutos);

          return { ...subcategories, grids };
        });
        const produtos = await Promise.all(promisesProdSub);

        const { id, name, status } = categories;

        return {
          id,
          name,
          status,
          subcategories: produtos,
        };
      });
      try {
        const response = await Promise.all(promisesSubCategories);
        return res.json(response);
      } catch ({ message, ...error }) {
        return res.status(404).json({ error: message });
      }
    } catch (error) {
      return res.json({ error: 'Ocorreu algum erro, tente novamente' });
    }
  }
  async findProductByCategories(req, res) {
    try {
      const { fk_categories, limit } = req.params;

      const categoriesExists = await Categories.findOne({
        where: { id: fk_categories, status: true },
      });
      if (!categoriesExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'categories',
            message: 'Categoria não existe, tente novamente !',
          },
        });
      }
      const { page = 1 } = req.query;

      const product = await Products.findAll({
        limit: limit,
        order: ['created_at'],
        offset: (page - 1) * limit,
        where: { fk_categories, status: true },
        attributes: [
          'id',
          'name',
          'sku',
          'description',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: Files,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url'],
          },
        ],
      });

      if (!product) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'products in categories',
            message: 'Não existe produtos cadastrados nessa categoria !',
          },
        });
      }
      const promisesGrids = product.map(async response => {
        const gridsList = await Grids.findAll({
          where: { fk_products: response.id, status: true },
          attributes: [
            [fn('min', col('price')), 'minPrice'],
            [fn('max', col('price')), 'maxPrice'],
            [fn('sum', col('amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }
}

export default new HomeController();
