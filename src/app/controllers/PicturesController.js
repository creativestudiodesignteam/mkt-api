import Pictures from '../models/Pictures';

class PicturesController {
  async index(req, res) {
    const { fk_grids } = req.params;
    const pictures = await Pictures.findAll({
      where: { fk_grids }
    })
    return res.json(pictures);
  }

  async store(req, res) {
    const list = await req.files.map(async  response =>

      await Pictures.create(
        {
          name: response.originalname,
          path: response.filename,
          fk_grids: req.params.fk_grids
        }
      ),
    )

    const functionWithPromise = item => { //a function that returns a promise
      return Promise.resolve(item)
    }

    const anAsyncFunction = async item => {
      return functionWithPromise(item)
    }

    const getData = async () => {
      return Promise.all(list.map(item => anAsyncFunction(item)))
    }

    getData().then(data => {
      return res.json(data);
    })
  }

}
export default new PicturesController();

