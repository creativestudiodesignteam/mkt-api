import * as Yup from 'yup';
import Details from '../models/Details';

class DetailsController {
  async index(req, res) {
    const { id } = req.params;
    const details = await Details.findOne({
      where: { id, status: true },
      attributes: [
        'id',
        'length',
        'height',
        'width',
        'weight',
        'situation',
        'delivery_time',
      ],
    });
    if (!details) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'details',
          message: 'Detalhe do produto não existe, tente novamente',
        },
      });
    }
    return res.json(details);
  }

  async store(req, res) {
    /* length height width ((weight_unit //removido)) weight situation */
    const schema = Yup.object().shape({
      length: Yup.string().required(),
      height: Yup.string().required(),
      width: Yup.string().required(),
      weight: Yup.string().required(),
      situation: Yup.string(),
      delivery_time: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const {
      id,
      length,
      height,
      width,
      weight,
      situation,
      delivery_time,
    } = await Details.create(req.body);

    return res.json({
      id,
      length,
      height,
      width,
      weight,
      situation,
      delivery_time,
    });
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      length: Yup.string(),
      height: Yup.string(),
      width: Yup.string(),
      weight: Yup.string(),
      situation: Yup.string(),
      delivery_time: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const DetailsExists = await Details.findOne({
      where: { id, status: true },
    });
    if (!DetailsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'details',
          message: 'Detalhes não existe, tente novamente !',
        },
      });
    }
    const {
      length,
      height,
      width,
      weight,
      situation,
      delivery_time,
    } = await DetailsExists.update(req.body);

    return res.json({
      id,
      length,
      height,
      width,
      weight,
      situation,
      delivery_time,
    });
  }

  async delete(req, res) {
    const { id } = req.params;
    const DetailsExists = await Details.findOne({
      where: { id, status: true },
    });
    if (!DetailsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'sublevels',
          message: 'Sub categoria não existe.',
        },
      });
    }

    await DetailsExists.destroy();

    return res.json({ msg: 'Detalhe deletado' });
  }
}

export default new DetailsController();
