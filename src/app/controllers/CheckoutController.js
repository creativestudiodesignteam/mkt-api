import Users from '../models/Users';
import Addresses from '../models/Addresses';
import Carts from '../models/Carts';
import Grids from '../models/Grids';
import Products from '../models/Products';
import Sales from '../models/Sales';
import Payments from '../models/Payments';
import CreditCards from '../models/CreditCards';
import Fretes from '../models/Fretes';
import Customers from '../models/Customers';
import Providers from '../models/Providers';
import Progresses from '../models/Progresses';
import { format } from 'date-fns'

import Queue from '../../lib/Queue';


const Payload = require('../services/safe2pay/Transaction');

class CheckoutController {
  async store(req, res) {
    const {
      card_holder,
      card_number,
      expiration_date,
      security_code,
      fk_addresses,
      fk_creditcard,
    } = req.body;

    let payment_type = parseInt(req.body.payment_type);

    const cartModel = await Carts.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: [
        'id',
        'amount',
        'fk_grids',
        'fk_provider',
        'fk_users',
        'status',
      ],
    });

    if (!cartModel.length) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'cart',
          message: 'Carrinho não existe.',
        },
      });
    }

    const freteModel = await Fretes.findOne({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'valor', 'tipo'],
    });

    let frete;
    if (!freteModel) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'cart',
          message: 'Frete não selecionado.',
        },
      });
    } else {
      frete = {
        id: freteModel.id,
        valor: freteModel.valor / 100,
        tipo: freteModel.tipo
      }
    }
    const addressModel = await Addresses.findOne({
      where: { id: req.body.fk_addresses, fk_users: req.userId, status: true },
      attributes: [
        'id',
        'name',
        'postcode',
        'state',
        'city',
        'neighborhood',
        'street',
        'number',
        'complement',
        'references',
      ],
    });


    if (!addressModel) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'addresses',
          message: 'Endereço não está cadastrado.',
        },
      });
    }
    // CREDITO OU DEBITO
    if (payment_type === 2 || payment_type === 4) {
      if (card_holder && card_number && expiration_date && security_code) {
        let generateToken = await Payload.generateToken(
          card_holder,
          card_number,
          expiration_date,
          security_code,
          payment_type,
          card_number.substring(0, 4),
          req.userId
        );

        var cardModel = {
          type: generateToken.card.type,
          token: generateToken.card.token,
        };
      } else {
        var cardModel = await CreditCards.findOne({
          where: { id: req.body.fk_creditcard },
          attributes: ['id', 'token', 'type', 'holder_name', 'first_digits'],
        });
      }
      if (!cardModel) {
        return res.json({
          error: {
            type: 'not_exists_parameter',
            parameter_name: 'checkout',
            message: 'Nenhum cartão cadastrado.',
          },
        });
      } else {
        //multi type
        if (cardModel.type !== 0) {
          //pagando com debito e usando cartao de credito
          if (cardModel.type === 2 && payment_type === 4) {
            return res.json({
              error: {
                type: 'not_exists_parameter',
                parameter_name: 'checkout',
                message: 'Cartão não compatível com forma de pagamento.',
              },
            });
          }

          //pagando com credito e usando cartao de debito
          if (cardModel.type === 4 && payment_type === 2) {
            return res.json({
              error: {
                type: 'not_exists_parameter',
                parameter_name: 'checkout',
                message: 'Cartão não compatível com forma de pagamento.',
              },
            });
          }
        }
      }
    }

    var produtos = [];
    var totalPrice = 0;
    for (const element of cartModel) {
      const productsModel = await Grids.findOne({
        where: { id: element.fk_grids, status: true },
        attributes: ['id', 'fk_products', 'price'],
        include: [
          {
            model: Products,
            as: 'products',
            attributes: ['id', 'name', 'brand'],
          },
        ],
      });

      let productPrice =
        parseInt(productsModel.price) * parseInt(element.amount);
      totalPrice = totalPrice + productPrice;
      produtos = [
        {
          id: productsModel.products.id,
          fk_grids: element.fk_grids,
          nome: productsModel.products.name,
          valor: productPrice / 100,
          qty: element.amount,
        },
      ];
    }

    const userModel = await Users.findOne({
      where: { id: req.userId },
      attributes: ['email', 'type'],
    });

    let personalData = {};
    if (userModel.type) {
      const providerModel = await Providers.findOne({
        where: { fk_users: req.userId },
        attributes: ['fantasy_name', 'cnpj', 'telephone_commercial'],
      });

      personalData = {
        name: providerModel.fantasy_name,
        documento: providerModel.cnpj,
        telephone: providerModel.telephone_commercial,
      };
    } else {
      const customerModel = await Customers.findOne({
        where: { fk_users: req.userId },
        attributes: ['name', 'cpf', 'telephone'],
      });

      personalData = {
        name: customerModel.name,
        documento: customerModel.cpf,
        telephone: customerModel.telephone,
      };
    }

    let safe2payData = {
      SAFE2PAY_IS_SANDBOX: process.env.SAFE2PAY_IS_SANDBOX,
      SAFE2PAY_KEY_SANDBOX: process.env.SAFE2PAY_KEY_SANDBOX,
      SAFE2PAY_CALLBACK_URL: process.env.SAFE2PAY_CALLBACK_URL,
    };

    const pagamento = await Payload.setTransaction(
      req,
      addressModel,
      userModel,
      produtos,
      cardModel,
      payment_type,
      safe2payData,
      frete,
      personalData
    );


    var status = true;
    if (pagamento.response.HasError) {
      return res.json({
        error: {
          type: 'checkout',
          parameter_name: 'checkout',
          message: pagamento.response.Error,
        },
      });
    }

    let paymentDetail = pagamento.response.ResponseDetail;

    //boleto
    if (payment_type === 1) {
      status = false;
    }

    let references = '';
    let url = '';
    //credito e debito
    if (payment_type === 2 || payment_type === 4) {
      //pagamento autorizado
      if (paymentDetail.Status === 3) {
        status = true;
      } else {
        status = false;
      }

      references = paymentDetail.Token;
    } else {
      url = paymentDetail.BankSlipUrl;
    }

    let payment = await Payments.create({
      code: paymentDetail.IdTransaction,
      method: payment_type,
      references: references,
      situations: paymentDetail.Message,
      status: status,
      url: url,
    });

    for (const element of cartModel) {
      // inativa o carrinho
      let cartInactive = await Carts.findOne({
        where: { id: element.dataValues.id },
      });
      await cartInactive.update({ status: false });

      //salva os dados da venda
      await freteModel.update({
        name: addressModel.name,
        postcode: addressModel.postcode,
        state: addressModel.state,
        city: addressModel.city,
        neighborhood: addressModel.neighborhood,
        street: addressModel.street,
        number: addressModel.number,
        complement: addressModel.complement,
        references: addressModel.references,
      })

      await Sales.create({
        hash: paymentDetail.IdTransaction,
        amount: element.amount,
        quota: 0,
        price: totalPrice,
        status: status,
        fk_grids: element.fk_grids,
        fk_users: req.userId,
        fk_provider: element.fk_provider,
        fk_payments: payment.id,
        fk_fretes: freteModel.id,
        /* fk_addresses: addressModel.id, */
        fk_cards: cardModel ? cardModel.id : null,
        payment_type,
      });

      var sales = await Sales.findOne({
        where: { hash: paymentDetail.IdTransaction.toString() },
        attributes: ['id'],
      });

      // boleto
      // if (payment_type === 1) {
      // }

      if (!Sales) {
        return res.json({
          error: {
            type: 'payment_error',
            parameter_name: 'checkout',
            message: pagamento.response.Error,
          },
        });
      }
    }
    await Progresses.create({
      type: 'received',
      situation: 'Pedido recebido',
      date: Date.now(),
      step: 0,
      fk_sales: sales.id,
      status: true
    })

    await Queue.add('OrderUpdateMail', {
      users: {
        name: personalData.name,
        email: userModel.email,
      },
      order: {
        order: sales.hash,
        status: "foi recebido com sucesso",
      },
    });



    if (paymentDetail.Status === 3) {
      //compra autorizada
      await Progresses.create({
        type: 'payment',
        situation: 'Pagamento Aprovado',
        date: Date.now(),
        step: 1,
        fk_sales: sales.id,
        status: true
      })
      for (const p of produtos){
        await Grids.decrement('amount', {
          by: p.qty,
          where: {id: p.fk_grids}
        })
      }

      await Queue.add('OrderUpdateMail', {
        users: {
          name: personalData.name,
          email: userModel.email,
        },
        order: {
          order: sales.hash,
          status: "teve o pagamento aprovado",
        },
      });
      
    }


    let holder_name = cardModel ? cardModel.holder_name : null;
    let first_digits = cardModel ? cardModel.first_digits : null;
    let resposta = {
      payment: {
        payment_type: payment_type,
        token: paymentDetail.Token,
        holder_name,
        first_digits,
        BankSlipUrl: paymentDetail.BankSlipUrl,
        DigitableLine: paymentDetail.DigitableLine,
        Barcode: paymentDetail.Barcode,
        DueDate: paymentDetail.DueDate,
        Message: paymentDetail.Message,
        Description: paymentDetail.Description,
        IdTransaction: paymentDetail.IdTransaction

      },
      address: {
        name: addressModel.name,
        postcode: addressModel.postcode,
        state: addressModel.state,
        city: addressModel.city,
        neighborhood: addressModel.neighborhood,
        street: addressModel.street,
        number: addressModel.number,
        complement: addressModel.complement,
        references: addressModel.references,
      },
      sale: {
        price: totalPrice,
        shipping: freteModel.valor
      }
    }

    return res.json(resposta);
  }
}

export default new CheckoutController();
