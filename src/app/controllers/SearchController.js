import { QueryTypes, Op, fn, col, Sequelize } from 'sequelize'
import Products from '../models/Products'
import Subcategories from '../models/Subcategories'
import Prodsubcats from '../models/Prodsubcats'
import Categories from '../models/Categories'
import Files from '../models/Files'
import Details from '../models/Details'
import Grids from '../models/Grids'

class SearchController {

    async search(req, res) {
        try {

            const queryList = await Products.findAll({
                where: {
                    [Op.or]: [{
                            name: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        },
                        {
                            description: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            }
                        },
                        {
                            model: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        },

                        {
                            brand: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        }
                    ],
                    status: true
                },
                include: [{
                        model: Categories,
                        as: 'categories',
                        attributes: ['id', 'name', 'status'],
                    },
                    {
                        model: Files,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                    {
                        model: Details,
                        as: 'details',
                        attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                    },
                ]
            })

            const promisesQueryList = queryList.map(async response => {

                const gridsList = await Grids.findAll({
                    where: { fk_products: response.id },
                    attributes: [
                        [fn('min', col('price')), 'minPrice'],
                        [fn('max', col('price')), 'maxPrice'],
                        [fn('sum', col('amount')), 'amount_stock']
                    ]
                })
                const { id, name, description, sku, brand, model, status, categories, file, details } = response
                return ({ id, name, status, categories, thumb: file.url, infos: gridsList[0] })

            })

            const products = await Promise.all(promisesQueryList);
            if (products.length == 0) {

                const categoriesList = await Categories.findAll({
                        where: {
                            [Op.or]: [{
                                name: {
                                    [Op.iLike]: '%' + req.query.content + '%'
                                },
                            }],
                            status: true
                        },
                    })
                    //Busca pelas categorias
                if (!categoriesList) {
                    let array = [];
                    const promisesCategoriesList = categoriesList.map(async response => {

                        const ProductList = await Products.findAll({
                            where: {
                                fk_categories: response.id,
                                status: true
                            },
                            include: [{
                                    model: Categories,
                                    as: 'categories',
                                    attributes: ['id', 'name', 'status'],
                                },
                                {
                                    model: Files,
                                    as: 'file',
                                    attributes: ['id', 'name', 'path', 'url', 'status'],
                                },
                                {
                                    model: Details,
                                    as: 'details',
                                    attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                                },
                            ]
                        })

                        const promisesProducts = ProductList.map(async response => {
                            array.push(response)
                            return (response)
                        })
                        const productsListMap = await Promise.all(promisesProducts);

                        return (productsListMap)
                    })
                    await Promise.all(promisesCategoriesList);
                    const promisesQueryList = array.map(async responseQL => {

                        const gridsList = await Grids.findAll({
                            where: { fk_products: responseQL.id, status: true },
                            attributes: [
                                [fn('min', col('price')), 'minPrice'],
                                [fn('max', col('price')), 'maxPrice'],
                                [fn('sum', col('amount')), 'amount_stock']
                            ]
                        })
                        const { id, name, description, sku, brand, model, status, categories, file, details } = responseQL
                        return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

                    })
                    const promiseQLL = await Promise.all(promisesQueryList);
                    return res.json(promiseQLL)
                }

            }

            return res.json(products)

        } catch ({ message, error }) {
            return res.json({ message })
        }
    }

    async SearchMyProducts(req, res) {
        try {

            const queryList = await Products.findAll({
                where: {
                    [Op.or]: [{
                            name: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        },
                        {
                            description: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            }
                        },
                        {
                            model: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        },

                        {
                            brand: {
                                [Op.iLike]: '%' + req.query.content + '%'
                            },
                        }
                    ],
                    status: true,
                    fk_users: req.userId
                },
                include: [{
                        model: Categories,
                        as: 'categories',
                        attributes: ['id', 'name', 'status'],
                    },
                    {
                        model: Files,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                    {
                        model: Details,
                        as: 'details',
                        attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                    },
                ]
            })

            const promisesQueryList = queryList.map(async response => {

                const gridsList = await Grids.findAll({
                    where: { fk_products: response.id },
                    attributes: [
                        [fn('min', col('price')), 'minPrice'],
                        [fn('max', col('price')), 'maxPrice'],
                        [fn('sum', col('amount')), 'amount_stock']
                    ]
                })
                const { id, name, description, sku, brand, model, status, categories, file, details } = response
                return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

            })

            const products = await Promise.all(promisesQueryList);
            if (products.length == 0) {

                const categoriesList = await Categories.findAll({
                        where: {
                            [Op.or]: [{
                                name: {
                                    [Op.iLike]: '%' + req.query.content + '%'
                                },
                            }],
                            status: true
                        },
                    })
                    //Busca pelas categorias
                if (!categoriesList) {
                    let array = [];
                    const promisesCategoriesList = categoriesList.map(async response => {

                        const ProductList = await Products.findAll({
                            where: {
                                fk_categories: response.id,
                                status: true
                            },
                            include: [{
                                    model: Categories,
                                    as: 'categories',
                                    attributes: ['id', 'name', 'status'],
                                },
                                {
                                    model: Files,
                                    as: 'file',
                                    attributes: ['id', 'name', 'path', 'url', 'status'],
                                },
                                {
                                    model: Details,
                                    as: 'details',
                                    attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                                },
                            ]
                        })

                        const promisesProducts = ProductList.map(async response => {
                            array.push(response)
                            return (response)
                        })
                        const productsListMap = await Promise.all(promisesProducts);

                        return (productsListMap)
                    })
                    await Promise.all(promisesCategoriesList);
                    const promisesQueryList = array.map(async responseQL => {

                        const gridsList = await Grids.findAll({
                            where: { fk_products: responseQL.id, status: true },
                            attributes: [
                                [fn('min', col('price')), 'minPrice'],
                                [fn('max', col('price')), 'maxPrice'],
                                [fn('sum', col('amount')), 'amount_stock']
                            ]
                        })
                        const { id, name, description, sku, brand, model, status, categories, file, details } = responseQL
                        return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

                    })
                    const promiseQLL = await Promise.all(promisesQueryList);
                    return res.json(promiseQLL)
                }

            }

            return res.json(products)

        } catch ({ message, error }) {
            return res.json({ message })
        }
    }

}

export default new SearchController();
