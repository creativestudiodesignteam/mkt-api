import 'dotenv'
import * as Yup from 'yup';
import Users from '../models/Users';
import Providers from '../models/Providers';
import Customers from '../models/Customers';
import { createHash } from 'crypto'
import Queue from '../../lib/Queue';
import { toDate, addMinutes, isBefore, parseISO } from 'date-fns'
/*import Files from '../models/Files'; */

class ForgotPasswordController {
  async verify_token(req, res) {
    const { token } = req.params
    const usersToken = await Users.findOne({
      where: { reset_password: token, status: true },
      attributes: ['id', 'email', 'reset_password', 'date_reset_password']
    })
    if (!usersToken) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "token",
          "message": "Token inválido, token inexistente"
        }
      })
    }
    const dateNow = toDate(Date.now()).toJSON()
    const validate = isBefore(parseISO(dateNow), addMinutes(usersToken.date_reset_password, 5))
    if (!validate) {
      return res.json({
        error: {
          "type": "is_invalid",
          "parameter_name": "datetime",
          "message": "Token inválido, tempo expirado !"
        }
      })
    }

    return res.json({ validation: validate });
  }

  async store(req, res) {

    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }

      });
    }

    const userExists = await Users.findOne({ where: { email: req.body.email }, attributes: ['id', 'email', 'type', 'status'] })
    if (!userExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "user",
          "message": "Usuário não existe"
        }

      });
    }
    let name;
    if (userExists.type) {
      const { fantasy_name } = await Providers.findOne({
        where: { fk_users: userExists.id },
        attributes: ['id', 'fantasy_name', 'status']
      })
      name = fantasy_name;
    } else {
      const { name: nameCustomer } = await Customers.findOne({
        where: { fk_users: userExists.id },
        attributes: ['id', 'name', 'status']
      })
      name = nameCustomer;
    }


    const dateNow = `${Date.now()}`

    const token = createHash('md5').update(dateNow).digest("hex");

    const date_reset_password = toDate(Date.now()).toJSON()

    const { reset_password } = await userExists.update({ reset_password: token, date_reset_password })

    await Queue.add('ForgotPasswordMail', {
      users: { email: userExists.email, name },
      config: {
        token: reset_password,
        url: process.env.FRONT_URL
      }

    });

    return res.json({ message: 'Verifique seu email para recuperar sua senha !' })
  }

  async update(req, res) {
    const { token } = req.body

    const schema = Yup.object().shape({
      new_password: Yup.string().min(6).required('Por favor digite sua senha'),
      new_confirmPassword: Yup.string().min(6).required('Por favor confirme sua senha'),
    });

    if (req.body.new_password !== req.body.new_confirmPassword) {
      return res.json({
        error: {
          "type": "password_not_match",
          "parameter_name": "password",
          "message": "As senhas são diferentes, tente novamente"
        }
      });
    }
    await schema.validate(req.body).catch(function (err) {
      return res.json({
        error: {
          "type": "validation_invalid_user",
          "parameter_name": "yup",
          "message": err.errors.toString()
        }
      });
    });

    const usersToken = await Users.findOne({
      where: { reset_password: token, status: true },
      attributes: ['id', 'email', 'reset_password', 'date_reset_password']
    })

    if (!usersToken) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "token",
          "message": "Token inválido, token inexistente"
        }
      })
    }

    const dateNow = toDate(Date.now()).toJSON()
    const validate = isBefore(parseISO(dateNow), addMinutes(usersToken.date_reset_password, 10))
    if (!validate) {
      return res.json({
        error: {
          "type": "is_invalid",
          "parameter_name": "datetime",
          "message": "Token inválido, tempo expirado !"
        }
      })
    }

    await usersToken.update({
      password: req.body.new_password,
      reset_password: null,
      date_reset_password: null
    })

    return res.json({ message: 'Usuario editado com sucesso' })
  }

}

export default new ForgotPasswordController();
