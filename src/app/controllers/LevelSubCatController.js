import * as Yup from 'yup';
import Products from '../models/Products';

import Prodsubcats from '../models/Prodsubcats';
import Sublevels from '../models/Sublevels';
import Levelsubcats from '../models/Levelsubcats';
import Subcategories from '../models/Subcategories';
/*import Files from '../models/Files'; */

class LevelsubcatsController {
  async index(req, res) {

    const levelsubcats = await Levelsubcats.findAll({

      where: { fk_prodsubcat: req.params.fk_prodsubcat, status: true },
      include: [
        {
          model: Prodsubcats,
          as: 'prodsubcat',
          attributes: ['id'],
          include: [
            {
              model: Subcategories,
              as: 'subcategories',
              attributes: ['id', 'name', 'status']
            },
          ]
        },
        {
          model: Sublevels,
          as: 'sublevels',
          attributes: ['id', 'name', 'status']
        }
      ]

    })

    return res.json(levelsubcats)
  }

  async store(req, res) {
    const prodSubCatsExists = await Prodsubcats.findOne({
      where: { id: req.body.fk_prodsubcat }
    })

    if (!prodSubCatsExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "prodsubcat",
          "message": "Sub Categoria já está relacionada a este produto."
        }
      })
    }

    const subLevelsExists = await Sublevels.findOne({
      where: { id: req.body.fk_sublevels }
    })

    if (!subLevelsExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "subLevelsExists",
          "message": "Sub nivel não existe."
        }
      })
    }


    const {
      id,
    } = await Levelsubcats.create(req.body);

    return res.json(
      await Levelsubcats.findOne({
        where: { id },
        attributes: ['id'],
        include: [
          {
            model: Prodsubcats,
            as: 'prodsubcat',
            attributes: ['id'],
            include: [
              {
                model: Subcategories,
                as: 'subcategories',
                attributes: ['id', 'name', 'status']
              },
            ]
          },
          {
            model: Sublevels,
            as: 'sublevels',
            attributes: ['id', 'name', 'status']
          }
        ]
      })
    );
  }

  async update(req, res) {
    const { id } = req.params

    const LevelsubcatsExists = await Levelsubcats.findOne({
      where: { id }
    })

    if (!LevelsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcatregories",
          "message": "Sub Categoria não está relacionada ao produto, tente novamente !"
        }
      })
    }
    const subcatregoriesExists = await Prodsubcats.findOne({
      where: { id: req.body.fk_prodsubcat }
    })
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcat",
          "message": "Sub Categoria não existe, tente novamente !"
        }

      })
    }
    const sublevelsExists = await Sublevels.findOne({
      where: { id: req.body.fk_sublevels }
    })
    if (!sublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevelsExists",
          "message": "Sub nivel não existe, tente novamente !"
        }
      })
    }

    const { id: idsublevel } = await LevelsubcatsExists.update(req.body);

    return res.json(
      await Levelsubcats.findOne({
        where: { id: idsublevel },
        attributes: ['id'],
        include: [
          {
            model: Prodsubcats,
            as: 'prodsubcat',
            attributes: ['id'],
            include: [
              {
                model: Subcategories,
                as: 'subcategories',
                attributes: ['id', 'name', 'status']
              },
            ]
          },
          {
            model: Sublevels,
            as: 'sublevels',
            attributes: ['id', 'name', 'status']
          }
        ]
      })
    );
  }

  async delete(req, res) {
    const { id } = req.params

    const LevelsubcatsExists = await Levelsubcats.findOne({
      where: { id, status: true },
    });
    if (!LevelsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "provider",
          "message": "Relação entre produto e sub categoria não existe, tente novamente !"
        }
      });
    }

    await LevelsubcatsExists.destroy();

    return res.json({ msg: 'Vinculo entre sub categoria e produto foi excluida' })
  }
}

export default new LevelsubcatsController();
