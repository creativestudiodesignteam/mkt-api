import jwt from 'jsonwebtoken';
import User from '../models/Users';
import Providers from '../models/Providers';
import Customers from '../models/Customers';
import authConfig from '../../config/auth';
import Queue from '../../lib/Queue';
import { createHash } from 'crypto';

import {
  toDate,
  addMinutes,
  isBefore,
  parseISO,
  differenceInMinutes,
} from 'date-fns';
class SessionController {
  async store(req, res) {
    req.body.email = req.body.email.toLowerCase()
    const { email, password } = req.body;
    var status = 'Success';
    const user = await User.findOne({ where: { email, type: req.body.type } });

    if (!user) {
      return res.status(401).json({
        error: {
          type: 'incorrect_auth',
          parameter_name: 'auth',
          message: 'Login Incorreto, tente novamente.',
        },
      });
    }

    const { id, type, timeout, activation_at } = user;
    let { attempts } = user;
    const dateNow = toDate(Date.now()).toJSON();
    const validate = isBefore(parseISO(dateNow), addMinutes(timeout, 30));

    if (attempts >= 3 && validate) {
      const remaining = differenceInMinutes(
        addMinutes(timeout, 30),
        parseISO(dateNow)
      );
      return res.status(401).json({
        error: {
          type: 'timeout_login',
          parameter_name: 'timeout',
          message: `Espere ${remaining} minutos para fazer login novamente!`,
        },
      });
    } else if (attempts >= 3) {
      attempts = 0;
      user.update({ attempts: 0 });
    }

    if (!(await user.checkPassword(password))) {
      user.update({ attempts: attempts + 1, timeout: dateNow });
      return res.status(401).json({
        error: {
          type: 'incorrect_auth',
          parameter_name: 'auth',
          message: 'Login Incorreto, tente novamente.',
        },
      });
    }

    if (user.token_email && user.activation_at == null) {
      const dateNow = `${Date.now()}`;

      const token_email = createHash('md5')
        .update(dateNow)
        .digest('hex');
<<<<<<< HEAD
      const teste = await user.update({
=======

      const userUpdate = await user.update({
>>>>>>> develop
        token_email,
      });

      let name;
      if (user.type) {
        const provider = await Providers.findOne({
          where: { fk_users: user.id },
        });
        name = provider.fantasy_name;
      } else {
        const customer = await Customers.findOne({
          where: { fk_users: user.id },
        });
        name = customer.name;
      }

      await Queue.add('ActivateMailer', {
        users: { name, email: user.email },
        config: {
<<<<<<< HEAD
          token: teste.token_email,
          url: process.env.FRONT_URL,
=======
          token: userUpdate.token_email,
>>>>>>> develop
        },
      });


      return res.status(401).json({
        error: {
          type: 'account_not_activate',
          parameter_name: 'activate',
          message: 'Por favor, verifique seu email para ativar sua conta.',
        },
      });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({
        error: {
          type: 'incorrect_password',
          parameter_name: 'password',
          message: 'Senha incorreta.',
        },
      });
    }

    var name;
    if (type) {
      const providerExist = await Providers.findOne({
        where: { fk_users: id, status: true },
      });
      if (!providerExist) {
        status = 'cadastro de fornecedor está incompleto';
      } else {
        name = providerExist.fantasy_name;
      }
    } else {
      const customerExist = await Customers.findOne({
        where: { fk_users: id, status: true },
      });
      if (!customerExist) {
        status = 'cadastro de cliente está incompleto';
      } else {
        name = customerExist.name;
      }
    }
    await user.update({ attempts: 0 });
    return res.json({
      name: name,
      status: status,
      token: jwt.sign({ id, name, email, type, status }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
