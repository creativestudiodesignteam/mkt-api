import * as Yup from 'yup';
import Providers from './../models/Providers'
import Users from './../models/Users'
import Addresses from './../models/Addresses'

const safe2pay = require('safe2pay');
safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY_SANDBOX);

class SubAccountsController {

  async store(req, res) {
    const MarketplaceRequest = safe2pay.api.MarketplaceRequest;
    const providers = await Providers.findOne({
      where: { fk_users: req.userId },
      attributes: ['id', 'fantasy_name', 'reason_social', 'state_register', 'county_register', 'telephone_commercial', 'telephone_whatsapp', 'is_panel_restricted', 'status'],
      include: [
        {
          model: Users,
          as: 'users',
          attributes: ['id', 'email']
        },
      ]
    })

    const addresses = await Addresses.findOne({
      where: { fk_users: req.userId, status: true }
    })

    const data = {
      "Name": providers.fantasy_name,
      "CommercialName": providers.reason_social,
      "Identity": providers.cnpj,
      "ResponsibleName": providers.users.name,
      "ResponsibleIdentity": providers.users.cpf,
      "Email": providers.users.email,
      "BankData": {
        "Bank": {
          "Id": 0,
          "Code": "041"
        },
        "AccountType": {
          "Code": "CC"
        },
        "BankAgency": "1676",
        "BankAgencyDigit": "0",
        "BankAccount": "41776",
        "BankAccountDigit": "7"
      },
      "Address": {
        "ZipCode": addresses.postcode,
        "Street": addresses.street,
        "Number": addresses.number,
        "Complement": addresses.complement,
        "District": addresses.neighborhood,
        "CityName": addresses.city,
        "StateInitials": addresses.state,
        "CountryName": 'Brasil'
      },
      "MerchantSplit": [
        {
          "PaymentMethodCode": "1",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        },
        {
          "PaymentMethodCode": "3",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            },
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        },
        {
          "PaymentMethodCode": "4",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        }
      ]
    }

    const teste = await MarketplaceRequest.Add(data);
    return res.json(teste)

  }


}

export default new SubAccountsController();
