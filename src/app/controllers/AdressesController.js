import * as Yup from 'yup';
import Addresses from '../models/Addresses';
import Users from '../models/Users';

class AddressesController {

  async index(req, res) {
    try {
      const addresses = await Addresses.findAll({
        where: { fk_users: req.userId, status: true },
        attributes: [
          'id',
          'name',
          'postcode',
          'state',
          'city',
          'neighborhood',
          'street',
          'number',
          'complement',
          'references',
          'status',
          'select_at',
          'selected_store'
        ],
      });

      return res.json(addresses);
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async findBySelect(req, res) {
    try {
      const addresses = await Addresses.findOne({
        where: { fk_users: req.userId, status: true },
        attributes: [
          'id',
          'name',
          'postcode',
          'state',
          'city',
          'neighborhood',
          'street',
          'number',
          'complement',
          'references',
          'status',
          'select_at',
          'selected_store'
        ],
      });

      return res.json(addresses);

    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }

  }

  async store(req, res) {
    try {
      /* name postcode state city neighborhood street number complement */
      /* req.body.fk_users = req.userId; */

      const schema = Yup.object().shape({
        name: Yup.string().required(),
        postcode: Yup.string().required(),
        state: Yup.string().required(),
        city: Yup.string().required(),
        neighborhood: Yup.string().required(),
        street: Yup.string().required(),
        number: Yup.string().required(),
        complement: Yup.string(),
        references: Yup.string(),
      });
      if (!(await schema.isValid(req.body))) {
        return res.json({
          error: {
            type: 'validation_invalid',
            parameter_name: 'yup',
            message: 'Digitou todos os campos certinhos ?, tente novamente',
          },
        });
      }
      const fk_userExists = await Users.findByPk(req.body.fk_users);

      if (!fk_userExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'user',
            message: 'Usuario não existe, tente novamente!',
          },
        });
      }

      const addresseChecking = await Addresses.findAll({
        where: { fk_users: req.body.fk_users }
      })
      if (addresseChecking.length <= 0) {
        req.body.select_at = true
      }

      const {
        id,
        postcode,
        number,
        state,
        neighborhood,
        city,
        street,
        complement,
        references,
        status,
        select_at,
        selected_store
      } = await Addresses.create(req.body);

      return res.json({
        id,
        postcode,
        number,
        state,
        neighborhood,
        city,
        street,
        complement,
        references,
        status,
        select_at,
        selected_store
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async update(req, res) {
    try {
      const { id } = req.params;
      req.body.fk_users = req.userId;

      const schema = Yup.object().shape({
        postcode: Yup.string(),
        state: Yup.string(),
        city: Yup.string(),
        neighborhood: Yup.string(),
        street: Yup.string(),
        number: Yup.string(),
        complement: Yup.string(),
        references: Yup.string(),
      });

      if (!(await schema.isValid(req.body))) {
        return res.json({
          error: {
            type: 'validation_invalid',
            parameter_name: 'yup',
            message: 'Digitou todos os campos certinhos ?, tente novamente',
          },
        });
      }

      const addressesExists = await Addresses.findOne({
        where: { id, fk_users: req.body.fk_users },
      });
      if (!addressesExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'addresses',
            message: 'Endreço não existe, tente novamente!',
          },
        });
      }

      const {
        postcode,
        number,
        state,
        neighborhood,
        city,
        street,
        complement,
        references,
        status,
        select_at,
      } = await addressesExists.update(req.body);

      return res.json({
        id,
        postcode,
        number,
        state,
        neighborhood,
        city,
        street,
        complement,
        references,
        status,
        select_at,
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async selectUpdate(req, res) {
    try {
      const { fk_new_addresses } = req.body;

      const addressesExists = await Addresses.findOne({
        where: { fk_users: req.userId, select_at: true, status: true },
      });

      if (addressesExists) {
        await addressesExists.update({ select_at: false, status: true });
      }

      const addressesNewExists = await Addresses.findOne({
        where: { id: fk_new_addresses, fk_users: req.userId },
      });

      const {
        id,
        name,
        postcode,
        state,
        city,
        neighborhood,
        street,
        number,
        complement,
        references,
        status,
        select_at,
        selected_store
      } = await addressesNewExists.update({ select_at: true });

      return res.json({
        id,
        name,
        postcode,
        state,
        city,
        neighborhood,
        street,
        number,
        complement,
        references,
        status,
        select_at,
        selected_store
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async select_store(req, res) {
    try {
      const checkUser = await Users.findOne({
        where: { id: req.userId, type: true }
      })

      if (!checkUser) {
        throw {
          toError: function () {
            return {
              type: 'not_exists',
              parameter_name: 'user',
              error: 'Você não tem permissão para ultilizar esta funcionalidade!',
            };
          },
        };
      }

      const { fk_new_addresses } = req.params;


      const addressesExists = await Addresses.findOne({
        where: { fk_users: req.userId, selected_store: true, status: true },
      });

      const checkAddresses = await Addresses.findOne({
        where: { id: fk_new_addresses, fk_users: req.userId, status: true },
      });

      if (!checkAddresses) {
        throw {
          toError: function () {
            return {
              type: 'not_exists',
              parameter_name: 'addresses',
              error: 'Endereço não existe, por favor efetue um cadastro!',
            };
          },
        };
      }


      if (addressesExists) {
        await addressesExists.update({ selected_store: false, status: true });
      }

      const addressesNewExists = await Addresses.findOne({
        where: { id: fk_new_addresses, fk_users: req.userId },
      });

      const {
        id,
        name,
        postcode,
        state,
        city,
        neighborhood,
        street,
        number,
        complement,
        references,
        status,
        select_at,
        selected_store
      } = await addressesNewExists.update({ selected_store: true });

      return res.json({
        id,
        name,
        postcode,
        state,
        city,
        neighborhood,
        street,
        number,
        complement,
        references,
        status,
        select_at,
        selected_store
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }
  async delete(req, res) {
    try {


      const { id } = req.params;

      const checkLengthAdresses = await Addresses.count({
        where: { fk_users: req.userId, status: true }
      })

      if (checkLengthAdresses <= 1) {
        throw {
          toError: function () {
            return {
              type: 'addresses_length',
              parameter_name: 'addresses',
              error: 'Você não pode deletar este endereço',
            };
          },
        };
      }

      const addressesExists = await Addresses.findOne({
        where: { id, status: true, fk_users: req.userId },
      });

      if (!addressesExists) {
        throw {
          toError: function () {
            return {
              type: 'not_exists',
              parameter_name: 'addresses',
              error: 'Endreço não existe, tente novamente!',
            };
          },
        };
      }
      if (!!addressesExists.select_at) {
        throw {
          toError: function () {
            return {
              type: 'select_at',
              parameter_name: 'addresses',
              error: 'Não é permitido remover o seu endereço de entrega principal, torne outro endereço principal se deseja excluir este!',
            };
          },
        };
      }
      if (!!addressesExists.selected_store) {
        throw {
          toError: function () {
            return {
              type: 'select_at',
              parameter_name: 'addresses',
              error: 'Não é permitido remover o seu endereço da sua loja, torne outro endereço principal se deseja excluir este!',
            };
          },
        };
      }

      await addressesExists.destroy();

      return res.json({ msg: 'Endereço excluido' });


    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }
}

export default new AddressesController();
