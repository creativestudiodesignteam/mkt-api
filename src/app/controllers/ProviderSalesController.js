import Sales from '../models/Sales';
import OptionsGrids from '../models/OptionsGrids';
import Grids from '../models/Grids';
import Products from '../models/Products';
import Files from '../models/Files';
import Providers from '../models/Providers';
import Payments from '../models/Payments';

class ProviderSalesController {

  async index(req, res) {
    const { fk_provider } = req.params;
    const sales = await Sales.findAll({
      where: { fk_provider: fk_provider, status: true },
      attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status'],
      include: [
        {
          model: Grids,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: Products,
              as: 'products',
              attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
              include: [
                {
                  model: Files,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: Providers,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name', 'reason_social', 'state_register',
            'county_register', 'telephone_commercial', 'telephone_whatsapp', 'token',
            'idcode', 'status', 'data_fundacao', 'qty_funcionarios', 'site', 'modalidade'],
        },
        {
          model: Payments,
          as: 'payments',
          attributes: ['id', 'code', 'method', 'situations', 'status'],
        },
      ],
    });

    if (!sales) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'sales',
          'message': 'venda não existe.',
        },
      });
    }

    return res.json({ sales });
  }

  async detail(req, res) {
    const { hash } = req.params;
    return res.json(await Sales.findOne({
        where: { hash: hash },
        attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status'],
      group: ['hash'],
        include: [
          {
            model: Grids,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: Products,
                as: 'products',
                attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                include: [
                  {
                    model: Files,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
          {
            model: Providers,
            as: 'providers',
            attributes: ['id', 'cnpj', 'fantasy_name', 'reason_social', 'state_register',
              'county_register', 'telephone_commercial', 'telephone_whatsapp', 'token',
              'idcode', 'status', 'data_fundacao', 'qty_funcionarios', 'site', 'modalidade'],
          },
          {
            model: Payments,
            as: 'payments',
            attributes: ['id', 'code', 'method', 'situations', 'status'],
          },
        ],
      }),
    );
  }

  async delete(req, res) {
    const { hash } = req.params;

    const salesExists = await Sales.findAll({
      where: { hash: hash },
      group: ['hash'],
    });

    if (!salesExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'addresses',
          'message': 'Endreço não existe, tente novamente !',
        },
      });
    }

    await salesExists.update({ status: false });

    return res.json({ msg: 'venda excluída' });
  }
}

export default new ProviderSalesController();
