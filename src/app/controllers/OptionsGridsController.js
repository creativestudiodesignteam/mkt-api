import * as Yup from 'yup';
import OptionsGrids from '../models/OptionsGrids';
import Options from '../models/Options';
import OptionsNames from '../models/OptionsNames';
import Grids from '../models/Grids';
import Products from '../models/Products';



/*import Files from '../models/Files'; */

class OptionsGridsController {

    async index(req, res) {
        const { fk_grids } = req.params;
        /* const grids = await Grids.findByPk(fk_grids); */
        const grids = await Grids.findOne({

            attributes: ['id', 'status'],
            where: {
                id: fk_grids,
                status: true,
            },

            include: [{
                        model: Products,
                        as: 'products',
                        attributes: ['id', 'name', 'description', 'brand', 'model'],
                        where: { status: true },
                    },

                ]
                /* attributes: ['id', 'name', 'status',] */
        })

        const optionsGrids = await OptionsGrids.findAll({
            attributes: ['id', 'status'],
            where: {
                fk_grids,
                status: true,
            },

            include: [{
                        model: Options,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: OptionsNames,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: Grids,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]
                /* attributes: ['id', 'name', 'status',] */
        })

        if (!optionsGrids) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "grid",
                    "message": "Relação dos produtos em grades não existe, tente novamente !"
                }
            });
        }

        return res.json([{ grids, optionsGrids }]);

    }

    async store(req, res) {
        req.body.fk_users = req.userId;

        const schema = Yup.object().shape({
            fk_option: Yup.number(),
            fk_options_names: Yup.number(),
            fk_grids: Yup.number(),
        });


        if (!(await schema.isValid(req.body))) {
            return res.json({
                error: {
                    "type": "validation_invalid",
                    "parameter_name": "yup",
                    "message": "Digitou todos os campos certinhos ?, tente novamente"
                }
            });
        }
        const GridsExists = await Grids.findByPk(req.body.fk_grids);
        if (!GridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "grid",
                    "message": "Relação dos produtos em grades não existe, tente novamente !"
                }
            });
        }

        const OptionsExists = await Options.findByPk(req.body.fk_options);
        if (!OptionsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }

            });
        }

        const {
            id,
            fk_options,
            fk_options_names,
            fk_grids,
            fk_users
        } = await OptionsGrids.create(req.body);


        return res.json(
            await OptionsGrids.findOne({
                where: { id, fk_users },
                attributes: ['id', 'status'],
                include: [{
                        model: Options,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: OptionsNames,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: Grids,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]

            })
        );
    }

    async update(req, res) {
        const { id } = req.params

        const schema = Yup.object().shape({
            name: Yup.string(),
        });
        if (!(await schema.isValid(req.body))) {
            return res.json({
                error: {
                    "type": "validation_invalid",
                    "parameter_name": "yup",
                    "message": "Digitou todos os campos certinhos ?, tente novamente"
                }
            });
        }
        const OptionsGridsExists = await OptionsGrids.findOne({
            where: { id, status: true },
        });
        if (!OptionsGridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }

            })
        }
        await OptionsGridsExists.update(req.body);

        return res.json(
            await OptionsGrids.findOne({
                where: { id, fk_users },
                attributes: ['id', 'status'],
                include: [{
                        model: Options,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: OptionsNames,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: Grids,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]

            }));
    }

    async delete(req, res) {
        const { id } = req.params
        const OptionsGridsExists = await OptionsGrids.findOne({
            where: { id, fk_users: req.userId, status: true },
        });
        if (!OptionsGridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }
            });
        }

        await OptionsGridsExists.update({ status: false });

        return res.json({ msg: 'Opção deletada' })

    }
}

export default new OptionsGridsController();
