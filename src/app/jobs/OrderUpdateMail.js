import 'dotenv'
import Mail from '../../lib/Mail';

export default {
	key: 'OrderUpdateMail',
	async handle({ data }) {
		const { users, order } = data;
		console.log(data)

		await Mail.sendMail({
			to: `${users.email}`,
			subject: 'Portal Da Maria | Acompanhe seu pedido',
			template: 'orderupdate',
			context: {
				name: users.name,
				order: order.order,
				status: order.status,
				url: process.env.FRONT_URL
			},
		});
	},
};
