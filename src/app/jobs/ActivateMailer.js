import 'dotenv'
import Mail from '../../lib/Mail';

export default {
  key: 'ActivateMailer',
  options: {
    attempts: 3,
    delay: 5000
  },
  async handle({ data }) {
    const { users, config } = data;
    console.log(data)

    await Mail.sendMail({
      to: `${users.email}`,
      subject: 'Portal Da Maria | Ativar Conta',
      template: 'activate',
      context: {
        name: users.name,
        token: config.token,
        url: process.env.FRONT_URL
      },
    });
  },
};


