export { default as ForgotPasswordMail } from './ForgotPasswordMail';
export { default as CustomerMail } from './CustomerMail';
export { default as WelcomeMail } from './WelcomeMail';
export { default as OrderUpdateMail } from './OrderUpdateMail';
export { default as ActivateMailer } from './ActivateMailer';
