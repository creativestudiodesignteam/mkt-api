import Mail from '../../lib/Mail';

export default {
  key: 'CreateProviderMail',
  async handle({ data }) {
    const { user } = data;
    await Mail.sendMail({
      to: `${provider.name} <${provider.email}>`,
      subject: 'Seja bem vindo ao portal da',
      template: 'createprovider',
      context: {
        provider: provider.name,
      },
    });

  },
};

    /*     await Mail.sendMail({
          from: 'Queue Test <queue@queuetest.com.br>',
          to: `${user.name} <${user.email}>`,
          subject: 'Cadastro de usuário',
          html: `Olá, ${user.name}, bem-vindo ao sistema de filas da Rocketseat :D`
        }); */

