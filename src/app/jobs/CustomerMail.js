import 'dotenv'
import Mail from '../../lib/Mail';

export default {
  key: 'CustomerMail',
  options: {
    attempts: 3,
    delay: 5000
  },
  async handle({ data }) {
    const { users, config } = data;
    console.log(data)

    await Mail.sendMail({
      to: `${users.email}`,
      subject: 'Portal Da Maria | Bem Vindo',
      template: 'welcomecustomer',
      context: {
        name: users.name,
        token: config.token,
        url: process.env.FRONT_URL
      },
    });
  },
};


