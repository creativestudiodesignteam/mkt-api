import Mail from '../../lib/Mail';
import 'dotenv'
export default {
  key: 'ForgotPasswordMail',
  options: {
    attempts: 3,
    delay: 5000
  },

  async handle({ data }) {
    const { users, config } = data;
    await Mail.sendMail({
      to: `${users.email}`,
      subject: 'Portal Da Maria | Recuperar Senha',
      template: 'forgotpassword',
      context: {
        name: users.name,
        token: config.token,
        url: process.env.FRONT_URL
      },
    });
  },
};
