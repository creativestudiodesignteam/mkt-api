import express from 'express';
import path from 'path';
import cors from 'cors';
import routes from './routes';
import './database/index';

class App {
  constructor() {
    this.server = express();
    this.middlewares();
    this.routes();
  }

  middlewares() {
    // set this servers
    this.server.use(express.json());

    this.server.use("/files", express.static(path.resolve(__dirname, "..", "tmp", "uploads")))
    this.server.use("/invoices", express.static(path.resolve(__dirname, "..", "tmp", "uploads", "invoices")))

    this.server.use(cors({
      origin: '*'
    }));
  }

  routes() {
    this.server.use(routes);
  }
}

export default new App().server;
