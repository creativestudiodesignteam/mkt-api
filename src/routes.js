import { Router } from 'express';
import multer from 'multer';
import multerConfig from './config/multer';
import multerCSV from './config/multerCSV';

import SessionController from './app/controllers/SessionController';
import authMiddware from './app/middlewares/auth';
import BullBoard from 'bull-board';
import Queue from './lib/Queue';
/* ==============================CONTROLLERS================================= */

import AdressesController from './app/controllers/AdressesController';
import UserController from './app/controllers/UserController';
import FileController from './app/controllers/FileController';
import ProvidersController from './app/controllers/ProvidersController';
import CustomersController from './app/controllers/CustomersController';
import ProductsController from './app/controllers/ProductsController';
import CategoriesController from './app/controllers/CategoriesController';
import SubCategoriesController from './app/controllers/SubCategoriesController';
import GalleriesController from './app/controllers/GalleriesController';
import DetailsController from './app/controllers/DetailsController';
import CheckoutController from './app/controllers/CheckoutController';
import OptionsControllers from './app/controllers/OptionsControllers';
import OptionsNameController from './app/controllers/OptionsNameController';
import GridsController from './app/controllers/GridsController';
import OptionsGridsController from './app/controllers/OptionsGridsController';
import CartsController from './app/controllers/CartsController';
import ProdsubcatsController from './app/controllers/ProdsubcatsController';
import DatasheetController from './app/controllers/DatasheetController';
import ForgotPasswordController from './app/controllers/ForgotPasswordController';
import Safe2PayController from './app/controllers/Safe2PayController';
import SubAccountController from './app/controllers/SubAccountController';
import SalesController from './app/controllers/SalesController';
import ProviderSalesController from './app/controllers/ProviderSalesController';
import CorreiosController from './app/controllers/CorreiosController';
import SublevelsController from './app/controllers/SublevelsController';
import LevelSubCatController from './app/controllers/LevelSubCatController';
import HomeController from './app/controllers/HomeController';
import SearchController from './app/controllers/SearchController';
import FormContactController from './app/controllers/FormContactController';
import BankController from './app/controllers/BankController';
import CardsController from './app/controllers/CardsController';
import TesteController from './app/controllers/TesteController';
import InvoiceController from './app/controllers/InvoiceController';
const routes = new Router();
BullBoard.setQueues(Queue.queues.map(queue => queue.bull));

/* ========== Config the multer for upload images ========== */
const upload = multer(multerConfig);
const InvoiceUpload = multer(multerCSV);

routes.use('/admin/queues', BullBoard.UI);
routes.post('/files', upload.single('file'), FileController.store);
<<<<<<< HEAD

routes.post('/categoriesCreate', TesteController.createCategoriesAll);

=======
routes.post('/queue', TesteController.QueueTest);
routes.post('/others', TesteController.OthersCreate);
>>>>>>> develop
routes.post('/forgot_password', ForgotPasswordController.store);
routes.put('/forgot_password/reset', ForgotPasswordController.update);
routes.get(
  '/forgot_password/verify_token/:token',
  ForgotPasswordController.verify_token
);

routes.put('/activate_user', UserController.activate);
routes.get('/search', SearchController.search);

routes.get('/bank/codes', BankController.bankCode);

/* ========== User Register and addresses ========== */
routes.post('/users', UserController.store);
routes.post('/check_email', UserController.checkEmail);
routes.post('/check_cnpj', UserController.checkCNPJ);
routes.post('/customers', CustomersController.store);
routes.post('/providers', ProvidersController.store);
routes.post('/addresses', AdressesController.store);

routes.get('/galleries/:fk_grids', GalleriesController.findGrid);

routes.get(
  '/categories/all',
  CategoriesController.findSubcategoriesByCategories
);

routes.post(
  '/create/categories',
  TesteController.createCategoriesAll
);

routes.get(
  '/home/:fk_categories/:limit',
  HomeController.findProductByCategories
);
routes.get(
  '/products/categories/:fk_categories',
  CategoriesController.findProductByCategories
);
routes.get('/v2/products/find/:id', ProductsController.findByIdV2);
routes.get(
  '/v2/options/products/:fk_products',
  OptionsControllers.findByProducts
);
routes.get('/products/find/:id', ProductsController.findById);

routes.get('/prodsubcats/:fk_subcategories', ProdsubcatsController.index);

routes.get('/subcategories/:fk_categories', SubCategoriesController.index);
routes.get('/sublevels/:fk_subcategories', SublevelsController.index);

routes.get('/products/related/:fk_products/:limit', ProductsController.related);
/* ========== Testing if the user is logged in ========== */
routes.post('/contact', FormContactController.store);

routes.get('/categories', CategoriesController.index);
routes.get('/details/:id', DetailsController.index);
routes.post('/shipping_product', CorreiosController.calculoPrazoProduto);

/* ========== Session authentication JWT ========== */
routes.post('/auth', SessionController.store);

routes.use(authMiddware);

routes.put('/users', UserController.update);
routes.get('/users', UserController.index);
routes.get('/activate_user/resend', UserController.resendTokenActivate);

routes.post('/subaccount', SubAccountController.store);

routes.get('/providers', ProvidersController.index);
routes.post('/providers', ProvidersController.store);
routes.put('/providers/:id', ProvidersController.update);
routes.delete('/providers/:id', ProvidersController.delete);

routes.get('/addresses', AdressesController.index);
routes.get('/addresses/select', AdressesController.findBySelect);
routes.put('/addresses/select', AdressesController.selectUpdate);
routes.patch('/addresses/store/:fk_new_addresses', AdressesController.select_store);
routes.put('/addresses/:id', AdressesController.update);
routes.delete('/addresses/:id', AdressesController.delete);

routes.get('/customers', CustomersController.index);
routes.put('/customers/:id', CustomersController.update);
routes.delete('/customers/:id', CustomersController.delete);

/* routes.post('/products/pictures/:fk_grids', upload.array('files'), PicturesController.store); */

routes.get('/products', ProductsController.index);
routes.get('/products/:id', ProductsController.findByIdReqUser);
routes.post('/products', ProductsController.store);
routes.put('/products/:id', ProductsController.update);
routes.delete('/products/:id', ProductsController.delete);

routes.post('/gallery', upload.array('files'), GalleriesController.store);

routes.put('/gallery/:id', upload.single('file'), GalleriesController.update);

routes.delete('/gallery/:id', GalleriesController.delete);

routes.post('/categories', CategoriesController.store);
routes.put('/categories/:id', CategoriesController.update);
routes.delete('/categories/:id', CategoriesController.delete);

routes.post('/subcategories', SubCategoriesController.store);
routes.put('/subcategories/:id', SubCategoriesController.update);
routes.delete('/subcategories/:id', SubCategoriesController.delete);

routes.post('/sublevels', SublevelsController.store);
routes.put('/sublevels/:id', SublevelsController.update);
routes.delete('/sublevels/:id', SublevelsController.delete);

routes.post('/sublevels', SublevelsController.store);
routes.put('/sublevels/:id', SublevelsController.update);
routes.delete('/sublevels/:id', SublevelsController.delete);

routes.post('/prodsubcats', ProdsubcatsController.store);
routes.put('/prodsubcats/:id', ProdsubcatsController.update);
routes.delete('/prodsubcats/:id', ProdsubcatsController.delete);

routes.get('/levelprodsubcat/:fk_prodsubcat', LevelSubCatController.index);
routes.post('/levelprodsubcat', LevelSubCatController.store);
routes.put('/levelprodsubcat/:id', LevelSubCatController.update);
routes.delete('/levelprodsubcat/:id', LevelSubCatController.delete);

routes.get('/options', OptionsControllers.index);
routes.post('/options', OptionsControllers.store);
routes.put('/options/:id', OptionsControllers.update);
routes.delete('/options/:id', OptionsControllers.delete);

routes.get('/options_name/:fk_options', OptionsNameController.index);
routes.post('/options_name', OptionsNameController.store);
routes.put('/options_name/:id', OptionsNameController.update);
routes.delete('/options_name/:id', OptionsNameController.delete);

routes.get('/options_grids/:fk_grids', OptionsGridsController.index);
routes.post('/options_grids', OptionsGridsController.store);
routes.put('/options_grids/:id', OptionsGridsController.update);
routes.delete('/options_grids/:id', OptionsGridsController.delete);

routes.get('/grids/:fk_products', GridsController.index);
routes.post('/grids', GridsController.store);
routes.put('/grids/:id', GridsController.update);
routes.delete('/grids/:id', GridsController.delete);

routes.post('/details', DetailsController.store);
routes.put('/details/:id', DetailsController.update);
routes.delete('/details/:id', DetailsController.delete);

routes.post('/checkout', CheckoutController.store);

routes.get('/carts', CartsController.index);
routes.post('/carts', CartsController.store);
routes.put('/carts/:id', CartsController.update);
routes.delete('/carts/:id', CartsController.delete);
routes.put('/carts/addresses/:id', CartsController.addressSelectedUpdate);

routes.get('/datasheet/:fk_products', DatasheetController.index);
routes.post('/datasheet/:fk_products', DatasheetController.store);
routes.put('/datasheet/:id', DatasheetController.update);
routes.delete('/datasheet/:id', DatasheetController.delete);

routes.post('/safe2pay/webhook', Safe2PayController.webhook);

routes.get('/creditcard', CardsController.index);
routes.post('/creditcard', CardsController.post);
routes.delete('/creditcard/:id', CardsController.delete);
routes.get('/creditcard/find/:id', CardsController.find);
routes.put('/creditcard/select', CardsController.cardSelectedUpdate);
routes.get('/installments', CardsController.installments);

routes.get('/sales/detail/:id', SalesController.detail);

routes.get('/sales/my_sales', SalesController.my_sales);
routes.get('/sales/nfe/:id', SalesController.detailSales);
routes.get('/sales/my_requests', SalesController.my_requests);

routes.delete('/sales/provider/:hash', SalesController.delete);
//Nota Fiscal
routes.post(
  '/sales/invoice',
  InvoiceUpload.single('file'),
  InvoiceController.create
);
routes.patch('/sales/nfe/:fk_sales', SalesController.addNfe);
routes.patch('/sales/tracking/:id', SalesController.addTracking);

routes.get('/buscacep/:cep', CorreiosController.buscaCep);
routes.get('/shipping', CorreiosController.calculoFrete);
routes.get('/tracking', CorreiosController.rastreamento);
routes.get('/calculo_frete/:type', CorreiosController.calculoFrete);
routes.get(
  '/calculo_prazo_produto/:type',
  CorreiosController.calculoPrazoProduto
);
routes.post('/salva_frete', CorreiosController.storeFrete);
routes.post('/shipping', CorreiosController.storeFrete);

routes.get('/provider_sales/:fk_provider', ProviderSalesController.index);
routes.get('/provider_sales/:hash', ProviderSalesController.detail);
routes.delete('/provider_sales/:hash', ProviderSalesController.delete);

routes.get('/search/provider', SearchController.SearchMyProducts);

export default routes;
