import multer from 'multer';
import crypto from 'crypto';

import { extname, resolve } from 'path';

export default {
  storage: multer.diskStorage({
    destination: resolve(__dirname, '..', '..', 'tmp', 'uploads', 'invoices'),
    path: resolve(__dirname, '..', '..', 'tmp', 'uploads', 'invoices'),

    filename: (req, file, cb) => {
/*       if (!extname(file.originalname).includes(['pdf', 'csv', 'xlsx', 'xlsm'])) {
        return cb('Extensão do arquivo inválida !')
      } */
      crypto.randomBytes(16, (err, res) => {
        if (err) return cb(err);
        return cb(null, res.toString('hex') + extname(file.originalname));
      });

    },
  }),
};
