import CreditCards from '../app/models/CreditCards';

require('dotenv/config');
import Sequelize from 'sequelize';
import mongoose from 'mongoose';
import db from '../config/database';

import Users from '../app/models/Users';
import Files from '../app/models/Files';
import Addresses from '../app/models/Addresses';
import Providers from '../app/models/Providers';
import Customers from '../app/models/Customers';
import Products from '../app/models/Products';
import Categories from '../app/models/Categories';
import Subcategories from '../app/models/Subcategories';
import Pictures from '../app/models/Pictures';
import Orders from '../app/models/Orders';
import Details from '../app/models/Details';
import Options from '../app/models/Options';
import OptionsNames from '../app/models/OptionsNames';
import OptionsGrids from '../app/models/OptionsGrids';
import Grids from '../app/models/Grids';
import Galleries from '../app/models/Galleries';
import Carts from '../app/models/Carts';
import Prodsubcats from '../app/models/Prodsubcats';
import Datasheets from '../app/models/Datasheets';
import Sublevels from '../app/models/Sublevels';
import Levelsubcats from '../app/models/Levelsubcats';
import Payments from '../app/models/Payments';
import Sales from '../app/models/Sales';
import Fretes from '../app/models/Fretes';
import Responsibles from '../app/models/Responsibles';
import ProviderRamos from '../app/models/ProviderRamos';
import FormContacts from '../app/models/FormContacts';
import Invoices from '../app/models/Invoices';
import Progresses from '../app/models/Progresses';



const models = [
    Users,
    Files,
    Addresses,
    Providers,
    Customers,
    Products,
    Categories,
    Subcategories,
    Pictures,
    Orders,
    Details,
    Options,
    OptionsNames,
    OptionsGrids,
    Grids,
    Galleries,
    Carts,
    Prodsubcats,
    Datasheets,
    Sublevels,
    Levelsubcats,
    Payments,
    Sales,
    CreditCards,
    Fretes,
    Responsibles,
    ProviderRamos,
    FormContacts,
    Invoices,
    Progresses,
];


class Database {
    constructor() {
        this.init();
        this.mongo();
    }

    init() {
        this.connection = new Sequelize(db);

        models
            .map(model => model.init(this.connection))
            .map(model => model.associate && model.associate(this.connection.models));
    }

    mongo() {
        this.mongoConnection = mongoose.connect(
            process.env.MONGO_URL, {
            useNewUrlParser: true,
            useFindAndModify: true,
            useUnifiedTopology: true,
        }
        );
    }
}

export default new Database();
