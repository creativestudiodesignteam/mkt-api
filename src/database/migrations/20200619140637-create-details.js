module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('details', {
      /* fk_grids fk_userss fk_addresses */
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      length: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      height: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      width: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      weight_unit: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: 'g',
      },
      weight: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      situation: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('details');
  },
};
