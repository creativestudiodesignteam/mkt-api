module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('fretes', 'name', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('fretes', 'postcode', {
      type: Sequelize.STRING,
      allowNull: true,
    });
    queryInterface.addColumn('fretes', 'state', {
      type: Sequelize.STRING,
      allowNull: true,
    });
    queryInterface.addColumn('fretes', 'city', {
      type: Sequelize.STRING,
      allowNull: true,
    });
    queryInterface.addColumn('fretes', 'neighborhood', {
      type: Sequelize.STRING,
      allowNull: true,
    });
    queryInterface.addColumn('fretes', 'street', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('fretes', 'number', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('fretes', 'complement', {
      type: Sequelize.STRING,
      allowNull: true
    });
    return queryInterface.addColumn('fretes', 'references', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    queryInterface.removeColumn('fretes', 'name');
    queryInterface.removeColumn('fretes', 'postcode');
    queryInterface.removeColumn('fretes', 'state');
    queryInterface.removeColumn('fretes', 'city');
    queryInterface.removeColumn('fretes', 'neighborhood');
    queryInterface.removeColumn('fretes', 'street');
    queryInterface.removeColumn('fretes', 'number');
    queryInterface.removeColumn('fretes', 'complement');
    return queryInterface.removeColumn('fretes', 'references');
  },
};

/*

     name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      postcode: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      city: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      neighborhood: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      street: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      number: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      complement:{
        type: Sequelize.STRING,
        allowNull: true,
      },
      references:{
        type: Sequelize.STRING,
        allowNull: true,
      },
*/
