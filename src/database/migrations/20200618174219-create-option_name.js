module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('options_names', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      type: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      fk_options: {
        type: Sequelize.INTEGER,
        references: { model: 'options', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },
      fk_users: {
        type: Sequelize.INTEGER,
        references: { model: 'users', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },
      default: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('options_names');
  },
};

