module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('products', 'condition', {
      type: Sequelize.STRING,
      allowNull: true,
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('products', 'condition');
  },
};
