module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('fretes', 'tracking', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('fretes', 'tracking');
  },
};