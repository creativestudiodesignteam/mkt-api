module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('details', 'delivery_time', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('details', 'delivery_time');
  },
};