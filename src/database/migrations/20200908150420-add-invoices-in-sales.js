module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('sales', 'fk_invoices', {
      type: Sequelize.INTEGER,
      references: { model: 'invoices', key: 'id' },
      onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
      onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('sales', 'fk_invoices');
  },
};
