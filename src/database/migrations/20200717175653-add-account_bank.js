module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('providers', 'fk_accounts_banks', {
      type: Sequelize.INTEGER,
      references: { model: 'providers', key: 'id' },
      onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
      onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('providers', 'fk_accounts_banks');
  },
};
