module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('credit_cards', 'holder_name', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('credit_cards', 'holder_name');
  },
};
