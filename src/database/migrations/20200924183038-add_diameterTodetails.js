module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('details', 'diameter', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('details', 'diameter');
  },
};
