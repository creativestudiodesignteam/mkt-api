module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('details', 'type_packing', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('details', 'type_packing');
  },
};
