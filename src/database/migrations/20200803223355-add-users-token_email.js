module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.addColumn('users', 'activation_at', {
      type: Sequelize.DATE,
      allowNull: true,
    });

    return queryInterface.addColumn('users', 'token_email', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    queryInterface.removeColumn('users', 'activation_at');

    return queryInterface.removeColumn('users', 'token_email');
  },
};
