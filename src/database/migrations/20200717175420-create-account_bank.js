module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('accounts_banks', {
      /* name description */
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      bank_code: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      account_type: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bank_agency: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bank_agency_digit: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 0
      },
      bank_account: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bank_account_digit: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('accounts_banks');
  },
};
