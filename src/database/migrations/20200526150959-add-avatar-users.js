module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('users', 'fk_avatar', {
      type: Sequelize.INTEGER,
      references: { model: 'files', key: 'id' },
      onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
      onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'fk_avatar');
  },
};
