module.exports = {
  up: (queryInterface, Sequelize) => {

    queryInterface.addColumn('sales', 'payment_type', {
      type: Sequelize.INTEGER,
      allowNull: false,
    });

    return queryInterface.addColumn('sales', 'canceled_at', {
      type: Sequelize.DATE,
      allowNull: true,
    });
  },

  down: queryInterface => {

    queryInterface.removeColumn('sales', 'payment_type');

    return queryInterface.removeColumn('sales', 'canceled_at');
  },
};
