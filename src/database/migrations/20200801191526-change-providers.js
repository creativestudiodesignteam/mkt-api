module.exports = {
  up: (queryInterface, Sequelize) => {

    queryInterface.addColumn('providers', 'telephone_commercial', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('providers', 'telephone_whatsapp', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('providers', 'data_fundacao', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('providers', 'qty_funcionarios', {
      type: Sequelize.INTEGER,
      allowNull: true
    });
    queryInterface.addColumn('providers', 'site', {
      type: Sequelize.STRING,
      allowNull: true
    });
    queryInterface.addColumn('providers', 'modalidade', {
      type: Sequelize.STRING,
      allowNull: true,
    });

    return queryInterface.removeColumn('providers', 'telephone');
  },

  down: queryInterface => {
    queryInterface.removeColumn('providers', 'telephone_commercial');
    queryInterface.removeColumn('providers', 'telephone_whatsapp');

    return queryInterface.addColumn('providers', 'telephone', {
      type: Sequelize.STRING,
      allowNull: false
    });
  },
};
