"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Details = require('../models/Details'); var _Details2 = _interopRequireDefault(_Details);

class DetailsController {
  async index(req, res) {
    const { id } = req.params;
    const details = await _Details2.default.findOne({
      where: { id, status: true },
      attributes: [
        'id',
        'length',
        'height',
        'width',
        'weight_unit',
        'weight',
        'situation',
        'delivery_time',
      ],
    });
    if (!details) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'details',
          message: 'Detalhe do produto não existe, tente novamente',
        },
      });
    }
    return res.json(details);
  }

  async store(req, res) {
    /* length height width weight_unit weight situation */
    const schema = Yup.object().shape({
      length: Yup.string().required(),
      height: Yup.string().required(),
      width: Yup.string().required(),
      weight_unit: Yup.string().required(),
      weight: Yup.string().required(),
      situation: Yup.string(),
      delivery_time: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const {
      id,
      length,
      height,
      width,
      weight_unit,
      weight,
      situation,
      delivery_time,
    } = await _Details2.default.create(req.body);

    return res.json({
      id,
      length,
      height,
      width,
      weight_unit,
      weight,
      situation,
      delivery_time,
    });
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      length: Yup.string(),
      height: Yup.string(),
      width: Yup.string(),
      weight_unit: Yup.string(),
      weight: Yup.string(),
      situation: Yup.string(),
      delivery_time: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const DetailsExists = await _Details2.default.findOne({
      where: { id, status: true },
    });
    if (!DetailsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'details',
          message: 'Detalhes não existe, tente novamente !',
        },
      });
    }
    const {
      length,
      height,
      width,
      weight_unit,
      weight,
      situation,
      delivery_time,
    } = await DetailsExists.update(req.body);

    return res.json({
      id,
      length,
      height,
      width,
      weight_unit,
      weight,
      situation,
      delivery_time,
    });
  }

  async delete(req, res) {
    const { id } = req.params;
    const DetailsExists = await _Details2.default.findOne({
      where: { id, status: true },
    });
    if (!DetailsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'sublevels',
          message: 'Sub categoria não existe.',
        },
      });
    }

    await DetailsExists.destroy();

    return res.json({ msg: 'Detalhe deletado' });
  }
}

exports. default = new DetailsController();
