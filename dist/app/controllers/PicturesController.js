"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Pictures = require('../models/Pictures'); var _Pictures2 = _interopRequireDefault(_Pictures);

class PicturesController {
  async index(req, res) {
    const { fk_grids } = req.params;
    const pictures = await _Pictures2.default.findAll({
      where: { fk_grids }
    })
    return res.json(pictures);
  }

  async store(req, res) {
    const list = await req.files.map(async  response =>

      await _Pictures2.default.create(
        {
          name: response.originalname,
          path: response.filename,
          fk_grids: req.params.fk_grids
        }
      ),
    )

    const functionWithPromise = item => { //a function that returns a promise
      return Promise.resolve(item)
    }

    const anAsyncFunction = async item => {
      return functionWithPromise(item)
    }

    const getData = async () => {
      return Promise.all(list.map(item => anAsyncFunction(item)))
    }

    getData().then(data => {
      return res.json(data);
    })
  }

}
exports. default = new PicturesController();

