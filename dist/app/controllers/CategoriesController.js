"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);

var _sequelize = require('sequelize');

class CategoriesController {
  async index(req, res) {
    const categories = await _Categories2.default.findAll({
      where: { status: true },
      attributes: ['id', 'name'],
    });

    return res.json(categories);
  }

  async findSubcategoriesByCategories(req, res) {
    try {
      const categories = await _Categories2.default.findAll({
        where: { status: true },
        attributes: ['id', 'name'],
      });

      const promisesGrids = categories.map(async response => {
        const subcategories = await _Subcategories2.default.findAll({
          where: { fk_categories: response.id, status: true },
          attributes: ['id', 'name'],
        });

        return { id: response.id, name: response.name, subcategories };
      });
      const all = await Promise.all(promisesGrids);

      return res.json(all);
    } catch ({ message, error }) {
      return res.json(message);
    }
  }

  async findProductByCategories(req, res) {
    try {
      const { fk_categories } = req.params;

      const categoriesExists = await _Categories2.default.findOne({
        where: { id: fk_categories, status: true },
      });

      if (!categoriesExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'categories',
            message: 'Categoria não existe, tente novamente !',
          },
        });
      }
      /*
            const { page = 1 } = req.query; */

      const product = await _Products2.default.findAll({
        /*         limit: 20, */
        order: ['created_at'],
        /*         offset: (page - 1) * 20, */
        where: { fk_categories, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'brand',
          'sku',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url'],
          },
        ],
      });

      if (!product) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'products in categories',
            message: 'Não existe produtos cadastrados nessa categoria !',
          },
        });
      }
      const promisesGrids = product.map(async response => {
        const gridsList = await _Grids2.default.findAll({
          where: { fk_products: response.id },
          attributes: [
            [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
            [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
            [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const { id, name } = await _Categories2.default.create(req.body);

    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const CategoriesExists = await _Categories2.default.findOne({
      where: { id, status: true },
    });
    if (!CategoriesExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'categories',
          message: 'Categoria não existe, tente novamente !',
        },
      });
    }
    const { name } = await CategoriesExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params;
    const CategoriesExists = await _Categories2.default.findOne({
      where: { id, status: true },
    });
    if (!CategoriesExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'categories',
          message: 'Categoria não existe, tente novamente !',
        },
      });
    }

    await CategoriesExists.update({ status: false });

    return res.json({ msg: 'Categories is disabled' });
  }
}

exports. default = new CategoriesController();
