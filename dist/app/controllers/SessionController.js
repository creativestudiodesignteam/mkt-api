"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _jsonwebtoken = require('jsonwebtoken'); var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Customers = require('../models/Customers'); var _Customers2 = _interopRequireDefault(_Customers);
var _auth = require('../../config/auth'); var _auth2 = _interopRequireDefault(_auth);
var _mailerHelper = require('./../Helpers/user/mailerHelper'); var _mailerHelper2 = _interopRequireDefault(_mailerHelper);

var _datefns = require('date-fns');
class SessionController {
  async store(req, res) {
    const { email, password } = req.body;
    var status = 'Success';
    const user = await _Users2.default.findOne({ where: { email, type: req.body.type } });

    if (!user) {
      return res.status(401).json({
        error: {
          "type": "incorrect_email",
          "parameter_name": "email",
          "message": "Email incorreto."
        }
      });
    }

    const { id, type, timeout, activation_at } = user;
    let { attempts } = user;
    const dateNow = _datefns.toDate.call(void 0, Date.now()).toJSON();
    const validate = _datefns.isBefore.call(void 0, _datefns.parseISO.call(void 0, dateNow), _datefns.addMinutes.call(void 0, timeout, 30));

    if (attempts >= 3 && validate) {
      const remaining = _datefns.differenceInMinutes.call(void 0, _datefns.addMinutes.call(void 0, timeout, 30), _datefns.parseISO.call(void 0, dateNow))
      return res.status(401).json({
        error: {
          type: 'timeout_login',
          parameter_name: 'timeout',
          message: `Espere ${remaining} minutos para fazer login novamente!`
        }
      });
    } else if (attempts >= 3) {
      attempts = 0;
      user.update({ attempts: 0 });
    }

    if (!await user.checkPassword(password)) {
      user.update({ attempts: attempts + 1, timeout: dateNow });
      return res.status(401).json({
        error: {
          type: 'incorrect_password',
          parameter_name: 'password',
          message: 'Senha incorreta.'
        }
      });
    }

    if (user.token_email && user.activation_at == null) {
      _mailerHelper2.default.resendTokenActivate({
        user
      })
      return res.status(401).json({
        error: {
          "type": "account_not_activate",
          "parameter_name": "activate",
          "message": "Por favor, verifique seu email para ativar sua conta."
        }
      });
    }

    if (!(await user.checkPassword(password))) {
      return res.status(401).json({
        error: {
          "type": "incorrect_password",
          "parameter_name": "password",
          "message": "Senha incorreta."
        }
      });
    }

    var name;
    if (type) {
      const providerExist = await _Providers2.default.findOne({ where: { fk_users: id, status: true } });
      if (!providerExist) {
        status = 'cadastro de fornecedor está incompleto';
      }
      name = providerExist.fantasy_name;
    } else {
      const customerExist = await _Customers2.default.findOne({ where: { fk_users: id, status: true } });
      if (!customerExist) {
        status = 'cadastro de cliente está incompleto';
      }
      name = customerExist.name
    }
    user.update({ attempts: 0 });
    return res.json({
      status: status,
      token: _jsonwebtoken2.default.sign({ id, name, email, type, status }, _auth2.default.secret, {
        expiresIn: _auth2.default.expiresIn,
      }),
    });
  }
}

exports. default = new SessionController();
