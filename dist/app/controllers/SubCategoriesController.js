"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
/*import Files from '../models/Files'; */

class SubcategoriesController {
  async index(req, res) {
    const { fk_categories } = req.params
    const subcategories = await _Subcategories2.default.findAll({
      where: { status: true, fk_categories },
      attributes: ['id', 'name'],
    })

    return res.json(subcategories);
  }


  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      fk_categories: Yup.number().integer().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }

    const {
      id,
      name,
    } = await _Subcategories2.default.create(req.body);


    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }
    const SubcategoriesExists = await _Subcategories2.default.findOne({
      where: { id, status: true },
    });
    if (!SubcategoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub categoria não existe."
        }
      })
    }
    const {
      name,
    } = await SubcategoriesExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params
    const SubcategoriesExists = await _Subcategories2.default.findOne({
      where: { id, status: true },
    });
    if (!SubcategoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub categoria não existe."
        }
      });
    }

    await SubcategoriesExists.destroy();

    return res.json({ msg: 'Sub categoria deletada' })

  }
}

exports. default = new SubcategoriesController();
