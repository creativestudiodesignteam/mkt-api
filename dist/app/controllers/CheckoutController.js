"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Addresses = require('../models/Addresses'); var _Addresses2 = _interopRequireDefault(_Addresses);
var _Carts = require('../models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Sales = require('../models/Sales'); var _Sales2 = _interopRequireDefault(_Sales);
var _Payments = require('../models/Payments'); var _Payments2 = _interopRequireDefault(_Payments);
var _CreditCards = require('../models/CreditCards'); var _CreditCards2 = _interopRequireDefault(_CreditCards);
var _Fretes = require('../models/Fretes'); var _Fretes2 = _interopRequireDefault(_Fretes);
var _Customers = require('../models/Customers'); var _Customers2 = _interopRequireDefault(_Customers);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);

const Payload = require('../services/safe2pay/Transaction');

class CheckoutController {

  async store(req, res) {

    const {
      card_holder,
      card_number,
      expiration_date,
      security_code
    } = req.body;

    let payment_type = parseInt(req.body.payment_type);

    const cartModel = await _Carts2.default.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'amount', 'fk_grids', 'fk_provider', 'fk_users', 'status'],
    });

    if (!cartModel.length) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'cart',
          'message': 'Carrinho não existe.',
        },
      });
    }

    const freteModel = await _Fretes2.default.findOne({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'valor'],
    });

    if (!freteModel) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'cart',
          'message': 'Frete não selecionado.',
        },
      });
    }

    const addressModel = await _Addresses2.default.findOne({
      where: { fk_users: req.userId, select_at: true },
      attributes: ['id', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number', 'complement', 'references'],
    });

    if (!addressModel) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'checkout',
          'message': 'Customer sem endereço.',
        },
      });
    }

    // CREDITO OU DEBITO
    if (payment_type === 2 || payment_type === 4) {

      if (card_holder && card_number && expiration_date && security_code) {

        let generateToken = await Payload.generateToken(
          card_holder,
          card_number,
          expiration_date,
          security_code,
          payment_type,
          card_number.substring(0, 4),
          req.userId
        );

        var cardModel = {
          type: generateToken.card.type,
          token: generateToken.card.token,
        }
      } else {
        var cardModel = await _CreditCards2.default.findOne({
          where: { fk_users: req.userId, status: true, selected: true },
          attributes: ['token', 'type'],
        });
      }

      if (!cardModel) {
        return res.json({
          error: {
            'type': 'not_exists_parameter',
            'parameter_name': 'checkout',
            'message': 'Nenhum cartão cadastrado.',
          },
        });
      } else {
        //multi type
        if (cardModel.type !== 0) {

          //pagando com debito e usando cartao de credito
          if (cardModel.type === 2 && payment_type === 4) {
            return res.json({
              error: {
                'type': 'not_exists_parameter',
                'parameter_name': 'checkout',
                'message': 'Cartão não compatível com forma de pagamento.',
              },
            });
          }

          //pagando com credito e usando cartao de debito
          if (cardModel.type === 4 && payment_type === 2) {
            return res.json({
              error: {
                'type': 'not_exists_parameter',
                'parameter_name': 'checkout',
                'message': 'Cartão não compatível com forma de pagamento.',
              },
            });
          }
        }
      }
    }

    var produtos = [];
    var totalPrice = 0;
    for (const element of cartModel) {

      const productsModel = await _Grids2.default.findOne({
        where: { id: element.fk_grids, status: true },
        attributes: ['id', 'fk_products', 'price'],
        include: [
          {
            model: _Products2.default,
            as: 'products',
            attributes: ['id', 'name', 'brand'],
          },
        ],
      });

      let productPrice = parseInt(productsModel.price) * parseInt(element.amount);
      totalPrice = totalPrice + productPrice;
      produtos = [{
        id: productsModel.products.id,
        nome: productsModel.products.name,
        valor: productPrice / 100,
        qty: element.amount,
      }];
    }

    const userModel = await _Users2.default.findOne({
      where: { id: req.userId },
      attributes: ['email', 'type'],
    });

    let personalData = {}
    if (userModel.type) {
      const providerModel = await _Providers2.default.findOne({
        where: { 'fk_users': req.userId },
        attributes: ['fantasy_name', 'cnpj', 'telephone_commercial']
      });

      personalData = {
        'name': providerModel.fantasy_name,
        'documento': providerModel.cnpj,
        'telephone': providerModel.telephone_commercial
      }
    } else {
      const customerModel = await _Customers2.default.findOne({
        where: { 'fk_users': req.userId },
        attributes: ['name', 'cpf', 'telephone']
      });

      personalData = {
        'name': customerModel.name,
        'documento': customerModel.cpf,
        'telephone': customerModel.telephone
      }
    }

    let safe2payData = {
      'SAFE2PAY_IS_SANDBOX': process.env.SAFE2PAY_IS_SANDBOX,
      'SAFE2PAY_KEY_SANDBOX': process.env.SAFE2PAY_KEY_SANDBOX,
      'SAFE2PAY_CALLBACK_URL': process.env.SAFE2PAY_CALLBACK_URL,
    };

    const pagamento = await Payload.setTransaction(req, addressModel, userModel, produtos, cardModel, payment_type, safe2payData, freteModel, personalData);

    var status = true;
    if (pagamento.response.HasError) {
      console.log(pagamento)
      return res.json({
        error: {
          'type': 'checkout',
          'parameter_name': 'checkout',
          'message': pagamento.response.Error,
        },
      });
    }

    let paymentDetail = pagamento.response.ResponseDetail;

    //boleto
    if (payment_type === 1) {
      status = false;
    }

    let references = '';
    //credito e debito
    if (payment_type === 2 || payment_type === 4) {
      //pagamento autorizado
      if (paymentDetail.Status === 3) {
        status = true;
      } else {
        status = false;
      }

      references = paymentDetail.Token;
    } else {
      references = paymentDetail.BankSlipUrl;
    }
    console.log(paymentDetail)
    let payment = await _Payments2.default.create({
      code: paymentDetail.IdTransaction,
      method: payment_type,
      references: references,
      situations: paymentDetail.Message,
      status: status,
      url: paymentDetail.BankSlipUrl,
    });

    for (const element of cartModel) {

      // inativa o carrinho
      let cartInactive = await _Carts2.default.findOne({
        where: { id: element.dataValues.id },
      });
      await cartInactive.update({ status: false });

      //salva os dados da venda
      await _Sales2.default.create(
        {
          hash: paymentDetail.IdTransaction,
          amount: element.amount,
          quota: 0,
          price: totalPrice,
          status: status,
          fk_grids: element.fk_grids,
          fk_users: req.userId,
          fk_provider: element.fk_provider,
          fk_payments: payment.id,
          fk_fretes: freteModel.id,
          payment_type,
        },
      );

      //boleto
      if (payment_type === 1) {
        return res.json(pagamento);
      }

      if (!status) {
        return res.json({
          error: {
            'type': 'payment_error',
            'parameter_name': 'checkout',
            'message': pagamento.response.Error,
          },
        });
      }
    }

    return res.json(pagamento);
  }
}

exports. default = new CheckoutController();
