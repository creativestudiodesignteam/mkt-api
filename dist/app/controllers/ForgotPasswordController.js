"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Customers = require('../models/Customers'); var _Customers2 = _interopRequireDefault(_Customers);
var _bcryptjs = require('bcryptjs'); var _bcryptjs2 = _interopRequireDefault(_bcryptjs);
var _Queue = require('../../lib/Queue'); var _Queue2 = _interopRequireDefault(_Queue);
var _ForgotPasswordMail = require('../jobs/ForgotPasswordMail'); var _ForgotPasswordMail2 = _interopRequireDefault(_ForgotPasswordMail);
var _datefns = require('date-fns');
/*import Files from '../models/Files'; */

class ForgotPasswordController {
  async verify_token(req, res) {
    const { token } = req.params
    const usersToken = await _Users2.default.findOne({
      where: { reset_password: token, status: true },
      attributes: ['id', 'email', 'reset_password', 'date_reset_password']
    })
    if (!usersToken) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "token",
          "message": "Token inválido, token inexistente"
        }
      })
    }
    const dateNow = _datefns.toDate.call(void 0, Date.now()).toJSON()
    const validate = _datefns.isBefore.call(void 0, _datefns.parseISO.call(void 0, dateNow), _datefns.addMinutes.call(void 0, usersToken.date_reset_password, 5))
    if (!validate) {
      return res.json({
        error: {
          "type": "is_invalid",
          "parameter_name": "datetime",
          "message": "Token inválido, tempo expirado !"
        }
      })
    }

    return res.json({ validation: validate });
  }

  async store(req, res) {

    const schema = Yup.object().shape({
      email: Yup.string().email().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }

      });
    }

    const userExists = await _Users2.default.findOne({ where: { email: req.body.email }, attributes: ['id', 'email', 'type', 'status'] })
    if (!userExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "user",
          "message": "Usuário não existe"
        }

      });
    }
    let name;
    if (userExists.type) {
      const { fantasy_name } = await _Providers2.default.findOne({
        where: { fk_users: userExists.id },
        attributes: ['id', 'fantasy_name', 'status']
      })
      name = fantasy_name;
    } else {
      const { name: nameCustomer } = await _Customers2.default.findOne({
        where: { fk_users: userExists.id },
        attributes: ['id', 'name', 'status']
      })
      name = nameCustomer;
    }
    const token = await _bcryptjs2.default.hash(req.body.email, 8);

    const date_reset_password = _datefns.toDate.call(void 0, Date.now()).toJSON()

    const { reset_password } = await userExists.update({ reset_password: token, date_reset_password })

    await _Queue2.default.add(_ForgotPasswordMail2.default.key, {
      users: { email: userExists.email, name },
      token: reset_password
    });

    return res.json({ message: 'Verifique seu email para recuperar sua senha !' })
  }

  async update(req, res) {
    const { token } = req.body
    const schema = Yup.object().shape({
      new_password: Yup.string().min(6).required('Por favor digite sua senha'),
      new_confirmPassword: Yup.string().min(6).required('Por favor confirme sua senha'),
    });
    if (req.body.new_password !== req.body.new_confirmPassword) {
      return res.json({
        error: {
          "type": "password_not_match",
          "parameter_name": "password",
          "message": "As senhas são diferentes, tente novamente"
        }
      });
    }
    await schema.validate(req.body).catch(function (err) {
      return res.json({
        error: {
          "type": "validation_invalid_user",
          "parameter_name": "yup",
          "message": err.errors.toString()
        }
      });
    });

    const usersToken = await _Users2.default.findOne({
      where: { reset_password: token, status: true },
      attributes: ['id', 'email', 'reset_password', 'date_reset_password']
    })

    if (!usersToken) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "token",
          "message": "Token inválido, token inexistente"
        }
      })
    }

    const dateNow = _datefns.toDate.call(void 0, Date.now()).toJSON()
    const validate = _datefns.isBefore.call(void 0, _datefns.parseISO.call(void 0, dateNow), _datefns.addMinutes.call(void 0, usersToken.date_reset_password, 10))
    if (!validate) {
      return res.json({
        error: {
          "type": "is_invalid",
          "parameter_name": "datetime",
          "message": "Token inválido, tempo expirado !"
        }
      })
    }

    await usersToken.update({
      password: req.body.new_password,
      reset_password: null,
      date_reset_password: null
    })

    return res.json({ message: 'Usuario editado com sucesso' })
  }

}

exports. default = new ForgotPasswordController();
