"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize');
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Sublevels = require('../models/Sublevels'); var _Sublevels2 = _interopRequireDefault(_Sublevels);
var _new_excel_json_categoriesjson = require('../../../tmp/new_excel_json_categories.json'); var _new_excel_json_categoriesjson2 = _interopRequireDefault(_new_excel_json_categoriesjson);
var _Queue = require('../../lib/Queue'); var _Queue2 = _interopRequireDefault(_Queue);
class TesteController {

  async store(req, res) {
    const teste = false;
    if (teste === true) {
      const promisesCategories = _new_excel_json_categoriesjson2.default.map(async response => {

        console.log('=================== Nome da categoria========================')
        const promisesCategories = response.categories.map(async categories => {


          const { id: idCategories } = await _Categories2.default.create({
            name: categories.name,
          });

          console.log(categories.subcategories)

          console.log('=================== Nome da subcategoria============================')
          const promisesSubcategories = categories.subcategories.map(async subcategories => {

            console.log(subcategories.name)

            const { id: idSubCategories } = await _Subcategories2.default.create({
              name: subcategories.name,
              fk_categories: idCategories
            });


            console.log('=================== Sublevels ============================')
            if (subcategories.sublevels) {
              const promisesSublevels = subcategories.sublevels.map(async sublevels => {
                console.log('cu')
                console.log(sublevels.name)

                const { id: idSubLevels } = await _Sublevels2.default.create({
                  name: sublevels.name,
                  fk_subcategories: idSubCategories
                });



              })


              const sublevels = await Promise.all(promisesSublevels);
            }

            /* return sublevels */
          })

          const subCategories = await Promise.all(promisesSubcategories);

          /*  return subCategories */
        })

        const categories = await Promise.all(promisesCategories);

        /*  return ({ categories }) */

      })

      const grids = await Promise.all(promisesCategories);
      console.log('finished')
      return res.json(grids)
    }

  }
  async QueueTest(req, res) {
    const { name, email, password } = req.body;

    const users = {
      name,
      email,
      password,
    };

    // Adicionar job RegistrationMail na fila

    const config = { token: 'aooo boiada' }

    await _Queue2.default.add('ForgotPasswordMail', { users, config });
    /* await Queue.add('RegistrationMail', { user }); */

    /*    await Queue.add('UserReport', { user }); */

    return res.json(users);
  }

}

exports. default = new TesteController();
