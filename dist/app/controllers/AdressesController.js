"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Addresses = require('../models/Addresses'); var _Addresses2 = _interopRequireDefault(_Addresses);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);

class AddressesController {

  async index(req, res) {

    const addresses = await _Addresses2.default.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number', 'complement', 'references', 'status', 'select_at'],
    });

    return res.json(addresses);
  }

  async findBySelect(req, res) {

    const addresses = await _Addresses2.default.findOne({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'name', 'postcode', 'state', 'city', 'neighborhood', 'street', 'number', 'complement', 'references', 'status', 'select_at'],

    });

    return res.json(addresses);
  }

  async store(req, res) {
    /* name postcode state city neighborhood street number complement */
    /* req.body.fk_users = req.userId; */

    const schema = Yup.object().shape({
      name: Yup.string().required(),
      postcode: Yup.string().required(),
      state: Yup.string().required(),
      city: Yup.string().required(),
      neighborhood: Yup.string().required(),
      street: Yup.string().required(),
      number: Yup.string().required(),
      complement: Yup.string(),
      references: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const fk_userExists = await _Users2.default.findByPk(req.body.fk_users);
    if (!fk_userExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'user',
          'message': 'Usuario não existe, tente novamente !',
        },
      });
    }
    /* console.log(req.body) */

    const {
      id,
      postcode,
      number,
      state,
      neighborhood,
      city,
      street,
      complement,
      references,
      status,
      select_at,
    } = await _Addresses2.default.create(req.body);
    return res.json({
      id,
      postcode,
      number,
      state,
      neighborhood,
      city,
      street,
      complement,
      references,
      status,
      select_at,
    });
  }

  async update(req, res) {
    const { id } = req.params;
    req.body.fk_users = req.userId;

    const schema = Yup.object().shape({
      postcode: Yup.string(),
      state: Yup.string(),
      city: Yup.string(),
      neighborhood: Yup.string(),
      street: Yup.string(),
      number: Yup.string(),
      complement: Yup.string(),
      references: Yup.string(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const addressesExists = await _Addresses2.default.findOne({
      where: { id, fk_users: req.body.fk_users },
    });
    if (!addressesExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'addresses',
          'message': 'Endreço não existe, tente novamente !',
        },
      });
    }

    const {
      postcode,
      number,
      state,
      neighborhood,
      city,
      street,
      complement,
      references,
      status,
      select_at,
    } = await addressesExists.update(req.body);

    return res.json({
      id,
      postcode,
      number,
      state,
      neighborhood,
      city,
      street,
      complement,
      references,
      status,
      select_at,
    });
  }

  async selectUpdate(req, res) {
    const { fk_new_addresses } = req.body;

    const addressesExists = await _Addresses2.default.findOne({
      where: { fk_users: req.userId, select_at: true, status: true },
    });


    if (addressesExists) {
      await addressesExists.update({ select_at: false, status: true });
    }

    const addressesNewExists = await _Addresses2.default.findOne({
      where: { id: fk_new_addresses, fk_users: req.userId },
    });


    const {
      id,
      name,
      postcode,
      state,
      city,
      neighborhood,
      street,
      number,
      complement,
      references,
      status,
      select_at,
    } = await addressesNewExists.update({ select_at: true });

    return res.json({
      id,
      name,
      postcode,
      state,
      city,
      neighborhood,
      street,
      number,
      complement,
      references,
      status,
      select_at,
    });
  }

  async delete(req, res) {
    const { id } = req.params;

    const addressesExists = await _Addresses2.default.findOne({
      where: { id, status: true, fk_users: req.userId },
    });

    if (!addressesExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'addresses',
          'message': 'Endreço não existe, tente novamente !',
        },
      });
    }

    await addressesExists.update({ status: false });


    return res.json({ msg: 'Endereço desabilitado' });

  }
}

exports. default = new AddressesController();
