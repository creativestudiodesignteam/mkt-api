"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Sales = require('../models/Sales'); var _Sales2 = _interopRequireDefault(_Sales);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Payments = require('../models/Payments'); var _Payments2 = _interopRequireDefault(_Payments);

class ProviderSalesController {

  async index(req, res) {
    const { fk_provider } = req.params;
    const sales = await _Sales2.default.findAll({
      where: { fk_provider: fk_provider, status: true },
      attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status'],
      include: [
        {
          model: _Grids2.default,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: _Products2.default,
              as: 'products',
              attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
              include: [
                {
                  model: _Files2.default,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: _Providers2.default,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name', 'reason_social', 'state_register',
            'county_register', 'telephone_commercial', 'telephone_whatsapp', 'token',
            'idcode', 'status', 'data_fundacao', 'qty_funcionarios', 'site', 'modalidade'],
        },
        {
          model: _Payments2.default,
          as: 'payments',
          attributes: ['id', 'code', 'method', 'situations', 'status'],
        },
      ],
    });

    if (!sales) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'sales',
          'message': 'venda não existe.',
        },
      });
    }

    return res.json({ sales });
  }

  async detail(req, res) {
    const { hash } = req.params;
    return res.json(await _Sales2.default.findOne({
        where: { hash: hash },
        attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status'],
      group: ['hash'],
        include: [
          {
            model: _Grids2.default,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: _Products2.default,
                as: 'products',
                attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                include: [
                  {
                    model: _Files2.default,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
          {
            model: _Providers2.default,
            as: 'providers',
            attributes: ['id', 'cnpj', 'fantasy_name', 'reason_social', 'state_register',
              'county_register', 'telephone_commercial', 'telephone_whatsapp', 'token',
              'idcode', 'status', 'data_fundacao', 'qty_funcionarios', 'site', 'modalidade'],
          },
          {
            model: _Payments2.default,
            as: 'payments',
            attributes: ['id', 'code', 'method', 'situations', 'status'],
          },
        ],
      }),
    );
  }

  async delete(req, res) {
    const { hash } = req.params;

    const salesExists = await _Sales2.default.findAll({
      where: { hash: hash },
      group: ['hash'],
    });

    if (!salesExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'addresses',
          'message': 'Endreço não existe, tente novamente !',
        },
      });
    }

    await salesExists.update({ status: false });

    return res.json({ msg: 'venda excluída' });
  }
}

exports. default = new ProviderSalesController();
