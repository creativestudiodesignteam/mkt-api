"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Prodsubcats = require('../models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _sequelize = require('sequelize');
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);

class ProdsubcatsController {
  async index(req, res) {

    const prodsubcats = await _Prodsubcats2.default.findAll({
      where: { fk_subcategories: req.params.fk_subcategories, status: true },
      include: [
        {
          model: _Products2.default,
          as: 'products',
          attributes: ['id', 'name', 'sku', 'description', 'brand', 'model', 'status'],
          include: [
            {
              model: _Files2.default,
              as: 'file',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
            {
              model: _Categories2.default,
              as: 'categories',
              attributes: ['id', 'name', 'status'],
            },
          ]
        },
      ]
    })

    const promisesGrids = prodsubcats.map(async response => {
      const gridsList = await _Grids2.default.findAll({
        where: { fk_products: response.products.id },
        attributes: [
          [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
          [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
          [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock']
        ]
      })

      const { id, name, description, sku, brand, model, status, categories, file, details } = response.products
      return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

    })

    const grids = await Promise.all(promisesGrids)

    return res.json(grids);
  }

  async store(req, res) {
    const subcatregoriesExists = await _Subcategories2.default.findOne({
      where: { id: req.body.fk_subcategories, status: true }
    })
    const prodsubcatregoriesExists = await _Prodsubcats2.default.findOne({
      where: { fk_subcategories: req.body.fk_subcategories, fk_products: req.body.fk_products, status: true }
    })
    if (prodsubcatregoriesExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria já está relacionada a este produto."
        }
      })
    }
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria não existe, tente novamente !"
        }
      })
    }
    const productsExists = await _Products2.default.findOne({
      where: { id: req.body.fk_products, status: true }
    })
    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produto não existe, tente novamente !"
        }
      })
    }

    const {
      id,
    } = await _Prodsubcats2.default.create(req.body);


    return res.json(
      await _Prodsubcats2.default.findOne({
        where: { id, status: true },
        include: [
          {
            model: _Subcategories2.default,
            as: 'subcategories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Products2.default,
            as: 'products',
            attributes: ['id', 'name', 'description', 'brand', 'model', 'status'],
          }
        ]
      })
    );
  }

  async update(req, res) {
    const { id } = req.params

    const prodsubcatregoriesExists = await _Prodsubcats2.default.findOne({
      where: { id, status: true }
    })
    if (!prodsubcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcatregories",
          "message": "Sub Categoria não está relacionada ao produto, tente novamente !"
        }
      })
    }
    const subcatregoriesExists = await _Subcategories2.default.findOne({
      where: { id: req.body.fk_subcategories, status: true }
    })
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "subcategories",
          "message": "Sub Categoria não existe, tente novamente !"
        }

      })
    }
    const productsExists = await _Products2.default.findOne({
      where: { id: req.body.fk_products }
    })
    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produto não existe, tente novamente !"
        }
      })
    }

    await prodsubcatregoriesExists.update(req.body);

    return res.json(
      await _Prodsubcats2.default.findOne({
        where: { id },
        include: [
          {
            model: _Subcategories2.default,
            as: 'subcategories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Products2.default,
            as: 'products',
            attributes: ['id', 'name', 'description', 'brand', 'model', 'status'],
          }
        ]
      }));
  }

  async delete(req, res) {
    const { id } = req.params
    const ProdsubcatsExists = await _Prodsubcats2.default.findOne({
      where: { id, status: true },
    });
    if (!ProdsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "provider",
          "message": "Relação entre produto e sub categoria não existe, tente novamente !"
        }
      });
    }

    await ProdsubcatsExists.destroy();

    return res.json({ msg: 'Vinculo entre sub categoria e produto foi excluida' })
  }
}

exports. default = new ProdsubcatsController();
