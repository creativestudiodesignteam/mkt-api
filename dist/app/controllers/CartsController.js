"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Carts = require('../models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);

class CartsController {
  async index(req, res) {
    const carts = await _Carts2.default.findAll(
      {
        where: { fk_users: req.userId, status: true },
        attributes: ['id', 'amount', 'status'],
        include: [
          {
            model: _Grids2.default,
            as: 'grids',
            attributes: ['id', 'amount', 'price', 'status'],
            include: [
              {
                model: _Products2.default,
                as: 'products',
                attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                include: [
                  {
                    model: _Files2.default,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url', 'status'],
                  },
                ],
              },
            ],
          },
        ],
      });

    return res.json(carts);
  }

  async store(req, res) {
    req.body.fk_users = req.userId;
    const { fk_grids } = req.body;
    const schema = Yup.object().shape({
      amount: Yup.number().integer().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const GridsExists = await _Grids2.default.findOne({
      where: { id: fk_grids, status: true },
      include: [
        {
          model: _Users2.default,
          as: 'user',
          attributes: ['id', 'email'],
        },
      ],
    });

    if (!GridsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'grids',
          'message': 'A versão do seu produto não existe ou está indisponivel !',
        },
      });
    }
    const GridsUserExist = await _Grids2.default.findOne({
      where: { id: fk_grids, fk_users: req.userId, status: true },
    });
    if (GridsUserExist) {
      return res.status(400).json({
        error: {
          'type': 'not_permission',
          'parameter_name': 'products',
          'message': 'Você não tem permissão para comprar este produto !',
        },
      });
    }

    const ProvidersList = await _Providers2.default.findOne({
      where: { fk_users: GridsExists.user.id, status: true },
    });

    req.body.fk_provider = ProvidersList.id;

    const CartsGridsExists = await _Carts2.default.findOne({
      where: { fk_grids, fk_users: req.userId, status: true },
    });


    if (CartsGridsExists) {
      if (req.body.amount > parseInt(GridsExists.amount)) {

        const { id } = await CartsGridsExists.update({

          amount: parseInt(GridsExists.amount),

        });

        return res.json(
          await _Carts2.default.findOne(
            {
              where: { id, status: true },
              attributes: ['id', 'amount', 'status'],
              include: [
                {
                  model: _Grids2.default,
                  as: 'grids',
                  attributes: ['id', 'status'],
                  include: [
                    {
                      model: _Products2.default,
                      as: 'products',
                      attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                      include: [
                        {
                          model: _Files2.default,
                          as: 'file',
                          attributes: ['id', 'name', 'path', 'url', 'status'],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ),
        );

      } else {

        const { id } = await CartsGridsExists.update({

          amount: parseInt(req.body.amount) + parseInt(CartsGridsExists.amount),

        });
        return res.json(
          await _Carts2.default.findOne(
            {
              where: { id, status: true },
              attributes: ['id', 'amount', 'status'],
              include: [
                {
                  model: _Grids2.default,
                  as: 'grids',
                  attributes: ['id', 'amount', 'price', 'status'],
                  include: [
                    {
                      model: _Products2.default,
                      as: 'products',
                      attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                      include: [
                        {
                          model: _Files2.default,
                          as: 'file',
                          attributes: ['id', 'name', 'path', 'url', 'status'],
                        },
                      ],
                    },
                  ],
                },
              ],
            },
          ),
        );
      }
    }


    if (req.body.amount > parseInt(GridsExists.amount)) {
      const { id } = await CartsGridsExists.update({
        amount: parseInt(GridsExists.amount),
      });

      return res.json(
        await _Carts2.default.findOne(
          {
            where: { id },
            attributes: ['id', 'amount', 'status'],
            include: [
              {
                model: _Grids2.default,
                as: 'grids',
                attributes: ['id', 'amount', 'price', 'status'],
                include: [
                  {
                    model: _Products2.default,
                    as: 'products',
                    attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                    include: [
                      {
                        model: _Files2.default,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                      },
                    ],
                  },
                ],
              },
            ],
          }),
      );
    }

    const { id } = await _Carts2.default.create(req.body);

    return res.json(
      await _Carts2.default.findOne(
        {
          where: { id },
          attributes: ['id', 'amount', 'status'],
          include: [
            {
              model: _Grids2.default,
              as: 'grids',
              attributes: ['id', 'amount', 'price', 'status'],
              include: [
                {
                  model: _Products2.default,
                  as: 'products',
                  attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                  include: [
                    {
                      model: _Files2.default,
                      as: 'file',
                      attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                  ],
                },
              ],

            },
          ],

        }),
    );
  }

  async update(req, res) {
    const { id } = req.params;

    const schema = Yup.object().shape({
      amount: Yup.number().integer(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const CartsExists = await _Carts2.default.findOne({
      where: { id },
    });


    if (!CartsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'carts',
          'message': 'Carrinho não existe, tente novamente !',
        },
      });
    }

    const GridsExists = await _Grids2.default.findOne({
      where: { id: CartsExists.fk_grids, status: true },
    });


    if (!GridsExists) {
      return res.json({
        error: {
          'type': 'inválid_parameters',
          'message': 'Grade do produto não existe, tente novamente.',
        },
      });
    }


    const amountGrids = await _Grids2.default.findOne({
      where: { id: GridsExists.id },
    });

    if (req.body.amount > parseInt(amountGrids.amount)) {
      return res.json(
        await CartsExists.update({
          amount: parseInt(amountGrids.amount),
        }),
      );
    }


    await CartsExists.update(req.body);

    return res.json(
      await _Carts2.default.findOne(
        {
          where: { id },
          attributes: ['id', 'amount', 'status'],
          include: [
            {
              model: _Grids2.default,
              as: 'grids',
              attributes: ['id', 'amount', 'price', 'status'],
              include: [
                {
                  model: _Products2.default,
                  as: 'products',
                  attributes: ['id', 'name', 'brand', 'model', 'sku', 'status'],
                  include: [
                    {
                      model: _Files2.default,
                      as: 'file',
                      attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                  ],
                },
              ],
            },
          ],
        }));
  }

  async delete(req, res) {
    const { id } = req.params;

    const CartsExists = await _Carts2.default.findOne({
      where: { id, status: true },
    });

    if (!CartsExists) {
      return res.json({
        error: {
          'type': 'inválid_parameters',
          'message': 'Carrinho do produto não existe, tente novamente.',
        },
      });
    }

    await CartsExists.destroy();

    return res.json({ msg: 'Deletado com sucesso' });

  }
}

exports. default = new CartsController();
