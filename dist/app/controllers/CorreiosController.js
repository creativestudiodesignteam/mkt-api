"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Carts = require('../models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Addresses = require('../models/Addresses'); var _Addresses2 = _interopRequireDefault(_Addresses);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Fretes = require('../models/Fretes'); var _Fretes2 = _interopRequireDefault(_Fretes);

const Correios = require('../services/correios/Correios');
/**
 * 04014 = SEDEX à vista
 04065 = SEDEX à vista pagamento na entrega
 04510 = PAC à vista
 04707 = PAC à vista pagamento na entrega
 40169 = SEDEX12 ( à vista e a faturar)
 40215 = SEDEX 10 (à vista e a faturar)
 40290 = SEDEX Hoje Varejo
 * @type {string}
 */
class CorreiosController {
  async buscaCep(req, res) {
    const { cep } = req.params;
    let cepCorreios = await Correios.buscaCEP({ cep: cep });
    return res.json(cepCorreios);
  }

  async calculoPrazoProduto(req, res) {
    return res.json();
  }

  async calculoFrete(req, res) {
    if (!req.params)
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'type',
          message: 'Tipo de frete não enviado.',
        },
      });

    const { type } = req.params;
    const codServico = Array();
    codServico['sedex'] = '04014';
    codServico['pac'] = '04510';
    const cartModel = await _Carts2.default.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'amount', 'fk_grids', 'fk_provider', 'fk_users'],
    });
    if (!cartModel.length) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'cart',
          message: 'Carrinho não existe.',
        },
      });
    }

    const AddressUser = await _Addresses2.default.findOne({
      where: { fk_users: req.userId, select_at: true },
      attributes: ['postcode'],
    });
    if (!AddressUser) {
      return res.json({
        error: {
          type: 'not_exists_parameter',
          parameter_name: 'address',
          message: 'Usuário não possui cep cadastrado.',
        },
      });
    }

    let cep = AddressUser.postcode;
    let cepUsuario = cep.replace(/[^\d]+/g, '');

    var fretes = Array();

    let amountTotal;
    _Carts2.default.sum('amount', {
      where: { fk_users: req.userId, status: true },
    }).then(sum => (amountTotal = sum));
    console.dir(cartModel);
    for (const [i, element] of cartModel.entries()) {
      const provider = await _Providers2.default.findOne({
        where: { id: element.fk_provider, status: true },
        attributes: ['fk_users'],
      });

      const addressProvider = await _Addresses2.default.findOne({
        where: { fk_users: provider.fk_users, select_at: true },
        attributes: ['postcode'],
      });
      if (!addressProvider) {
        return res.json({
          error: {
            type: 'not_exists_parameter',
            parameter_name: 'address',
            message: 'Fornecedor não possui cep cadastrado.',
          },
        });
      }
      let cep = addressProvider.postcode;
      let cepProvider = cep.replace(/[^\d]+/g, '');

      // teste
      let peso = 1;
      let comprimento = 20;
      let altura = 20;
      let largura = 20;
      let diametro = 20;

      let frete = await Correios.calcPrecoPrazoData(
        codServico[type],
        cepProvider,
        cepUsuario,
        peso,
        comprimento,
        altura,
        largura,
        diametro,
        amountTotal
      );
      fretes[i] = frete;
    }

    let valor = 0;
    let prazo = 0;
    for (const item of fretes) {
      valor = valor + parseFloat(item[0].Valor);
      prazo = prazo + parseInt(item[0].PrazoEntrega);
    }

    return res.json({
      valor: valor,
      prazo: prazo,
      tipo: type,
      cod_servico: codServico[type],
    });
  }

  async storeFrete(req, res) {
    const schema = Yup.object().shape({
      valor: Yup.string().required(),
      tipo: Yup.string().required(),
      cod_servico: Yup.string().required(),
      prazo: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          type: 'validation_invalid',
          parameter_name: 'yup',
          message: 'Preencheu todos os campos certinhos ?, tente novamente',
        },
      });
    }

    const freteExists = await _Fretes2.default.findOne({
      where: { fk_users: req.userId, status: true },
    });

    if (freteExists) {
      await freteExists.update({ status: false });
    }

    const {
      id,
      valor,
      tipo,
      cod_servico,
      prazo,
      fk_users,
      status,
    } = await _Fretes2.default.create({
      valor: req.body.valor,
      tipo: req.body.tipo,
      cod_servico: req.body.cod_servico,
      prazo: req.body.prazo,
      fk_users: req.userId,
    });
    return res.json({
      id,
      valor,
      tipo,
      cod_servico,
      prazo,
      fk_users,
      status,
    });
  }
}

exports. default = new CorreiosController();
