"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _FormContacts = require('../models/FormContacts'); var _FormContacts2 = _interopRequireDefault(_FormContacts);

class FormContactController {
    async store(req, res) {
        try {
            const schema = Yup.object().shape({
                name: Yup.string().required('Por favor, preencha o campo de nome'),
                tel: Yup.string().required('Por favor, preencha o campo de telefone'),
                email: Yup.string().required('Por favor, preencha o campo de email'),
                message: Yup.string().required('Por favor, preencha o campo de mensagem'),
            });

            await schema.validate(req.body)

            const form_contact = await _FormContacts2.default.create(req.body);
            return res.json({ name: form_contact.name, tel: form_contact.tel, email: form_contact.email, message: form_contact.message });

        } catch ({ message, ...error }) {
            if (error.name === 'ValidationError') {
                return res.json({
                    type: error.type,
                    parameter_name: error.path,
                    error: error.errors.toString()
                })
            }
            return res.json({ error: message })

        }
    }
}

exports. default = new FormContactController();