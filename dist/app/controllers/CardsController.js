"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _CreditCards = require('../models/CreditCards'); var _CreditCards2 = _interopRequireDefault(_CreditCards);
var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Carts = require('../models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);

const Payload = require('../services/safe2pay/Transaction');

class CardsController {

  async index(req, res) {
    const creditCards = await _CreditCards2.default.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'status', 'fk_users', 'first_digits', 'holder_name', 'selected', 'type'],
    });

    return res.json(creditCards);
  }

  async post(req, res) {
    const schema = Yup.object().shape({
      card_holder: Yup.string().required(),
      card_number: Yup.string().required(),
      expiration_date: Yup.string().required(),
      security_code: Yup.string().required(),
      type: Yup.string().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    let cardNumber = req.body.card_number;
    let first4Digits = cardNumber.substring(0, 4);

    let type = parseInt(req.body.type);
    let holder = req.body.card_holder;
    let expiration_date = req.body.expiration_date;
    let security_code = req.body.security_code;

    let token = await Payload.generateToken(holder, cardNumber, expiration_date, security_code, type, first4Digits, req.userId);

    if (token.token_created === true) {
      return res.json(token.card);
    } else {
      return res.json({
        error: {
          'type': 'save_card',
          'parameter_name': 'credit_cards',
          'message': 'Erro ao salvar cartão, tente novamente',
        },
      });
    }
  }

  async delete(req, res) {
    const { id } = req.params;

    const creditCards = await _CreditCards2.default.findOne({
      where: { id, status: true, fk_users: req.userId },
    });

    if (!creditCards) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'credit_cards',
          'message': 'Cartão não existe, tente novamente!',
        },
      });
    }

    await creditCards.update({ status: false });

    return res.json({ msg: 'Cartão desabilitado' });
  }

  async cardSelectedUpdate(req, res) {
    const { id } = req.body;

    const CardExists = await _CreditCards2.default.findOne({
      where: { fk_users: req.userId, selected: true },
    });

    if (CardExists) {
      await CardExists.update({ selected: false });
    }

    const NewCardExists = await _CreditCards2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (NewCardExists) {
      await NewCardExists.update({ selected: true });
      return res.json(NewCardExists);
    } else {
      return res.json({
        error: {
          'type': 'update_credit_card',
          'parameter_name': 'creditcard',
          'message': 'Erro ao atualizar o carrinho.',
        },
      });
    }
  }

  async installments(req, res) {

    const cartModel = await _Carts2.default.findAll({
      where: { fk_users: req.userId, status: true },
      attributes: ['fk_grids'],
    });

    let totalPrice = 0;
    for (const element of cartModel) {
      const productsModel = await _Grids2.default.findOne({
        where: { id: element.fk_grids, status: true },
        attributes: ['id', 'fk_products', 'price']
      });

      let productPrice = parseInt(productsModel.price) * parseInt(element.amount);
      totalPrice = totalPrice + productPrice;
    }


    return res.json()
  }
}

exports. default = new CardsController();
