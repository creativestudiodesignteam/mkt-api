"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);

var _Prodsubcats = require('../models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Sublevels = require('../models/Sublevels'); var _Sublevels2 = _interopRequireDefault(_Sublevels);
var _Levelsubcats = require('../models/Levelsubcats'); var _Levelsubcats2 = _interopRequireDefault(_Levelsubcats);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
/*import Files from '../models/Files'; */

class LevelsubcatsController {
  async index(req, res) {

    const levelsubcats = await _Levelsubcats2.default.findAll({

      where: { fk_prodsubcat: req.params.fk_prodsubcat, status: true },
      include: [
        {
          model: _Prodsubcats2.default,
          as: 'prodsubcat',
          attributes: ['id'],
          include: [
            {
              model: _Subcategories2.default,
              as: 'subcategories',
              attributes: ['id', 'name', 'status']
            },
          ]
        },
        {
          model: _Sublevels2.default,
          as: 'sublevels',
          attributes: ['id', 'name', 'status']
        }
      ]

    })

    return res.json(levelsubcats)
  }

  async store(req, res) {
    const prodSubCatsExists = await _Prodsubcats2.default.findOne({
      where: { id: req.body.fk_prodsubcat }
    })

    if (!prodSubCatsExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "prodsubcat",
          "message": "Sub Categoria já está relacionada a este produto."
        }
      })
    }

    const subLevelsExists = await _Sublevels2.default.findOne({
      where: { id: req.body.fk_sublevels }
    })

    if (!subLevelsExists) {
      return res.json({
        error: {
          "type": "exists",
          "parameter_name": "subLevelsExists",
          "message": "Sub nivel não existe."
        }
      })
    }


    const {
      id,
    } = await _Levelsubcats2.default.create(req.body);

    return res.json(
      await _Levelsubcats2.default.findOne({
        where: { id },
        attributes: ['id'],
        include: [
          {
            model: _Prodsubcats2.default,
            as: 'prodsubcat',
            attributes: ['id'],
            include: [
              {
                model: _Subcategories2.default,
                as: 'subcategories',
                attributes: ['id', 'name', 'status']
              },
            ]
          },
          {
            model: _Sublevels2.default,
            as: 'sublevels',
            attributes: ['id', 'name', 'status']
          }
        ]
      })
    );
  }

  async update(req, res) {
    const { id } = req.params

    const LevelsubcatsExists = await _Levelsubcats2.default.findOne({
      where: { id }
    })

    if (!LevelsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcatregories",
          "message": "Sub Categoria não está relacionada ao produto, tente novamente !"
        }
      })
    }
    const subcatregoriesExists = await _Prodsubcats2.default.findOne({
      where: { id: req.body.fk_prodsubcat }
    })
    if (!subcatregoriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "prodsubcat",
          "message": "Sub Categoria não existe, tente novamente !"
        }

      })
    }
    const sublevelsExists = await _Sublevels2.default.findOne({
      where: { id: req.body.fk_sublevels }
    })
    if (!sublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevelsExists",
          "message": "Sub nivel não existe, tente novamente !"
        }
      })
    }

    const { id: idsublevel } = await LevelsubcatsExists.update(req.body);

    return res.json(
      await _Levelsubcats2.default.findOne({
        where: { id: idsublevel },
        attributes: ['id'],
        include: [
          {
            model: _Prodsubcats2.default,
            as: 'prodsubcat',
            attributes: ['id'],
            include: [
              {
                model: _Subcategories2.default,
                as: 'subcategories',
                attributes: ['id', 'name', 'status']
              },
            ]
          },
          {
            model: _Sublevels2.default,
            as: 'sublevels',
            attributes: ['id', 'name', 'status']
          }
        ]
      })
    );
  }

  async delete(req, res) {
    const { id } = req.params

    const LevelsubcatsExists = await _Levelsubcats2.default.findOne({
      where: { id, status: true },
    });
    if (!LevelsubcatsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "provider",
          "message": "Relação entre produto e sub categoria não existe, tente novamente !"
        }
      });
    }

    await LevelsubcatsExists.destroy();

    return res.json({ msg: 'Vinculo entre sub categoria e produto foi excluida' })
  }
}

exports. default = new LevelsubcatsController();
