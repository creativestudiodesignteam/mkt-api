"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize');
var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);


class GridsController {
  async index(req, res) {
    const { fk_products } = req.params
    const grids = await _Grids2.default.findAll({
      where: {
        fk_products, status: true,
      },
    })

    return res.json(grids);
  }

  async store(req, res) {
    req.body.fk_users = req.userId;

    const schema = Yup.object().shape({
      amount: Yup.number().integer().required(),
      price: Yup.number().required(),
      fk_products: Yup.number(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      })
    }

    const productsExists = await _Products2.default.findByPk(req.body.fk_products);

    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produtos não existe, tente novamente !"
        }
      })
    }

    const { id, amount, price } = await _Grids2.default.create(req.body);
    return res.json({
      id,
      amount,
      price
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      amount: Yup.number().integer(),
      price: Yup.number().required(),
      fk_products: Yup.number(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      })
    }

    const productsExists = await _Products2.default.findByPk(req.body.fk_products);

    if (!productsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "products",
          "message": "Produtos não existe, tente novamente !"
        }
      })
    }
    const GridsExists = await _Grids2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (!GridsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "grids",
          "message": "Relacionamento da grade não existe, tente novamente !"
        }
      })
    }
    const {
      amount,
      price
    } = await GridsExists.update(req.body);

    return res.json(
      {
        amount,
        price
      }
    );
  }

  async delete(req, res) {
    const { id } = req.params
    const GridsExists = await _Grids2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });
    if (!GridsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "grids",
          "message": "Relacionamento da grade não existe, tente novamente !"
        }
      })
    }

    await GridsExists.update({ status: false });

    return res.json({ msg: 'Grade está desabilitada' })

  }
}

exports. default = new GridsController();
