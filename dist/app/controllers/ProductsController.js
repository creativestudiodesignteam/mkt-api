"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Details = require('../models/Details'); var _Details2 = _interopRequireDefault(_Details);
var _Prodsubcats = require('../models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Datasheets = require('../models/Datasheets'); var _Datasheets2 = _interopRequireDefault(_Datasheets);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _OptionsNames = require('../models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _Options = require('../models/Options'); var _Options2 = _interopRequireDefault(_Options);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Galleries = require('../models/Galleries'); var _Galleries2 = _interopRequireDefault(_Galleries);
var _Sales = require('../models/Sales'); var _Sales2 = _interopRequireDefault(_Sales);
var _Sublevels = require('../models/Sublevels'); var _Sublevels2 = _interopRequireDefault(_Sublevels);
var _Carts = require('../models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Levelsubcats = require('../models/Levelsubcats'); var _Levelsubcats2 = _interopRequireDefault(_Levelsubcats);
var _sequelize = require('sequelize');

var _ProductHelper = require('../Helpers/products/ProductHelper'); var _ProductHelper2 = _interopRequireDefault(_ProductHelper);

class ProductsController {
  async index(req, res) {
    /* name description */
    const products = await _Products2.default.findAll({
      order: [['created_at', 'asc']],
      where: { fk_users: req.userId, status: true },
      attributes: [
        'id',
        'name',
        'sku',
        'description',
        'brand',
        'model',
        'condition',
        'status',
      ],
      include: [
        {
          model: _Categories2.default,
          as: 'categories',
          attributes: ['id', 'name', 'status'],
        },
        {
          model: _Files2.default,
          as: 'file',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
        {
          model: _Details2.default,
          as: 'details',
          attributes: [
            'id',
            'length',
            'height',
            'width',
            'weight_unit',
            'weight',
            'situation',
            'type_packing',
            'delivery_time',
          ],
        },
      ],
    });
    const promisesGrids = products.map(async response => {
      const gridsList = await _Grids2.default.findAll({
        where: { fk_products: response.id, status: true },
        attributes: [
          [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
          [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
          [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock'],
        ],
      });

      const {
        id,
        name,
        description,
        sku,
        brand,
        model,
        condition,
        status,
        categories,
        file,
        details,
      } = response;
      return {
        id,
        name,
        description,
        sku,
        brand,
        model,
        condition,
        status,
        categories,
        thumb: file,
        details,
        infos: gridsList[0],
      };
    });

    const grids = await Promise.all(promisesGrids);

    return res.json(grids);
  }

  async findByIdV2(req, res) {
    try {
      const { id } = req.params;

      const products = await _Products2.default.findOne({
        where: { id, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: _Details2.default,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight_unit',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(400).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }
      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        details: detailsProd,
        description: descriptionProd,
        status: statusProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        file: fileProd,
      } = products;

      const datasheet = await _Datasheets2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const grids = await _Grids2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const optionsGrids = await _OptionsGrids2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Options2.default,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: _OptionsNames2.default,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await _Galleries2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Files2.default,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });

        return { Grids: response, optionsGrids, gallery };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        thumb: { id: fileProd.id, url: fileProd.url },
        categories: categoriesProd,
        details: detailsProd,
        datasheet: datasheet,
        grids: GridsList,
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async findById(req, res) {
    try {
      const { id } = req.params;

      const products = await _Products2.default.findOne({
        where: { id, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: _Details2.default,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight_unit',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(400).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }

      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        description: descriptionProd,
        status: statusProd,
        details,
        file: files,
      } = products;

      const datasheet = await _Datasheets2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const grids = await _Grids2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const optionsGrids = await _OptionsGrids2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Options2.default,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: _OptionsNames2.default,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await _Galleries2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Files2.default,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });

        return { Grids: response, optionsGrids, gallery };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        product: {
          id: idProd,
          name: nameProd,
          description: descriptionProd,
          statusProd,
          thumb: { id: files.id, url: files.url },
          details,
          categories: categoriesProd,
          datasheet,
          GridsList,
        },
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async related(req, res) {
    try {
      const { fk_products, limit } = req.params;

      const productsList = await _Products2.default.findOne({
        where: { id: fk_products, status: true },
        order: [['created_at', 'asc']],
        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
        ],
      });

      const categoriesproductsList = await _Products2.default.findAll({
        where: {
          id: {
            [_sequelize.Op.ne]: fk_products,
          },
          status: true,
          fk_categories: productsList.categories.id,
        },
        attributes: ['id', 'name', 'description', 'status'],
        limit: limit,
        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: _Details2.default,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight_unit',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      const promisesGrids = categoriesproductsList.map(async response => {
        const gridsList = await _Grids2.default.findAll({
          where: { fk_products: response.id, status: true },
          attributes: [
            [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
            [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
            [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, error }) {
      return res.json({ error: message });
    }
  }

  async findByIdReqUser(req, res) {
    try {
      const { id } = req.params;

      const products = await _Products2.default.findOne({
        order: [['created_at', 'asc']],
        where: { id, fk_users: req.userId, status: true },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],

        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: _Details2.default,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight_unit',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });
      if (!products) {
        return res.status(400).json({
          error: {
            type: 'not_exists',
            parameter_name: 'products',
            message: 'Produto não existe, tente novamente !',
          },
        });
      }

      const {
        id: idProd,
        name: nameProd,
        categories: categoriesProd,
        details: detailsProd,
        description: descriptionProd,
        status: statusProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        file: fileProd,
      } = products;

      const dataSheet = await _Datasheets2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'name', 'description', 'status'],
      });

      const prodsubcats = await _Prodsubcats2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: _Subcategories2.default,
            as: 'subcategories',
            attributes: ['id', 'name', 'status', 'status'],
          },
        ],
      });

      const grids = await _Grids2.default.findAll({
        where: { fk_products: id, status: true },
        attributes: ['id', 'amount', 'price', 'status'],
      });

      const promisesGrids = grids.map(async response => {
        const options = await _OptionsGrids2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Options2.default,
              as: 'options',
              attributes: ['id', 'name', 'type', 'status'],
            },
            {
              model: _OptionsNames2.default,
              as: 'options_names',
              attributes: ['id', 'name', 'type', 'status'],
            },
          ],
        });

        const gallery = await _Galleries2.default.findAll({
          where: { fk_grids: response.id, status: true },
          attributes: ['id', 'status'],
          include: [
            {
              model: _Files2.default,
              as: 'files',
              attributes: ['id', 'name', 'path', 'url', 'status'],
            },
          ],
        });
        /*         const { id: idOptions, name: nameOptions, type: typeOptions, status: statusOptions } = options
                console.log(options.options_names) */

        const promisesGalery = gallery.map(async response => {
          return {
            id: response.id,
            status: response.status,
            files: {
              id: response.files.id,
              url: response.files.url,
            },
          };
        });

        const galleryList = await Promise.all(promisesGalery);

        return {
          id: response.id,
          amount: response.amount,
          price: response.price,
          options_grids: options,
          gallery: galleryList,
        };
      });

      const GridsList = await Promise.all(promisesGrids);

      return res.json({
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        thumb: { id: fileProd.id, url: fileProd.url },
        categories: categoriesProd,
        details: detailsProd,
        datasheet: dataSheet,
        subcategories: prodsubcats,
        grids: GridsList,
      });
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }

  async store(req, res) {
    try {
      const { products, subcategories, details, grids, datasheet } = req.body;
      /* ======= VALIDAÇÃO ========= */
      //Validando os detalhes e o produto
      const schema = Yup.object().shape({
        details: Yup.object().shape({
          length: Yup.string().required('Por favor, digite o comprimento'),
          height: Yup.string().required('Por favor digite a altura'),
          width: Yup.string().required('Por favor digite a largura'),
          weight_unit: Yup.string().required(''),
          weight: Yup.string().required('Por favor digite o peso do produto'),
          type_packing: Yup.string().required(
            'Por favor selecione o tipo de pacote'
          ),
          delivery_time: Yup.string(),
        }),

        products: Yup.object().shape({
          name: Yup.string().required('Por favor digite o nome do produto'),
          description: Yup.string().required(
            'Por favor digite a descrição do produto'
          ),
          brand: Yup.string(),
          model: Yup.string(),
          condition: Yup.string(),
          sku: Yup.string(),
        }),
      });

      await schema.validate({ products, details });

      //Validando se existe grid no payload
      if (!grids || !grids.length > 0) {
        throw {
          toError: function() {
            return {
              type: 'grids_general',
              parameter_name: 'grids',
              error: 'Por favor cadastre os atributos do produto',
            };
          },
        };
      }

      //validando dados das grids
      const gridsMap = grids.map(async response => {
        //Validando os arquivos das grids
        const schemaGrids = Yup.object().shape({
          grids: Yup.object().shape({
            amount: Yup.number()
              .integer()
              .required('Por favor digite o estoque do produto'),
            price: Yup.number()
              .integer()
              .required('Por favor digite o valor do produto'),
          }),
        });
        await schemaGrids.validate({ grids: response });

        //Tetando se os arquivos de fotos existem
        const galleryMap = response.gallery.map(async responseGallery => {
          const fileExists = await _Files2.default.findOne({
            where: { id: responseGallery.fk_files },
          });
          if (!fileExists) {
            throw {
              toError: function() {
                return {
                  type: 'not_exists_file',
                  parameter_name:
                    'grids.gallery.id=' + responseGallery.fk_files,
                  error: 'Por favor verifique os arquivos de imagens',
                };
              },
            };
          }
          return responseGallery;
        });

        await Promise.all(galleryMap);

        const optionsgridsMap = response.options_grids.map(
          async responseOptions => {
            const optionsExists = await _Options2.default.findOne({
              where: { id: responseOptions.fk_options },
            });

            if (!optionsExists) {
              throw {
                toError: function() {
                  return {
                    type: 'not_exists_options',
                    parameter_name:
                      'grids.options.id=' + responseOptions.fk_options,
                    error: 'Por favor verifique os atributos do seu produto',
                  };
                },
              };
            }
            const optionsNamesExists = await _OptionsNames2.default.findOne({
              where: {
                id: responseOptions.fk_options_names,
                fk_options: responseOptions.fk_options,
                fk_users: req.userId,
                status: true,
              },
            });

            return responseOptions;
          }
        );

        await Promise.all(optionsgridsMap);

        return response;
      });
      const gridsPromise = await Promise.all(gridsMap);

      if (datasheet) {
        const datasheetMap = datasheet.map(async response => {
          const schema = Yup.object().shape({
            datasheet: Yup.object().shape({
              name: Yup.string().required(
                'Por favor, digite o titulo da ficha técnica'
              ),
              description: Yup.string().required(
                'Por favor, digite o conteúdo da ficha técnica'
              ),
            }),
          });
          await schema.validate({ datasheet: response });
        });

        await Promise.all(datasheetMap);
      }
      if (!subcategories || !subcategories.length > 0) {
        throw {
          toError: function() {
            return {
              type: 'subcategories_general',
              parameter_name: 'subcategories',
              error: 'Por favor insira as subcategorias do seu produto',
            };
          },
        };
      }

      if (products.fk_categories) {
        const subcategoriesMap = subcategories.map(async response => {
          const subcategoriesExists = await _Subcategories2.default.findOne({
            where: {
              id: response.fk_subcategories,
              fk_categories: products.fk_categories,
              status: true,
            },
          });
          if (!subcategoriesExists) {
            throw {
              toError: function() {
                return {
                  type: 'subcategories_not_exists',
                  parameter_name:
                    'subcategories.id=' + response.fk_subcategories,
                  error: 'Sua sub categoria não existe, tente novamente',
                };
              },
            };
          }
        });
        await Promise.all(subcategoriesMap);
      }

      /* ============ INICIO DO CADASTRO============= */

      const { id: idDetails } = await _Details2.default.create(req.body.details);
      products.fk_users = req.userId;
      products.fk_details = idDetails;
      const { id: idProduct } = await _Products2.default.create(products);

      if (subcategories) {
        const subcategoriesCreate = subcategories.map(async response => {
          await _Prodsubcats2.default.create({
            fk_subcategories: response.fk_subcategories,
            fk_products: idProduct,
          });
        });
        await Promise.all(subcategoriesCreate);
      }

      const gridsCreate = grids.map(async response => {
        const { id: idGrid } = await _Grids2.default.create({
          amount: response.amount,
          price: response.price,
          fk_products: idProduct,
          fk_users: req.userId,
        });

        const galleryCreate = response.gallery.map(async response => {
          await _Galleries2.default.create({
            fk_files: response.fk_files,
            fk_grids: idGrid,
          });
        });

        const optionsGridsCreate = response.options_grids.map(
          async response => {
            await _OptionsGrids2.default.create({
              fk_options: response.fk_options,
              fk_options_names: response.fk_options_names,
              fk_users: req.userId,
              fk_grids: idGrid,
            });
          }
        );
        await Promise.all(optionsGridsCreate);
      });
      await Promise.all(gridsCreate);

      if (datasheet) {
        const datasheetCreate = datasheet.map(async response => {
          await _OptionsGrids2.default.create({
            name: response.name,
            description: response.description,
            fk_products: idProduct,
          });
        });
        await Promise.all(datasheetCreate);
      }

      return res.json({
        status: 'success',
        message: 'Cadastro efetuado com sucesso',
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async update(req, res) {
    try {
      const { id } = req.params;
      const errors = [];
      const { products, subcategories, details, grids, datasheet } = req.body;

      /* ======= VALIDAÇÃO ========= */
      //Validando os detalhes e o produto
      const schema = Yup.object().shape({
        details: Yup.object().shape({
          length: Yup.string(),
          height: Yup.string(),
          width: Yup.string(),
          weight_unit: Yup.string(),
          weight: Yup.string(),
          type_packing: Yup.string(),
          situation: Yup.string(),
          delivery_time: Yup.string(),
        }),
        products: Yup.object().shape({
          name: Yup.string(),
          description: Yup.string(),
          brand: Yup.string(),
          model: Yup.string(),
          condition: Yup.string(),
          sku: Yup.string(),
        }),
      });

      await schema.validate({ products, details });

      const productsExists = await _Products2.default.findOne({
        where: { id, status: true, fk_users: req.userId },
        attributes: [
          'id',
          'name',
          'description',
          'sku',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: _Categories2.default,
            as: 'categories',
            attributes: ['id', 'name', 'status'],
          },
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
          {
            model: _Details2.default,
            as: 'details',
            attributes: [
              'id',
              'length',
              'height',
              'width',
              'weight_unit',
              'weight',
              'situation',
              'type_packing',
              'delivery_time',
            ],
          },
        ],
      });

      if (!productsExists) {
        throw {
          toError: function() {
            return {
              type: 'not_exists',
              parameter_name: 'products',
              error: 'O produto selecionado não existe !',
            };
          },
        };
      }

      if (products) {
        await productsExists.update(products);
      }

      if (details) {
        if (!productsExists.details) {
          const { id: fk_details } = await _Details2.default.create({
            length: details.length,
            height: details.height,
            width: details.width,
            weight_unit: details.weight_unit,
            weight: details.weight,
            situation: details.situation,
            type_packing: details.type_packing,
            delivery_time: details.delivery_time,
          });
          // console.log(fk_details);
          await productsExists.update({ fk_details });
        }

        await productsExists.details.update(details);
      }

      if (datasheet) {
        if (datasheet.length === 0) {
          await _Datasheets2.default.destroy({
            where: { fk_products: id },
          });
        } else {
          const datasheetsfindAll = await _Datasheets2.default.findAll({
            where: { fk_products: id },
          });
          const datasheetsAll = datasheetsfindAll.map(async datasheetResp => {
            return datasheetResp.id;
          });

          const datasheetAlls = await Promise.all(datasheetsAll);

          const datasheetMap = datasheet.map(async response => {
            if (response.id) {
              const datasheetExists = await _Datasheets2.default.findByPk(response.id);
              await datasheetExists.update({
                name: response.name,
                description: response.description,
              });
            } else {
              const createDatasheet = await _Datasheets2.default.create({
                name: response.name,
                description: response.description,
                fk_products: id,
              });

              const destroyDatasheets = datasheetAlls.filter(
                g => g !== createDatasheet.id
              );
              if (createDatasheet.id !== parseInt(destroyDatasheets)) {
                const promiseDestroy = destroyDatasheets.map(async response => {
                  await _Datasheets2.default.destroy({
                    where: { id: parseInt(response) },
                  });
                });

                await Promise.all(promiseDestroy);
              }
            }
          });
          await Promise.all(datasheetMap);
        }
      }

      if (subcategories) {
        if (subcategories.length === 0) {
          await _Prodsubcats2.default.destroy({
            where: { fk_products: id },
          });
        } else {
          const subcategoriesfindAll = await _Prodsubcats2.default.findAll({
            where: { fk_products: id },
          });
          const subcategoriesAll = subcategoriesfindAll.map(async response => {
            return response.id;
          });

          const subcategoriesAlls = await Promise.all(subcategoriesAll);

          const subcategoriesBody = subcategories.map(async response => {
            if (!response.id) {
              const subcategoriesExists = await _Subcategories2.default.findOne({
                where: {
                  id: response.fk_subcategories,
                  fk_categories: productsExists.fk_categories,
                },
              });
              if (subcategoriesExists) {
                const createSubProd = await _Prodsubcats2.default.create({
                  fk_subcategories: response.fk_subcategories,
                  fk_products: id,
                });

                const destroyProdsub = subcategoriesAlls.filter(
                  g => g !== createSubProd.id
                );

                if (createSubProd.id !== parseInt(destroyProdsub)) {
                  const promiseDestroy = destroyProdsub.map(async res => {
                    console.log(res);
                    await _Prodsubcats2.default.destroy({
                      where: { id: parseInt(res) },
                    });
                  });

                  await Promise.all(promiseDestroy);
                }
              } else {
                /* alert = { msg: 'Subcategoria não existe' } */
                errors.push({
                  type: 'not_exists',
                  parameter_name: 'subcategories',
                  message:
                    'Sub categoria selecionada não faz parte da categoria, tente novamente ou contate o suporte',
                });
              }
            }
            return response.id;
          });
          await Promise.all(subcategoriesBody);
        }
      }
      if (grids) {
        const gridsfindAll = await _Grids2.default.findAll({
          where: { fk_products: id, status: true },
        });
        const gridssAll = gridsfindAll.map(async response => {
          return response.id;
        });

        const gridsAlls = await Promise.all(gridssAll);

        if (grids.length === 0) {
          //DESTRUIR SAPORRA TODA
        } else {
          const gridsDatasheet = grids.map(async response => {
            if (response.id) {
              const gridsExists = await _Grids2.default.findOne({
                where: { id: response.id, status: true },
              });
              if (!gridsExists) {
                const gridCreate = await _Grids2.default.create({
                  amount: response.amount,
                  price: response.price,
                  fk_products: id,
                  fk_users: req.userId,
                });
                const destroyGrids = gridsAlls.filter(
                  g => g !== gridsUpdate.id
                );

                if (gridCreate.id !== parseInt(destroyGrids)) {
                  const promiseDestroy = destroyGrids.map(async res => {
                    const salesExists = await _Sales2.default.findAll({
                      where: { fk_grids: res },
                    });

                    //Apgando
                    if (salesExists.length < 0) {
                      await _OptionsGrids2.default.destroy({
                        where: { fk_grids: res, status: true },
                      });

                      await _Galleries2.default.destroy({
                        where: { fk_grids: res, status: true },
                      });
                      await _Grids2.default.destroy({
                        where: { id: res, status: true },
                      });
                    } else {
                      await Promise.all(
                        await _OptionsGrids2.default.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );

                      await Promise.all(
                        await _Galleries2.default.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );

                      await Promise.all(
                        await _Grids2.default.findAll({
                          where: { id: res, status: true },
                        }).map(async response => {
                          await response.update({
                            status: false,
                          });
                        })
                      );
                    }
                  });

                  await Promise.all(promiseDestroy);
                }

                _ProductHelper2.default.galleryFunction({
                  fk_grids: gridCreate.id,
                  gallery: response.gallery,
                });

                _ProductHelper2.default.OptionGrids({
                  fk_users: req.userId,
                  fk_grids: gridCreate.id,
                  options_grids: response.options_grids,
                });
              } else {
                const gridsUpdate = await gridsExists.update({
                  amount: response.amount,
                  price: response.price,
                });

                await _ProductHelper2.default.galleryFunction({
                  fk_grids: gridsUpdate.id,
                  gallery: response.gallery,
                });

                await _ProductHelper2.default.OptionGrids({
                  fk_grids: gridsUpdate.id,
                  options_grids: response.options_grids,
                });
              }
            } else {
              const gridCreate = await _Grids2.default.create({
                amount: response.amount,
                price: response.price,
                fk_products: id,
                fk_users: req.userId,
              });
              const destroyGrids = gridsAlls.filter(g => g !== gridCreate.id);

              if (gridCreate.id !== parseInt(destroyGrids)) {
                const promiseDestroy = destroyGrids.map(async res => {
                  const salesExists = await _Sales2.default.findAll({
                    where: { fk_grids: res },
                  });
                  //Apagando
                  if (salesExists.length < 0) {
                    await _OptionsGrids2.default.destroy({
                      where: { fk_grids: res, status: true },
                    });

                    await _Galleries2.default.destroy({
                      where: { fk_grids: res, status: true },
                    });
                    await _Grids2.default.destroy({
                      where: { id: res, status: true },
                    });
                  } else {
                    await Promise.all(
                      await _OptionsGrids2.default.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        console.log('aoooo');
                        await response.update({
                          status: false,
                        });
                      })
                    );

                    await Promise.all(
                      await _Galleries2.default.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        await response.update({
                          status: false,
                        });
                      })
                    );

                    await Promise.all(
                      await _Grids2.default.findAll({
                        where: { id: res, status: true },
                      }).map(async response => {
                        await response.update({
                          status: false,
                        });
                      })
                    );
                  }
                });

                await Promise.all(promiseDestroy);
              }

              _ProductHelper2.default.galleryFunction({
                fk_grids: gridCreate.id,
                gallery: response.gallery,
              });
              _ProductHelper2.default.OptionGrids({
                fk_grids: gridCreate.id,
                options_grids: response.options_grids,
              });
            }
          });
          await Promise.all(gridsDatasheet);
        }
      }

      return res.json(await _ProductHelper2.default.List({ id }));
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError());
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString(),
        });
      }

      return res.json({ error: message });
    }
  }

  async delete(req, res) {
    const { id } = req.params;
    const ProductsExists = await _Products2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (!ProductsExists) {
      return res.json({
        error: {
          type: 'not_exists',
          parameter_name: 'products',
          message: 'Produto não existe, tente novamente !',
        },
      });
    }
    // ==========Puxando subprods =========
    const prodsubExists = await _Prodsubcats2.default.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });
    // ==========Puxando details =========
    const detailsExists = await _Details2.default.findOne({
      where: { id: ProductsExists.fk_details, status: true },
    });
    // ==========Puxando datasheet =========
    const datasheetExists = await _Datasheets2.default.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });
    // ==========Puxando grids =========
    const gridsExists = await _Grids2.default.findAll({
      where: { fk_products: ProductsExists.id, status: true },
    });

    if (prodsubExists) {
      const promisesprodsubExists = prodsubExists.map(async prodsub => {
        await prodsub.update({ status: false });
      });
      await Promise.all(promisesprodsubExists);
    }

    if (ProductsExists.fk_thumb) {
      const fk_thumbExists = await _Files2.default.findOne({
        where: { id: ProductsExists.fk_thumb },
      });

      if (fk_thumbExists) {
        await fk_thumbExists.update({ status: false });
      }
    }

    if (gridsExists) {
      const promisesCategories = gridsExists.map(async grids => {
        if (grids) {
          await grids.update({ status: false });
        }
      });
      await Promise.all(promisesCategories);
    }

    if (detailsExists) {
      await detailsExists.update({ status: false });
    }

    if (datasheetExists) {
      const promisesDatasheet = datasheetExists.map(async datasheet => {
        if (datasheet) {
          await datasheet.update({ status: false });
        }
      });

      await Promise.all(promisesDatasheet);
    }

    if (ProductsExists) {
      await ProductsExists.update({ status: false });
    }

    return res.json({ msg: 'Produto desabilitado' });
  }
}

exports. default = new ProductsController();
