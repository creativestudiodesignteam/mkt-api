"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Sales = require('../models/Sales'); var _Sales2 = _interopRequireDefault(_Sales);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Providers = require('../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Payments = require('../models/Payments'); var _Payments2 = _interopRequireDefault(_Payments);
var _Fretes = require('../models/Fretes'); var _Fretes2 = _interopRequireDefault(_Fretes);

const Payload = require('../services/safe2pay/Transaction');

class SalesController {

  async index(req, res) {
    const sales = await _Sales2.default.findAll({
      where: { fk_users: req.userId },
      attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status', 'canceled_at', 'created_at'],
      include: [
        {
          model: _Grids2.default,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: _Products2.default,
              as: 'products',
              attributes: ['id', 'name', 'status'],
              include: [
                {
                  model: _Files2.default,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: _Providers2.default,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name',
          ],
        },
        {
          model: _Payments2.default,
          as: 'payments',
          attributes: ['id', 'code', 'method', 'situations', 'status', 'created_at'],
        },
        {
          model: _Fretes2.default,
          as: 'fretes',
          attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo'],
        }
      ],
    });

    const salesPromise = sales.map(async response => {
      const { id, hash, amount, quota, price, status, created_at, canceled_at, grids, providers, payments, fretes, } = response

      return ({
        id,
        hash,
        amount,
        quota,
        price,
        status,
        canceled_at,
        providers,
        products: {
          id: grids.products.id,
          name: grids.products.name,
          status: grids.products.status,
          thumb: grids.products.file.url,
          grid: {
            id: grids.id,
            price: grids.price,
            status: grids.status,
          }
        },
        shipping: {
          id: fretes.id,
          value: fretes.valor,
          type: fretes.tipo,
          code: fretes.cod_servico,
          deadline: fretes.prazo,
        },
        progress: {
          request: {
            id,
            situation: 'Pedido Entregue',
            code: hash,
            date: created_at,
            status: true,
            canceled_at
          },

          payments: {
            id: payments.id,
            code: payments.code,
            situation: payments.situations,
            date: payments.created_at,
            status: payments.status,
          },
          shipping: {
            /*           id: '',
                      situation: '', */
            status: false
          },
          delived_request: {
            /* id: '',
            situation: '', */
            status: false
          }
        },
      })
    })
    const promise = await Promise.all(salesPromise);

    /* hash, amount, quota, price, status, created_at, canceled_at, grids, providers, payments, fretes, */
    if (sales.length <= 0) {
      return res.json({
        error: {
          'type': 'not_exists_parameter',
          'parameter_name': 'sales',
          'message': 'venda não existe.',
        },
      });
    }

    return res.json(promise)
  }

  async indexSales(req, res) {
    console.log('alooooooooooooooooooooooooooooooo')
    const sales = await _Sales2.default.findAll({
      where: { fk_users: req.userId },
      attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status', 'canceled_at', 'created_at'],
      include: [
        {
          model: _Grids2.default,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: _Products2.default,
              as: 'products',
              attributes: ['id', 'name', 'status'],
              include: [
                {
                  model: _Files2.default,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: _Providers2.default,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name',
          ],
        },
        {
          model: _Payments2.default,
          as: 'payments',
          attributes: ['id', 'code', 'method', 'situations', 'status', 'created_at'],
        },
        {
          model: _Fretes2.default,
          as: 'fretes',
          attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo'],
        }
      ]
    });


    const salesPromise = sales.map(async response => {
      const { id, hash, amount, quota, price, status, created_at, canceled_at, grids, providers, payments, fretes, } = response

      return ({
        id,
        hash,
        amount,
        quota,
        price,
        status,
        canceled_at,
        providers,
        products: {
          id: grids.products.id,
          name: grids.products.name,
          status: grids.products.status,
          thumb: grids.products.file.url,
          grid: {
            id: grids.id,
            price: grids.price,
            status: grids.status,
          }
        },
        shipping: {
          id: fretes.id,
          value: fretes.valor,
          type: fretes.tipo,
          code: fretes.cod_servico,
          deadline: fretes.prazo,
        },
        progress: {
          request: {
            id,
            situation: 'Pedido Entregue',
            code: hash,
            date: created_at,
            status: true,
            canceled_at
          },

          payments: {
            id: payments.id,
            code: payments.code,
            situation: payments.situations,
            date: payments.created_at,
            status: payments.status,
          },
          shipping: {
            /*           id: '',
                      situation: '', */
            status: false
          },
          delived_request: {
            /* id: '',
            situation: '', */
            status: false
          }
        },
      })
    })

    const promiseSales = await Promise.all(salesPromise)
    return res.json(promiseSales)
  }

  async detail(req, res) {
    const { id } = req.params;

    const { hash, amount, quota, price, status, created_at, canceled_at, grids, providers, payments, fretes } = await _Sales2.default.findOne({
      where: { id },
      attributes: ['id', 'hash', 'amount', 'quota', 'price', 'status', 'canceled_at', 'created_at'],
      include: [
        {
          model: _Grids2.default,
          as: 'grids',
          attributes: ['id', 'amount', 'price', 'status'],
          include: [
            {
              model: _Products2.default,
              as: 'products',
              attributes: ['id', 'name', 'status'],
              include: [
                {
                  model: _Files2.default,
                  as: 'file',
                  attributes: ['id', 'name', 'path', 'url', 'status'],
                },
              ],
            },
          ],
        },
        {
          model: _Providers2.default,
          as: 'providers',
          attributes: ['id', 'cnpj', 'fantasy_name',
          ],
        },
        {
          model: _Payments2.default,
          as: 'payments',
          attributes: ['id', 'code', 'method', 'situations', 'status', 'created_at'],
        },
        {
          model: _Fretes2.default,
          as: 'fretes',
          attributes: ['id', 'valor', 'tipo', 'cod_servico', 'prazo'],
        }
      ],
    })
    return res.json({
      id,
      hash,
      amount,
      quota,
      price,
      status,
      canceled_at,
      providers,
      products: {
        id: grids.products.id,
        name: grids.products.name,
        status: grids.products.status,
        thumb: grids.products.file.url,
        grid: {
          id: grids.id,
          price: grids.price,
          status: grids.status,
        }
      },
      shipping: {
        id: fretes.id,
        value: fretes.valor,
        type: fretes.tipo,
        code: fretes.cod_servico,
        deadline: fretes.prazo,
      },
      progress: {
        request: {
          id,
          situation: 'Pedido Entregue',
          code: hash,
          date: created_at,
          status: true,
          canceled_at
        },

        payments: {
          id: payments.id,
          code: payments.code,
          situation: payments.situations,
          date: payments.created_at,
          status: payments.status,
        },
        shipping: {
          /*           id: '',
                    situation: '', */
          status: false
        },
        delived_request: {
          /* id: '',
          situation: '', */
          status: false
        }
      },
    })

  }

  async delete(req, res) {
    const { hash } = req.params;

    const salesExists = await _Sales2.default.findOne({
      where: { hash: hash, status: true }
    });
    console.log(salesExists)
    const paymentExists = await _Payments2.default.findOne({
      where: { code: hash, status: true }
    })
    console.log(paymentExists)
    if (!salesExists || !paymentExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'sales',
          'message': 'Erro ao cancelar venda!',
        },
      });
    }

    const cancelSafe2Pay = await Payload.cancelTransaction(hash, paymentExists.method);
    console.log(cancelSafe2Pay);
    if (cancelSafe2Pay.HasError) {
      return res.json({
        error: {
          'type': 'payment_error',
          'parameter_name': 'sale',
          'message': cancelSafe2Pay.Error,
        },
      });
    }

    await salesExists.update({ status: false });

    return res.json({ msg: 'venda cancelada' });
  }
}

exports. default = new SalesController();
