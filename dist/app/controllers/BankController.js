"use strict";Object.defineProperty(exports, "__esModule", {value: true});const request = require("request");

class BankController {
  async bankCode (req, res) {

    var options = { method: 'GET',
      url: "https://api.safe2pay.com.br/v2/util/Bank/List",
      headers: { 'X-API-KEY': process.env.SAFE2PAY_KEY_SANDBOX}
    };

    request(options, function (error, response, body) {
      if (error) throw new Error(error);
      let responseDetail = JSON.parse(body);
      return res.json(responseDetail.ResponseDetail)
    });
  }
}

exports. default = new BankController();
