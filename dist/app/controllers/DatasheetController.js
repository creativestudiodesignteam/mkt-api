"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Datasheets = require('../models/Datasheets'); var _Datasheets2 = _interopRequireDefault(_Datasheets);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);


class DatasheetsController {
  async index(req, res) {
    const datasheets = await _Datasheets2.default.findAll({
      where: { fk_products: req.params.fk_products, status: true },
      attributes: ['id', 'name', 'description', 'status'],
    });

    return res.json(datasheets);
  }

  async store(req, res) {
    /* cpf telephone gender birthday */
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      description: Yup.string().required(),
    });
    const productsExits = await _Products2.default.findOne({ where: { id: req.params.fk_products } });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }

    if (!productsExits) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'products',
          'message': 'Produto não existe, tente novamente !',
        },
      });
    }
    req.body.fk_products = req.params.fk_products;
    const {
      id,
      name,
      description,
    } = await _Datasheets2.default.create(req.body);


    return res.json({
      id,
      name,
      description,
    });
  }

  async update(req, res) {
    const { id } = req.params;
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      description: Yup.string().required(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const DatasheetsExists = await _Datasheets2.default.findOne({
      where: { id, status: true },
    });
    if (!DatasheetsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'datasheets',
          'message': 'Ficha técnica não existe, tente novamente !',
        },
      });
    }

    const {
      name,
      description,
    } = await DatasheetsExists.update(req.body);

    return res.json({
      id,
      name,
      description,
    });
  }

  async delete(req, res) {
    const { id } = req.params;

    const DatasheetsExists = await _Datasheets2.default.findOne({
      where: { id, status: true },
    });

    if (!DatasheetsExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'datasheets',
          'message': 'Ficha técnica não existe, tente novamente !',
        },
      });
    }

    await DatasheetsExists.destroy();

    return res.json({ msg: 'Ficha técnica deletada' });

  }
}

exports. default = new DatasheetsController();
