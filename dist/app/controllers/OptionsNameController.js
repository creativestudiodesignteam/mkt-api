"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _OptionsNames = require('../models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _sequelize = require('sequelize');
/*import Files from '../models/Files'; */

class OptionsNamesController {
  async index(req, res) {
    const { fk_options } = req.params;

    const optionsNames = await _OptionsNames2.default.findAll({
      where: {/*
        fk_users: req.userId, */
        status: true,
        fk_options
      },
      attributes: ['id', 'name', 'type', 'status'],
    })

    return res.json(optionsNames);
  }

  async store(req, res) {
    try {
      req.body.fk_users = req.userId;

      const promisesOptionsNames = req.body.map(async response => {

        const schema = Yup.object().shape({
          name: Yup.string().required(),
        });

        if (!(await schema.isValid(response))) {
          return res.json({
            error: {
              "type": "validation_invalid",
              "parameter_name": "yup",
              "message": "Digitou todos os campos certinhos ?, tente novamente"
            }
          });
        }
        const OptionsNamesExists = await _OptionsNames2.default.findOne({
          where: { name: response.name, fk_options: response.fk_options, fk_users: req.userId }
        })

        if (OptionsNamesExists) {
          throw {
            toError: function () {
              return {
                type: 'already_exists',
                parameter_name: `OptionsNames.${response.name}`,
                message: `A caracteristica ${response.name} já existe`
              };
            }
          }
        }

        response.fk_users = req.userId;
        const {
          id,
          name,
          type
        } = await _OptionsNames2.default.create(response);


        return ({
          id,
          name,
          type
        });
      })
      const options_names = await Promise.all(promisesOptionsNames);

      return res.json(options_names)

    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError())
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString()
        })
      }

      return res.json({ error: message })

    }
  }

  async update(req, res) {
    try {

      const { id } = req.params

      const schema = Yup.object().shape({
        name: Yup.string(),
      });
      if (!(await schema.isValid(req.body))) {
        return res.json({
          error: {
            "type": "validation_invalid",
            "parameter_name": "yup",
            "message": "Digitou todos os campos certinhos ?, tente novamente"
          }
        });
      }
      const OptionsNamesExists = await _OptionsNames2.default.findOne({
        where: { id, status: true },
      });
      if (!OptionsNamesExists) {
        return res.json({
          error: {
            "type": "not_exists",
            "parameter_name": "option",
            "message": "Opção não existe, tente novamente !"
          }
        })
      }
      const ONExistsToName = await _OptionsNames2.default.findOne({
        where: {
          id: {
            [_sequelize.Op.ne]: id,
          },
          name: req.body.name, fk_options: OptionsNamesExists.fk_options, fk_users: req.userId
        }
      })

      if (ONExistsToName) {
        throw {
          toError: function () {
            return {
              type: 'already_exists',
              parameter_name: `OptionsNames.${req.body.name}`,
              message: `A caracteristica ${req.body.name} já está cadastrada`
            };
          }
        }
      }
      const {
        name,
      } = await OptionsNamesExists.update(req.body);

      return res.json({
        id,
        name,
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError())
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString()
        })
      }

      return res.json({ error: message })

    }
  }

  async delete(req, res) {
    const { id } = req.params

    const OptionsNamesExists = await _OptionsNames2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });

    if (!OptionsNamesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "option",
          "message": "Opção não existe, tente novamente !"
        }
      });
    }

    const OptionsExistsinOPGrids = await _OptionsGrids2.default.findAll({
      where: { fk_options_names: id, fk_users: req.userId },
    });

    if (OptionsExistsinOPGrids.length > 0) {
      return res.json({
        error: {
          "type": "is_used",
          "parameter_name": "is_used",
          "message": `Sua caracteristica esta sendo usada em ${OptionsExistsinOPGrids.length} produto !`
        }
      });
    }

    await OptionsNamesExists.destroy();
    return res.json({ msg: 'Atributo deletado' })

  }
}

exports. default = new OptionsNamesController();
