"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize');
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Prodsubcats = require('../models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Details = require('../models/Details'); var _Details2 = _interopRequireDefault(_Details);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);

class SearchController {

    async search(req, res) {
        try {

            const queryList = await _Products2.default.findAll({
                where: {
                    [_sequelize.Op.or]: [{
                            name: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        },
                        {
                            description: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            }
                        },
                        {
                            model: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        },

                        {
                            brand: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        }
                    ],
                    status: true
                },
                include: [{
                        model: _Categories2.default,
                        as: 'categories',
                        attributes: ['id', 'name', 'status'],
                    },
                    {
                        model: _Files2.default,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                    {
                        model: _Details2.default,
                        as: 'details',
                        attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                    },
                ]
            })

            const promisesQueryList = queryList.map(async response => {

                const gridsList = await _Grids2.default.findAll({
                    where: { fk_products: response.id },
                    attributes: [
                        [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
                        [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
                        [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock']
                    ]
                })
                const { id, name, description, sku, brand, model, status, categories, file, details } = response
                return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

            })

            const products = await Promise.all(promisesQueryList);
            if (products.length == 0) {

                const categoriesList = await _Categories2.default.findAll({
                        where: {
                            [_sequelize.Op.or]: [{
                                name: {
                                    [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                                },
                            }],
                            status: true
                        },
                    })
                    //Busca pelas categorias
                if (!categoriesList) {
                    let array = [];
                    const promisesCategoriesList = categoriesList.map(async response => {

                        const ProductList = await _Products2.default.findAll({
                            where: {
                                fk_categories: response.id,
                                status: true
                            },
                            include: [{
                                    model: _Categories2.default,
                                    as: 'categories',
                                    attributes: ['id', 'name', 'status'],
                                },
                                {
                                    model: _Files2.default,
                                    as: 'file',
                                    attributes: ['id', 'name', 'path', 'url', 'status'],
                                },
                                {
                                    model: _Details2.default,
                                    as: 'details',
                                    attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                                },
                            ]
                        })

                        const promisesProducts = ProductList.map(async response => {
                            array.push(response)
                            return (response)
                        })
                        const productsListMap = await Promise.all(promisesProducts);

                        return (productsListMap)
                    })
                    await Promise.all(promisesCategoriesList);
                    const promisesQueryList = array.map(async responseQL => {

                        const gridsList = await _Grids2.default.findAll({
                            where: { fk_products: responseQL.id, status: true },
                            attributes: [
                                [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
                                [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
                                [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock']
                            ]
                        })
                        const { id, name, description, sku, brand, model, status, categories, file, details } = responseQL
                        return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

                    })
                    const promiseQLL = await Promise.all(promisesQueryList);
                    return res.json(promiseQLL)
                }

            }

            return res.json(products)

        } catch ({ message, error }) {
            return res.json({ message })
        }
    }

    async SearchMyProducts(req, res) {
        try {

            const queryList = await _Products2.default.findAll({
                where: {
                    [_sequelize.Op.or]: [{
                            name: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        },
                        {
                            description: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            }
                        },
                        {
                            model: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        },

                        {
                            brand: {
                                [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                            },
                        }
                    ],
                    status: true,
                    fk_users: req.userId
                },
                include: [{
                        model: _Categories2.default,
                        as: 'categories',
                        attributes: ['id', 'name', 'status'],
                    },
                    {
                        model: _Files2.default,
                        as: 'file',
                        attributes: ['id', 'name', 'path', 'url', 'status'],
                    },
                    {
                        model: _Details2.default,
                        as: 'details',
                        attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                    },
                ]
            })

            const promisesQueryList = queryList.map(async response => {

                const gridsList = await _Grids2.default.findAll({
                    where: { fk_products: response.id },
                    attributes: [
                        [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
                        [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
                        [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock']
                    ]
                })
                const { id, name, description, sku, brand, model, status, categories, file, details } = response
                return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

            })

            const products = await Promise.all(promisesQueryList);
            if (products.length == 0) {

                const categoriesList = await _Categories2.default.findAll({
                        where: {
                            [_sequelize.Op.or]: [{
                                name: {
                                    [_sequelize.Op.iLike]: '%' + req.query.content + '%'
                                },
                            }],
                            status: true
                        },
                    })
                    //Busca pelas categorias
                if (!categoriesList) {
                    let array = [];
                    const promisesCategoriesList = categoriesList.map(async response => {

                        const ProductList = await _Products2.default.findAll({
                            where: {
                                fk_categories: response.id,
                                status: true
                            },
                            include: [{
                                    model: _Categories2.default,
                                    as: 'categories',
                                    attributes: ['id', 'name', 'status'],
                                },
                                {
                                    model: _Files2.default,
                                    as: 'file',
                                    attributes: ['id', 'name', 'path', 'url', 'status'],
                                },
                                {
                                    model: _Details2.default,
                                    as: 'details',
                                    attributes: ['id', 'length', 'height', 'width', 'weight_unit', 'weight', 'situation', 'type_packing'],
                                },
                            ]
                        })

                        const promisesProducts = ProductList.map(async response => {
                            array.push(response)
                            return (response)
                        })
                        const productsListMap = await Promise.all(promisesProducts);

                        return (productsListMap)
                    })
                    await Promise.all(promisesCategoriesList);
                    const promisesQueryList = array.map(async responseQL => {

                        const gridsList = await _Grids2.default.findAll({
                            where: { fk_products: responseQL.id, status: true },
                            attributes: [
                                [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
                                [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
                                [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock']
                            ]
                        })
                        const { id, name, description, sku, brand, model, status, categories, file, details } = responseQL
                        return ({ id, name, description, sku, brand, model, status, categories, thumb: file, details, infos: gridsList[0] })

                    })
                    const promiseQLL = await Promise.all(promisesQueryList);
                    return res.json(promiseQLL)
                }

            }

            return res.json(products)

        } catch ({ message, error }) {
            return res.json({ message })
        }
    }

}

exports. default = new SearchController();
