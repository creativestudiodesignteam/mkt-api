"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }const fs = require('fs');
const { promisify } = require('util');
const unlink = promisify(fs.unlink);

var _Galleries = require('../models/Galleries'); var _Galleries2 = _interopRequireDefault(_Galleries);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);

class GalleriesController {

  async index(req, res) {
    const galery = await _Galleries2.default.findAll({
      where: { fk_grids: req.params.fk_grids, status: true }
    })
    return res.json({ galery })

  }

  async store(req, res) {
    if (!req.files) {
      return res.json({
        error: {
          "type": "not_found",
          "parameter_name": "files",
          "message": "Ocorreu algum problema com suas fotos, tente novamente"
        }
      })
    }


    const filesPromises = req.files.map(async response => {
      console.log(response.path)
      return (await _Files2.default.create({ name: response.filename, path: response.filename }));
    })
    try {
      const teste = await Promise.all(filesPromises);
      return res.json(teste)
    }
    catch ({ message, ...error }) {
      return res.status(404).json({ error: message })
    }

  }

  async findGrid(req, res) {
    const { fk_grids } = req.params
    const galleries = await _Galleries2.default.findAll({
      where: { fk_grids, status: true },
      attributes: ['id', 'status'],
      include: [{
        model: _Files2.default,
        as: 'files',
        attributes: ['id', 'path', 'url'],
      }
      ]
    })
    const galleryMap = galleries.map(async response => {
      return(response.files.url)
    })

    const galleryUrl = await Promise.all(galleryMap)

    return res.json(galleryUrl)
  }
  async update(req, res) {
    const { id } = req.params;
    const { filename: path, originalname: name } = req.file;


    const GalleriesExists = await _Galleries2.default.findOne({
      where: { id, status: true },
    });

    if (!GalleriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "galleries",
          "message": "Suas fotos não existem, tente novamente"
        }
      })
    }

    const fileExists = await _Files2.default.findOne({
      where: { id: GalleriesExists.fk_files }
    })

    if (!fileExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "files",
          "message": "Suas fotos não existem, tente novamente"
        }
      })
    }
    await _Files2.default.create(
      {
        name: name,
        path: path
      }
    );

    try {

      await GalleriesExists.update({ fk_files: filesUpd.id, });

      await Promise.all([unlink('tmp/uploads/' + fileExists.path)]);

      await fileExists.destroy();

      return res.json(await _Galleries2.default.findOne({
        where: { id },
        attributes: ['id', 'status'],
        include: [
          {
            model: _Files2.default,
            as: 'files',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },

        ]
      }))
    } catch (e) {
      res.status(505).send('Ocorreu um erro interno.');
    }


  }

  async delete(req, res) {
    const { id } = req.params
    const GalleriesExists = await _Galleries2.default.findByPk(id);
    if (!GalleriesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "galery",
          "message": "Suas fotos não existem, tente novamente"
        }
      })
    }

    const FilesExists = await _Files2.default.findByPk(GalleriesExists.fk_files);
    if (!FilesExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "files",
          "message": "Suas fotos não existem, tente novamente"
        }
      })
    }

    try {
      await Promise.all([unlink('tmp/uploads/' + FilesExists.name)]);
      await GalleriesExists.destroy();
      await FilesExists.destroy();
      return res.json({ msg: 'Galeria excluida' })
    } catch (e) {
      res.status(500).send('Ocorreu um erro interno.');
    }
  }
}

exports. default = new GalleriesController();
