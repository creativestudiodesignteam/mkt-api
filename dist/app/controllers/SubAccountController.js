"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Providers = require('./../models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Users = require('./../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Addresses = require('./../models/Addresses'); var _Addresses2 = _interopRequireDefault(_Addresses);

const safe2pay = require('safe2pay');
safe2pay.enviroment.setApiKey(process.env.SAFE2PAY_KEY_SANDBOX);

class SubAccountsController {

  async store(req, res) {
    const MarketplaceRequest = safe2pay.api.MarketplaceRequest;
    const providers = await _Providers2.default.findOne({
      where: { fk_users: req.userId },
      attributes: ['id', 'fantasy_name', 'reason_social', 'state_register', 'county_register', 'telephone_commercial', 'telephone_whatsapp', 'is_panel_restricted', 'status'],
      include: [
        {
          model: _Users2.default,
          as: 'users',
          attributes: ['id', 'email']
        },
      ]
    })

    const addresses = await _Addresses2.default.findOne({
      where: { fk_users: req.userId, status: true }
    })

    const data = {
      "Name": providers.fantasy_name,
      "CommercialName": providers.reason_social,
      "Identity": providers.cnpj,
      "ResponsibleName": providers.users.name,
      "ResponsibleIdentity": providers.users.cpf,
      "Email": providers.users.email,
      "BankData": {
        "Bank": {
          "Id": 0,
          "Code": "041"
        },
        "AccountType": {
          "Code": "CC"
        },
        "BankAgency": "1676",
        "BankAgencyDigit": "0",
        "BankAccount": "41776",
        "BankAccountDigit": "7"
      },
      "Address": {
        "ZipCode": addresses.postcode,
        "Street": addresses.street,
        "Number": addresses.number,
        "Complement": addresses.complement,
        "District": addresses.neighborhood,
        "CityName": addresses.city,
        "StateInitials": addresses.state,
        "CountryName": 'Brasil'
      },
      "MerchantSplit": [
        {
          "PaymentMethodCode": "1",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        },
        {
          "PaymentMethodCode": "3",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            },
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        },
        {
          "PaymentMethodCode": "4",
          "IsSubaccountTaxPayer": false,
          "Taxes": [
            {
              "TaxTypeName": "1",
              "Tax": "1.00"
            }
          ]
        }
      ]
    }

    const teste = await MarketplaceRequest.Add(data);
    return res.json(teste)

  }


}

exports. default = new SubAccountsController();
