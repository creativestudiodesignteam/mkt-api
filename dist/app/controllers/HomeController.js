"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Categories = require('../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Subcategories = require('../models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Prodsubcats = require('../models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _sequelize = require('sequelize');
var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);

class HomeController {
  async findAll(req, res) {
    try {
      const categoriesList = await _Categories2.default.findAll({
        limit: 5,
        where: {
          status: true,
        },
      });
      const promisesSubCategories = categoriesList.map(async categories => {
        const subCategories = await _Subcategories2.default.findAll({
          raw: true,
          attributes: ['id', 'name', 'status'],
          where: {
            status: true,
            fk_categories: categories.id,
          },
        });
        const promisesProdSub = subCategories.map(async subcategories => {
          const prodsubcats = await _Prodsubcats2.default.findAll({
            limit: 6,
            /* raw: true, */
            attributes: ['id'],
            where: {
              status: true,
              fk_subcategories: subcategories.id,
            },
            attributes: ['id'],
            include: [
              {
                model: _Products2.default,
                as: 'products',
                where: { status: true },
                attributes: ['id', 'name', 'status'],
                include: [
                  {
                    model: _Files2.default,
                    as: 'file',
                    attributes: ['id', 'name', 'path', 'url'],
                  },
                ],
              },
            ],
          });

          const promisesProdutos = prodsubcats.map(async response => {
            const gridsProducts = await _Grids2.default.findOne({
              attributes: [
                [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
                [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
              ],
              where: {
                status: true,
                fk_products: response.products.id,
              },
            });
            gridsProducts;
            return {
              products: { id: prodsubcats.id, product: productsprodsubcats.id },
            };
          });

          const grids = await Promise.all(promisesProdutos);

          return { ...subcategories, grids };
        });
        const produtos = await Promise.all(promisesProdSub);

        const { id, name, status } = categories;

        return {
          id,
          name,
          status,
          subcategories: produtos,
        };
      });
      try {
        const response = await Promise.all(promisesSubCategories);
        return res.json(response);
      } catch ({ message, ...error }) {
        return res.status(404).json({ error: message });
      }
    } catch (error) {
      return res.json({ error: 'Ocorreu algum erro, tente novamente' });
    }
  }
  async findProductByCategories(req, res) {
    try {
      const { fk_categories, limit } = req.params;

      const categoriesExists = await _Categories2.default.findOne({
        where: { id: fk_categories, status: true },
      });
      if (!categoriesExists) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'categories',
            message: 'Categoria não existe, tente novamente !',
          },
        });
      }
      const { page = 1 } = req.query;

      const product = await _Products2.default.findAll({
        limit: limit,
        order: ['created_at'],
        offset: (page - 1) * limit,
        where: { fk_categories, status: true },
        attributes: [
          'id',
          'name',
          'sku',
          'description',
          'brand',
          'model',
          'condition',
          'status',
        ],
        include: [
          {
            model: _Files2.default,
            as: 'file',
            attributes: ['id', 'name', 'path', 'url'],
          },
        ],
      });

      if (!product) {
        return res.json({
          error: {
            type: 'not_exists',
            parameter_name: 'products in categories',
            message: 'Não existe produtos cadastrados nessa categoria !',
          },
        });
      }
      const promisesGrids = product.map(async response => {
        const gridsList = await _Grids2.default.findAll({
          where: { fk_products: response.id, status: true },
          attributes: [
            [_sequelize.fn.call(void 0, 'min', _sequelize.col.call(void 0, 'price')), 'minPrice'],
            [_sequelize.fn.call(void 0, 'max', _sequelize.col.call(void 0, 'price')), 'maxPrice'],
            [_sequelize.fn.call(void 0, 'sum', _sequelize.col.call(void 0, 'amount')), 'amount_stock'],
          ],
        });

        const {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          file,
          details,
        } = response;
        return {
          id,
          name,
          description,
          sku,
          brand,
          model,
          condition,
          status,
          categories,
          thumb: file,
          details,
          infos: gridsList[0],
        };
      });

      const grids = await Promise.all(promisesGrids);

      return res.json(grids);
    } catch ({ message, ...error }) {
      return res.json({ error: message });
    }
  }
}

exports. default = new HomeController();
