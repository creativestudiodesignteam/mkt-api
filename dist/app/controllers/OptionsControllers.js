"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Options = require('../models/Options'); var _Options2 = _interopRequireDefault(_Options);
var _OptionsNames = require('../models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);

var _sequelize = require('sequelize');
/*import Files from '../models/Files'; */

class OptionsController {

  async index(req, res) {

    const userOptions = await _Options2.default.findAll({
      where: {
        status: true,
        fk_users: req.userId,
      },
      attributes: ['id', 'name', 'type', 'status']
    })

    const promisesOptions = userOptions.map(async response => {
      const optionsNames = await _OptionsNames2.default.findAll({
        where: {
          status: true,
          fk_options: response.id,
        },
        attributes: ['id', 'name', 'type', 'status']
      })
      return ({ options: response, optionsNames })

    })
    const options = await Promise.all(promisesOptions);

    return res.json(options);

  }

  async findByProducts(req, res) {
    const { fk_products } = req.params
    const productExist = await _Products2.default.findOne({
      where: { id: fk_products, status: true }
    })
    if (!productExist) {
      return res.json({
        type: 'product_not_exist',
        parameter_name: 'products_id',
        message: 'Seu produto não existe, tente novamente'
      })
    }
    const grids = await _Grids2.default.findAll({
      where: { fk_products, status: true }
    })


    const gridsOptions = grids.map(async response => {
      const optionsGrids = await _OptionsGrids2.default.findAll({
        where: { fk_grids: response.id, status: true },
        attributes: ['id'],
        include: [{
          model: _Options2.default,
          as: 'options',
          attributes: ['id', 'name', 'type', 'status'],
        },
        {
          model: _OptionsNames2.default,
          as: 'options_names',
          attributes: ['id', 'name', 'type', 'status'],
        }
        ]
      })

      //Percorrer optionsGrids
      const optionsGridsPromises = optionsGrids.map(async res => {
        return ({ fk_options: res.options.id, name_options: res.options.name, fk_options_names: res.options_names.id, name_options_names: res.options_names.name })
      })

      const resolvePromise = await Promise.all(optionsGridsPromises)
      return ({ id: response.id, options: resolvePromise })
    })

    const resp = await Promise.all(gridsOptions)

    return res.json(resp)
  }

  async store(req, res) {
    try {
      req.body.fk_users = req.userId;
      const schema = Yup.object().shape({
        name: Yup.string().required(),
      });

      if (!(await schema.isValid(req.body))) {
        return res.json({
          error: {
            "type": "validation_invalid",
            "parameter_name": "yup",
            "message": "Digitou todos os campos certinhos ?, tente novamente"
          }
        });
      }
      const OptionsExistsToName = await _Options2.default.findOne({
        where: { name: req.body.name, fk_users: req.userId }
      })

      if (OptionsExistsToName) {
        throw {
          toError: function () {
            return {
              type: 'already_exists',
              parameter_name: `Options.${req.body.name}`,
              message: `A caracteristica ${req.body.name} já existe`
            };
          }
        }
      }

      const {
        id,
        name
      } = await _Options2.default.create(req.body);


      return res.json({
        id,
        name
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError())
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString()
        })
      }

      return res.json({ error: message })

    }
  }

  async update(req, res) {
    try {
      const { id } = req.params

      const schema = Yup.object().shape({
        name: Yup.string(),
      });
      if (!(await schema.isValid(req.body))) {
        return res.json({
          error: {
            "type": "validation_invalid",
            "parameter_name": "yup",
            "message": "Digitou todos os campos certinhos ?, tente novamente"
          }
        });
      }
      const OptionsExists = await _Options2.default.findOne({
        where: { id, status: true },
      });
      if (!OptionsExists) {
        return res.json({
          error: {
            "type": "not_exists",
            "parameter_name": "option",
            "message": "Opção não existe, tente novamente !"
          }
        })
      }
      const OptionsExistsToName = await _Options2.default.findOne({
        where: {
          id: {
            [_sequelize.Op.ne]: id,
          },
          name: req.body.name, fk_users: req.userId
        }
      })

      if (OptionsExistsToName) {
        throw {
          toError: function () {
            return {
              type: 'already_exists',
              parameter_name: `OptionsNames.${req.body.name}`,
              message: `A caracteristica ${req.body.name} já está cadastrada`
            };
          }
        }
      }
      const {
        name,
      } = await OptionsExists.update(req.body);

      return res.json({
        id,
        name,
      });
    } catch ({ message, ...error }) {
      if (error.toError) {
        return res.json(error.toError())
      }
      if (error.name === 'ValidationError') {
        return res.json({
          type: error.type,
          parameter_name: error.path,
          error: error.errors.toString()
        })
      }

      return res.json({ error: message })

    }
  }

  async delete(req, res) {
    const { id } = req.params
    console.log(id)
    const OptionsExists = await _Options2.default.findOne({
      where: { id, fk_users: req.userId, status: true },
    });
    if (!OptionsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "option",
          "message": "Caracteristica não existe, tente novamente !"
        }
      });
    }

    const OptionsExistsinOPGrids = await _OptionsGrids2.default.findAll({
      where: { fk_options: id, fk_users: req.userId },
    });

    if (OptionsExistsinOPGrids.length > 0) {
      return res.json({
        error: {
          "type": "is_used",
          "parameter_name": "is_used",
          "message": `Sua caracteristica esta sendo usada em ${OptionsExistsinOPGrids.length} produto !`
        }
      });
    }


    const optionsNamesExists = await _OptionsNames2.default.findAll({
      where: { fk_options: id, fk_users: req.userId, status: true },
    });
    console.log(optionsNamesExists)
    if (optionsNamesExists.length > 0) {
      return res.json({
        error: {
          "type": "is_used_in_options_names",
          "parameter_name": "is_used_options_names",
          "message": `Sua caracteristica esta sendo usada em ${OptionsExistsinOPGrids.length} atributos !`
        }
      });
    }

    await OptionsExists.destroy();

    return res.json({ msg: 'Caracteristica deletada' })

  }
}

exports. default = new OptionsController();
