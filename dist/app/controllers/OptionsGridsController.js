"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _OptionsGrids = require('../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Options = require('../models/Options'); var _Options2 = _interopRequireDefault(_Options);
var _OptionsNames = require('../models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _Grids = require('../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Products = require('../models/Products'); var _Products2 = _interopRequireDefault(_Products);



/*import Files from '../models/Files'; */

class OptionsGridsController {

    async index(req, res) {
        const { fk_grids } = req.params;
        /* const grids = await Grids.findByPk(fk_grids); */
        const grids = await _Grids2.default.findOne({

            attributes: ['id', 'status'],
            where: {
                id: fk_grids,
                status: true,
            },

            include: [{
                        model: _Products2.default,
                        as: 'products',
                        attributes: ['id', 'name', 'description', 'brand', 'model'],
                        where: { status: true },
                    },

                ]
                /* attributes: ['id', 'name', 'status',] */
        })

        const optionsGrids = await _OptionsGrids2.default.findAll({
            attributes: ['id', 'status'],
            where: {
                fk_grids,
                status: true,
            },

            include: [{
                        model: _Options2.default,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _OptionsNames2.default,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _Grids2.default,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]
                /* attributes: ['id', 'name', 'status',] */
        })

        if (!optionsGrids) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "grid",
                    "message": "Relação dos produtos em grades não existe, tente novamente !"
                }
            });
        }

        return res.json([{ grids, optionsGrids }]);

    }

    async store(req, res) {
        req.body.fk_users = req.userId;

        const schema = Yup.object().shape({
            fk_option: Yup.number(),
            fk_options_names: Yup.number(),
            fk_grids: Yup.number(),
        });


        if (!(await schema.isValid(req.body))) {
            return res.json({
                error: {
                    "type": "validation_invalid",
                    "parameter_name": "yup",
                    "message": "Digitou todos os campos certinhos ?, tente novamente"
                }
            });
        }
        const GridsExists = await _Grids2.default.findByPk(req.body.fk_grids);
        if (!GridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "grid",
                    "message": "Relação dos produtos em grades não existe, tente novamente !"
                }
            });
        }

        const OptionsExists = await _Options2.default.findByPk(req.body.fk_options);
        if (!OptionsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }

            });
        }

        const {
            id,
            fk_options,
            fk_options_names,
            fk_grids,
            fk_users
        } = await _OptionsGrids2.default.create(req.body);


        return res.json(
            await _OptionsGrids2.default.findOne({
                where: { id, fk_users },
                attributes: ['id', 'status'],
                include: [{
                        model: _Options2.default,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _OptionsNames2.default,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _Grids2.default,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]

            })
        );
    }

    async update(req, res) {
        const { id } = req.params

        const schema = Yup.object().shape({
            name: Yup.string(),
        });
        if (!(await schema.isValid(req.body))) {
            return res.json({
                error: {
                    "type": "validation_invalid",
                    "parameter_name": "yup",
                    "message": "Digitou todos os campos certinhos ?, tente novamente"
                }
            });
        }
        const OptionsGridsExists = await _OptionsGrids2.default.findOne({
            where: { id, status: true },
        });
        if (!OptionsGridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }

            })
        }
        await OptionsGridsExists.update(req.body);

        return res.json(
            await _OptionsGrids2.default.findOne({
                where: { id, fk_users },
                attributes: ['id', 'status'],
                include: [{
                        model: _Options2.default,
                        as: 'options',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _OptionsNames2.default,
                        as: 'options_names',
                        attributes: ['id', 'name', 'type'],
                        where: { status: true },
                    },
                    {
                        model: _Grids2.default,
                        as: 'grids',
                        attributes: ['id', 'amount', 'price'],
                        where: { status: true },
                    },
                ]

            }));
    }

    async delete(req, res) {
        const { id } = req.params
        const OptionsGridsExists = await _OptionsGrids2.default.findOne({
            where: { id, fk_users: req.userId, status: true },
        });
        if (!OptionsGridsExists) {
            return res.json({
                error: {
                    "type": "not_exists",
                    "parameter_name": "option",
                    "message": "Opção não existe, tente novamente !"
                }
            });
        }

        await OptionsGridsExists.update({ status: false });

        return res.json({ msg: 'Opção deletada' })

    }
}

exports. default = new OptionsGridsController();
