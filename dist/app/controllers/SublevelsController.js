"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Sublevels = require('../models/Sublevels'); var _Sublevels2 = _interopRequireDefault(_Sublevels);
/*import Files from '../models/Files'; */

class SublevelsController {
  async index(req, res) {
    const { fk_subcategories } = req.params
    const sublevels = await _Sublevels2.default.findAll({
      where: { status: true, fk_subcategories },
      attributes: ['id', 'name'],
    })

    return res.json(sublevels);
  }
  async store(req, res) {
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      fk_subcategories: Yup.number().integer().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }

    const {
      id,
      name,
    } = await _Sublevels2.default.create(req.body);


    return res.json({
      id,
      name,
    });
  }

  async update(req, res) {
    const { id } = req.params

    const schema = Yup.object().shape({
      name: Yup.string(),
    });
    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          "type": "validation_invalid",
          "parameter_name": "yup",
          "message": "Digitou todos os campos certinhos ?, tente novamente"
        }
      });
    }
    const SublevelsExists = await _Sublevels2.default.findOne({
      where: { id, status: true },
    });
    if (!SublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevels",
          "message": "Sub categoria não existe."
        }
      })
    }
    const {
      name,
    } = await SublevelsExists.update(req.body);

    return res.json({
      id,
      name,
    });
  }

  async delete(req, res) {
    const { id } = req.params
    const SublevelsExists = await _Sublevels2.default.findOne({
      where: { id, status: true },
    });
    if (!SublevelsExists) {
      return res.json({
        error: {
          "type": "not_exists",
          "parameter_name": "sublevels",
          "message": "Sub nivel não existe."
        }
      });
    }

    await SublevelsExists.destroy();

    return res.json({ msg: 'Sub nivel deletado' })

  }
}

exports. default = new SublevelsController();
