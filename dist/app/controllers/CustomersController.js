"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { newObj[key] = obj[key]; } } } newObj.default = obj; return newObj; } } function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _yup = require('yup'); var Yup = _interopRequireWildcard(_yup);
var _Customers = require('../models/Customers'); var _Customers2 = _interopRequireDefault(_Customers);
var _Users = require('../models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Files = require('../models/Files'); var _Files2 = _interopRequireDefault(_Files);

class CustomersController {
  async index(req, res) {
    const { type } = await _Users2.default.findByPk(req.userId);

    if (type) {
      return res.json({
        error: {
          'type': 'not_authorized',
          'parameter_name': 'type_customers',
          'message': 'Você não tem autorização para executar está função !',
        },
      });
    }
    const customers = await _Customers2.default.findOne({
      where: { fk_users: req.userId, status: true },
      attributes: ['id', 'name', 'cpf', 'telephone', 'gender', 'birthday'],
      include: [
        {
          model: _Users2.default,
          as: 'users',
          attributes: ['id', 'email'],
          include: [
            {
              model: _Files2.default,
              as: 'avatar',
              attributes: ['id', 'path', 'url'],
            },
          ],
        },
      ],
    });

    return res.json(customers);
  }

  async store(req, res) {
    /* telephone gender birthday */
    const schema = Yup.object().shape({
      name: Yup.string().required(),
      cpf: Yup.string().required(),
      telephone: Yup.string().required(),
      gender: Yup.string().required(),
      birthday: Yup.date().required(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const {
      id,
      name,
      cpf,
      telephone,
      gender,
      birthday,
    } = await _Customers2.default.create(req.body);

    return res.json({
      id,
      name,
      cpf,
      telephone,
      gender,
      birthday,
    });
  }

  async update(req, res) {
    const { id } = await _Customers2.default.findOne({ where: { fk_users: req.userId } });

    const schema = Yup.object().shape({
      name: Yup.string(),
      cpf: Yup.string(),
      telephone: Yup.string(),
      gender: Yup.string(),
      birthday: Yup.date(),
    });

    if (!(await schema.isValid(req.body))) {
      return res.json({
        error: {
          'type': 'validation_invalid',
          'parameter_name': 'yup',
          'message': 'Digitou todos os campos certinhos ?, tente novamente',
        },
      });
    }
    const CustomersExists = await _Customers2.default.findOne({
      where: { id, status: true },
    });
    if (!CustomersExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'customers',
          'message': 'Customer não existe, tente novamente !',
        },
      });
    }

    const {
      name,
      cpf,
      telephone,
      gender,
      birthday,
    } = await CustomersExists.update(req.body);

    return res.json({
      id,
      name,
      cpf,
      telephone,
      gender,
      birthday,
    });
  }

  async delete(req, res) {

    const CustomersExists = await _Customers2.default.findOne({
      where: { fk_users: req.userId, status: true },
    });
    console.log(CustomersExists);
    if (!CustomersExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'customers',
          'message': 'Customer não existe, tente novamente !',
        },
      });
    }
    const { fk_users } = CustomersExists;
    const UsersExists = await _Users2.default.findOne({
      where: { id: fk_users, status: true },
    });
    if (!UsersExists) {
      return res.json({
        error: {
          'type': 'not_exists',
          'parameter_name': 'user',
          'message': 'Usuário não existe, tente novamente !',
        },
      });
    }

    await CustomersExists.update({ status: false });
    await UsersExists.update({ status: false });


    return res.json({ msg: 'Usuário está desabilitado' });

  }
}

exports. default = new CustomersController();
