"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Products = require('../../models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Files = require('../../models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Galleries = require('../../models/Galleries'); var _Galleries2 = _interopRequireDefault(_Galleries);
var _Grids = require('../../models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _OptionsGrids = require('../../models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Options = require('../../models/Options'); var _Options2 = _interopRequireDefault(_Options);
var _OptionsNames = require('../../models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _Categories = require('../../models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Details = require('../../models/Details'); var _Details2 = _interopRequireDefault(_Details);
var _Datasheets = require('../../models/Datasheets'); var _Datasheets2 = _interopRequireDefault(_Datasheets);

class ProductHelper {
  async galleryFunction(data) {
    const galleryAll = await _Galleries2.default.findAll({
      where: { fk_grids: data.fk_grids },
    });
    if (data.gallery.length === 0) {
      await _Galleries2.default.destroy({
        where: { fk_grids: data.fk_grids },
      });
    } else {
      const gridsGallery = data.gallery.map(async response => {
        const fileExist = await _Files2.default.findOne({
          where: { id: response.fk_files },
        });
        if (fileExist) {
          const galleriesCreate = await _Galleries2.default.create({
            fk_files: fileExist.id,
            fk_grids: data.fk_grids,
          });

          const destroyGallery = galleryAll.filter(
            g => g !== galleriesCreate.id
          );
          if (galleriesCreate.id !== parseInt(destroyGallery)) {
            const promiseDestroy = destroyGallery.map(async response => {
              await _Galleries2.default.destroy({
                where: { id: parseInt(response.id) },
              });
            });

            await Promise.all(promiseDestroy);
          }
        }
      });

      await Promise.all(gridsGallery);
    }
  }

  async OptionGrids(data) {
    var errors = [];
    if (data.fk_grids) {
      const OptionsGridsAll = await _OptionsGrids2.default.findAll({
        where: { fk_grids: data.fk_grids, status: true },
      });

      if (data.options_grids.length === 0) {
        await _OptionsGrids2.default.destroy({
          where: { fk_grids: data.fk_grids },
        });
      } else {
        const optionsGrids = data.options_grids.map(async response => {
          if (!response.id) {
            if (response.fk_options) {
              const optionsExists = await _Options2.default.findOne({
                where: { id: response.fk_options, status: true },
              });
              if (!optionsExists) {
                errors.push({
                  type: 'error_exists',
                  parameter_name: 'options',
                  message: 'Erro ao atualizar, o atributo do seu produto ',
                });
              }
            }
            if (response.fk_options_names) {
              const optionsNamesExists = await _OptionsNames2.default.findOne({
                where: {
                  id: response.fk_options_names,
                  fk_options: response.fk_options,
                  status: true,
                },
              });
              if (!optionsNamesExists) {
                errors.push({
                  type: 'error_exists',
                  parameter_name: 'options_name',
                  message: 'Erro ao atualizar, o atributo do seu produto ',
                });
              }
            }
            if (errors.length === 0) {
              const optionsGrids = await _OptionsGrids2.default.create({
                fk_options: response.fk_options,
                fk_options_names: response.fk_options_names,
                fk_grids: data.fk_grids,
                fk_users: data.fk_users,
              });

              const destroyOptionsGrids = OptionsGridsAll.filter(
                g => g !== optionsGrids.id
              );

              if (optionsGrids.id !== parseInt(destroyOptionsGrids)) {
                const promiseDestroy = OptionsGridsAll.map(async response => {
                  await _OptionsGrids2.default.destroy({
                    where: { id: parseInt(response.id) },
                  });
                });

                await Promise.all(promiseDestroy);
              }
            }
          }
        });
        await Promise.all(optionsGrids);
      }
    }
    if (errors.length >= 1) {
      return errors;
    }
  }
  async List(data) {
    console.log(data.id);
    const products = await _Products2.default.findOne({
      where: { id: data.id, status: true },
      attributes: [
        'id',
        'name',
        'description',
        'sku',
        'brand',
        'model',
        'condition',
        'status',
      ],
      include: [
        {
          model: _Categories2.default,
          as: 'categories',
          attributes: ['id', 'name', 'status'],
        },
        {
          model: _Files2.default,
          as: 'file',
          attributes: ['id', 'name', 'path', 'url', 'status'],
        },
        {
          model: _Details2.default,
          as: 'details',
          attributes: [
            'id',
            'length',
            'height',
            'width',
            'weight_unit',
            'weight',
            'situation',
            'type_packing',
            'delivery_time',
          ],
        },
      ],
    });

    const {
      id: idProd,
      name: nameProd,
      sku: skuProd,
      categories: categoriesProd,
      description: descriptionProd,
      brand: brandProd,
      model: modelProd,
      condition: conditionProd,
      status: statusProd,
    } = products;

    const dataSheet = await _Datasheets2.default.findAll({
      where: { fk_products: data.id, status: true },
      attributes: ['id', 'name', 'description', 'status'],
    });

    const grids = await _Grids2.default.findAll({
      where: { fk_products: data.id, status: true },
      attributes: ['id', 'amount', 'price', 'status'],
    });

    const promisesGrids = grids.map(async response => {
      const optionsGrids = await _OptionsGrids2.default.findAll({
        where: { fk_grids: response.id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: _Options2.default,
            as: 'options',
            attributes: ['id', 'name', 'type', 'status'],
          },
          {
            model: _OptionsNames2.default,
            as: 'options_names',
            attributes: ['id', 'name', 'type', 'status'],
          },
        ],
      });

      const gallery = await _Galleries2.default.findAll({
        where: { fk_grids: response.id, status: true },
        attributes: ['id', 'status'],
        include: [
          {
            model: _Files2.default,
            as: 'files',
            attributes: ['id', 'name', 'path', 'url', 'status'],
          },
        ],
      });

      return { Grids: response, optionsGrids, gallery };
    });

    const GridsList = await Promise.all(promisesGrids);

    return {
      product: {
        id: idProd,
        name: nameProd,
        description: descriptionProd,
        sku: skuProd,
        brand: brandProd,
        model: modelProd,
        condition: conditionProd,
        status: statusProd,
        categories: categoriesProd,
        datasheets: dataSheet,
        GridsList,
      },
    };
  }
}

exports. default = new ProductHelper();
