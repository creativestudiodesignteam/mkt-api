"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _Mail = require('../../lib/Mail'); var _Mail2 = _interopRequireDefault(_Mail);

exports. default = {
  key: 'ForgotPasswordMail',
  async handle({ data }) {
    console.log(data)
    const { users, config } = data;
    await _Mail2.default.sendMail({
      to: `${users.email}`,
      subject: 'Portal Da Maria | Recuperar Senha',
      template: 'forgotpassword',
      context: {
        users: users.name,
        token: config.token,
      },
    });
  },
};
