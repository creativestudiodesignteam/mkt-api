"use strict";const safe2pay = require('safe2pay');
safe2pay.enviroment.setApiKey('2FF1CB94A216419A9F21FE5643653F8E');

async function getSubConta(id) {
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  await MarketplaceRequest.Get(id)
    .then(function (result) {
      retorno = result;
    }).catch(function (error) {
      retorno = error;
    });

  return retorno;
}

async function deleteSubConta(id) {
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  await MarketplaceRequest.Delete(id)
    .then(function (result) {
      retorno = result;
    }).catch(function (error) {
      retorno = error;
    });

  return retorno;
}

async function createSubConta(merchant) {
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  await MarketplaceRequest.Add(merchant)
    .then(function(result) {
      retorno = result;
    }).catch(function(error) {
      retorno = error;
    });

  return retorno;
}

async function updateSubConta(merchant, id) {
  let MarketplaceRequest = safe2pay.api.MarketplaceRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  await MarketplaceRequest.Update(merchant, id)
    .then(function(result) {
      retorno = result;
    }).catch(function(error) {
      retorno = error;
    });

  return retorno;
}

// exports.setSubConta = setSubConta;
exports.getSubConta = getSubConta;
exports.deleteSubConta = deleteSubConta;
exports.createSubConta = createSubConta;
exports.updateSubConta = updateSubConta;
