"use strict"; function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }require('dotenv');
var _CreditCards = require('../../models/CreditCards'); var _CreditCards2 = _interopRequireDefault(_CreditCards);

const SAFE2PAY_KEY = '2FF1CB94A216419A9F21FE5643653F8E';

const safe2pay = require('safe2pay');

let application = "Portal da Maria";
let vendor = "Portal da Maria";

/**
 * Retorna os dados da transaçao pelo ID
 * @param id
 * @returns {Promise<string>}
 */
async function getTransactionById (id) {
  let TransactionRequest = safe2pay.api.TransactionRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  await TransactionRequest.Get(id)
    .then(function (result) {
      retorno = result;
    }).catch(function(error) {
    retorno = error;
  });

  return retorno;
}

/**
 * Retorna os dados da transaçao pelo codigo de referencia
 * @param reference
 * @returns {Promise<string>}
 */
async function getTransactionByReference (reference) {
  let TransactionRequest = safe2pay.api.TransactionRequest;

  // TODO - resolver esse retorno
  let retorno = "";
  TransactionRequest.GetByReference(reference)
    .then(function (result) {
      retorno = result
    }).catch(function(error) {
    retorno = error;
  });

  return retorno;
}

/**
 * realiza o pagamento via boleto, cartão de crédito e cartão de débito
 * @param req
 * @param addressModel
 * @param userModel
 * @param products
 * @param cardModel
 * @param payment_type
 * @param safe2payData
 * @param freteModel
 * @param personalData
 * @return {Promise<string>}
 */
async function setTransaction (req, addressModel, userModel, products, cardModel, payment_type, safe2payData, freteModel, personalData) {
  let isSandbox = safe2payData.SAFE2PAY_IS_SANDBOX;
  safe2pay.enviroment.setApiKey(safe2payData.SAFE2PAY_KEY_SANDBOX);
  let callbackUrl = safe2payData.SAFE2PAY_CALLBACK_URL;

  const PaymentRequest = safe2pay.api.PaymentRequest;

  let Transaction = safe2pay.model.transaction.Transaction;
  let Customer = safe2pay.model.general.Customer;
  let Product = safe2pay.model.general.Product
  let Address = safe2pay.model.general.Address;

  let payload = new Transaction();
  payload.IsSandbox = isSandbox;
  payload.Application = application;
  payload.Vendor = vendor;
  payload.CallbackUrl = callbackUrl;

  /**
   * Código da forma de pagamento
   * 1 - Boleto bancário
   * 2 - Cartão de crédito
   * 3 - Criptomoeda
   * 4 - Cartão de débito
   * 10 - Débito em conta
   */
  payload.PaymentMethod = payment_type.toString();

  //BOLETO
  if (payment_type === 1) {
    let BankSlip = safe2pay.model.payment.Bankslip;
    let bankslip = new BankSlip();
    bankslip.DueDate = generateFutureDate();
    bankslip.CancelAfterDue = false;
    bankslip.IsEnablePartialPayment = false;
    bankslip.PenaltyRate = 2.00;
    bankslip.InterestRate = 0.40;
    bankslip.Instruction = "Pagável em qualquer banco até o vencimento";
    bankslip.Message = [
      "Sr. Caixa, não receber após o vencimento"
    ];

    payload.PaymentObject = bankslip;
  }

  //CREDITO
  if(payment_type === 2) {
    let CreditCard = safe2pay.model.payment.CreditCard;
    let creditCard = new CreditCard();
    creditCard.InstallmentQuantity = parseInt(req.body.qty_parcelas);
    creditCard.Token = cardModel.token;
    creditCard.IsRecurrence = false;

    payload.PaymentObject = creditCard;
  }

  //DEBITO
  if(payment_type === 4) {
    let DebitCard = safe2pay.model.payment.DebitCard;
    let debitCard = DebitCard();
    debitCard.Token = cardModel.token;

    payload.PaymentObject = debitCard;
  }

  products.forEach(function(item) {
    payload.Products.push(new Product(item.id, item.nome, item.valor, item.qty));
  });

  //FRETE
  payload.Products.push(new Product(freteModel.id, 'frete', freteModel.valor, 1));

  let address = new Address();
  address.ZipCode = addressModel.postcode;
  address.Street = addressModel.street;
  address.Complement = addressModel.complement;
  address.Number = addressModel.number;
  address.District = addressModel.neighborhood;
  address.StateInitials = addressModel.state;
  address.CityName = addressModel.city;
  address.CountryName = "Brasil";

  let customer = new Customer();
  console.log(personalData)
  customer.Name = personalData.name;
  customer.Identity = personalData.documento;
  customer.Email = userModel.email;
  customer.Phone = personalData.telephone;
  customer.Address = address;

  payload.Customer = customer;

  // TODO - resolver esse retorno
  let retorno = "";
  await PaymentRequest.Payment(payload)
    .then(function (result) {
      retorno = {
        'response': result
      };
    }, function (error) {
      retorno = error;
    });

  return retorno;
}

/**
 * cancela a transaçao de todos os tipos: credito, debito e boleto
 * @param saleId
 * @param payment_type
 * @returns {Promise<string>}
 */
async function cancelTransaction(saleId, payment_type) {
  const PaymentRequest = safe2pay.api.PaymentRequest;
  payment_type = parseInt(payment_type);

  if (payment_type === 1) {
    var type = PaymentRequest.CancelType.BANKSLIP;
  } else if (payment_type === 4) {
    var type = PaymentRequest.CancelType.DEBIT;
  } else {
    var type = PaymentRequest.CancelType.CREDIT;
  }

  // TODO - resolver esse retorno
  let retorno = "";
  await PaymentRequest.Refund(saleId, type)
    .then(function(result) {
      retorno =  result;
    }, function(error) {
      retorno = error;
    });

  return retorno;
}

/**
 * Gera token do cartao de credito e debito
 * @param holder
 * @param cardNumber
 * @param expirationDate
 * @param securityCode
 * @param type
 * @param first4Digits
 * @param userId
 * @returns {Promise<void>}
 */
async function generateToken(holder, cardNumber, expirationDate, securityCode, type, first4Digits, userId) {

  const TokenizationRequest = safe2pay.api.TokenizationRequest;
  if (parseInt(type) === 2) {

    //credit card
    let CreditCard = safe2pay.model.payment.CreditCard;
    let creditCard = new CreditCard();
    creditCard.Holder = holder;
    creditCard.CardNumber = cardNumber;
    creditCard.ExpirationDate = expirationDate;
    creditCard.SecurityCode = securityCode;

    var card = creditCard;

  } else {

    //debit card
    let DebitCard = safe2pay.model.payment.DebitCard;
    let debitCard = new DebitCard();
    debitCard.Holder = holder;
    debitCard.CardNumber = cardNumber;
    debitCard.ExpirationDate = expirationDate;
    debitCard.SecurityCode = securityCode;

    var card = debitCard;
  }

  var retorno = "";
  await TokenizationRequest.Create(card)
    .then(function (result) {
      retorno = result;
    }, function (error) {
      retorno = error;
    });

  if(retorno.HasError === true) {
    return {
      'token_created': false
    }
  } else {

    let token = retorno.ResponseDetail.Token;
    // salva o novo token no banco
    let card =  await _CreditCards2.default.create(
      {
        token: token,
        status: true,
        first_digits: first4Digits,
        fk_users: userId,
        holder_name: holder,
        type: type
      }
    );

    return {
      'token_created': true,
      'card': card
    }
  }
}

/**
 * Retorna uma data no futuro baseado
 * @param date
 * @param days
 * @returns {Date}
 */
function generateFutureDate(date = new Date(), days = 3) {
  const dateFormat = require('dateformat');

  let result = new Date(date);
  result.setDate(date.getDate() + days);
  dateFormat(result, "dd/mm/yyyy");

  return result;
}

exports.setTransaction = setTransaction;
exports.cancelTransaction = cancelTransaction;
exports.getTransactionById = getTransactionById;
exports.getTransactionByReference = getTransactionByReference;
exports.generateToken = generateToken;
