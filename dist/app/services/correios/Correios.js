"use strict";require('dotenv');

const Correios = require('node-correios');
const correiosObj = new Correios();
const calcPrecoUrl = 'http://localhost:3336/files/CalcPrecoPrazo.xml';
const empresa = {
  codEmpresa: process.env.CORREIOS_COD_EMPRESA,
  senhaEmpresa: process.env.CORREIOS_SENHA,
};

async function buscaCEP(cep) {
  let response;
  await correiosObj
    .consultaCEP(cep)
    .then(result => {
      response = result;
    })
    .catch(error => {
      response = error;
    });
  return response;
}

async function calcPrecoPrazoData(
  cod_servico,
  origem,
  destino,
  peso,
  comprimento,
  altura,
  largura,
  diametro,
  valor
) {
  let args = {
    nCdEmpresa: empresa.codEmpresa,
    sDsSenha: empresa.senhaEmpresa,
    nCdServico: cod_servico,
    sCepOrigem: origem,
    sCepDestino: destino,
    nVlPeso: peso,
    nCdFormato: 1,
    nVlComprimento: comprimento,
    nVlAltura: altura,
    nVlLargura: largura,
    nVlDiametro: diametro,
    sCdMaoPropria: 'N',
    nVlValorDeclarado: valor,
    sCdAvisoRecebimento: 'N',
    sDtCalculo: '',
  };
  let response;
  await correiosObj
    .calcPrecoPrazo(args)
    .then(result => {
      response = result;
    })
    .catch(error => {
      response = error;
    });
  return response;
}

exports.calcPrecoPrazoData = calcPrecoPrazoData;
exports.buscaCEP = buscaCEP;
