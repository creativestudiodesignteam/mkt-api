"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Payments extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        code: _sequelize2.default.STRING,
        method: _sequelize2.default.STRING,
        url: _sequelize2.default.STRING,
        references: _sequelize2.default.STRING,
        situations: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
        created_at: _sequelize2.default.DATE,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

exports. default = Payments;
