"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Levelsubcats extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Sublevels, {
      foreignKey: 'fk_sublevels',
      as: 'sublevels',
    });
    this.belongsTo(models.Prodsubcats, {
      foreignKey: 'fk_prodsubcat',
      as: 'prodsubcat',
    });
  }
}


exports. default = Levelsubcats;
