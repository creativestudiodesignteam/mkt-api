"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class AccountBank extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        bank_code: _sequelize2.default.STRING,
        account_type: _sequelize2.default.STRING,
        bank_agency: _sequelize2.default.STRING,
        bank_agency_digit: _sequelize2.default.STRING,
        bank_account: _sequelize2.default.STRING,
        bank_account_digit: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_providers',
      as: 'providers',
    });
  }

}

exports. default = AccountBank;
