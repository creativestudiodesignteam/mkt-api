"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Carts extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        amount: _sequelize2.default.INTEGER,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });

    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'provider',
    });

    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });
  }
}

exports. default = Carts;
