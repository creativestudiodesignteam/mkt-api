"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Orders extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        /* fk_grids fk_users fk_addresses */
        amount: _sequelize2.default.INTEGER,
        price: _sequelize2.default.DOUBLE,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
/*     this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    }); */
    this.belongsTo(models.Addresses, {
      foreignKey: 'fk_addresses',
      as: 'addresses',
    });

  }
}

exports. default = Orders;
