"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class CreditCards extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        token: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
        first_digits: _sequelize2.default.STRING,
        holder_name: _sequelize2.default.STRING,
        selected: _sequelize2.default.BOOLEAN,
        type: _sequelize2.default.INTEGER
      },
      {
        sequelize,
      }
    );
    return this;
  }

  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}


exports. default = CreditCards;
