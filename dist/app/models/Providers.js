"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);
var _Responsibles = require('./Responsibles'); var _Responsibles2 = _interopRequireDefault(_Responsibles);

class Providers extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        /* cnpj fantasy_name reason_social state_register county_register telephone site */
        cnpj: _sequelize2.default.STRING,
        fantasy_name: _sequelize2.default.STRING,
        reason_social: _sequelize2.default.STRING,
        state_register: _sequelize2.default.STRING,
        county_register: _sequelize2.default.STRING,
        telephone_commercial: _sequelize2.default.STRING,
        telephone_whatsapp: _sequelize2.default.STRING,
        is_panel_restricted: _sequelize2.default.BOOLEAN,
        token: _sequelize2.default.STRING,
        idcode: _sequelize2.default.INTEGER,
        status: _sequelize2.default.BOOLEAN,
        data_fundacao: _sequelize2.default.STRING,
        qty_funcionarios: _sequelize2.default.INTEGER,
        site: _sequelize2.default.STRING,
        modalidade: _sequelize2.default.STRING
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users'
    });

    this.hasMany(models.ProviderRamos, {
      foreignKey: 'fk_provider',
      as: 'provider_ramos'
    });

    this.hasMany(models.Responsibles, {
      foreignKey: 'fk_provider',
      as: 'responsibles'
    });
  }
}

exports. default = Providers;
