"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Details extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        length: _sequelize2.default.STRING,
        height: _sequelize2.default.STRING,
        width: _sequelize2.default.STRING,
        weight_unit: _sequelize2.default.STRING,
        weight: _sequelize2.default.STRING,
        situation: _sequelize2.default.STRING,
        type_packing: _sequelize2.default.STRING,
        delivery_time: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
}

exports. default = Details;
