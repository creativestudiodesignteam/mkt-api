"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Responsibles extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        name: _sequelize2.default.STRING,
        cpf: _sequelize2.default.STRING,
        email: _sequelize2.default.STRING,
        telephone: _sequelize2.default.STRING,
        cargo: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'providers',
    });
  }
}

exports. default = Responsibles;
