"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Taxes extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        tax_type_name: _sequelize2.default.STRING,
        tax: _sequelize2.default.FLOAT,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.MerchantSplit, {
      foreignKey: 'fk_merchant_split',
      as: 'merchant_split',
    });
  }

}

exports. default = Taxes;
