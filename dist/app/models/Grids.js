"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Grids extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        /* length height width weight_unit weight situation */
        amount: _sequelize2.default.STRING,
        price: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'user',
    });
    this.belongsTo(models.Products, {
      foreignKey: 'fk_products',
      as: 'products',
    });
  }
}

exports. default = Grids;
