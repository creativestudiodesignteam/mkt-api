"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Addresses extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        /* name postcode state city neighborhood street number complement */
        name: _sequelize2.default.STRING,
        postcode: _sequelize2.default.STRING,
        state: _sequelize2.default.STRING,
        city: _sequelize2.default.STRING,
        neighborhood: _sequelize2.default.STRING,
        street: _sequelize2.default.STRING,
        number: _sequelize2.default.STRING,
        complement: _sequelize2.default.STRING,
        references: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
        select_at: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}

exports. default = Addresses;
