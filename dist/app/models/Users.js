"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);
var _bcryptjs = require('bcryptjs'); var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

class User extends _sequelize.Model {
	static init(sequelize) {
		super.init(
			{
				email: _sequelize2.default.STRING,
				password: _sequelize2.default.VIRTUAL,
				password_hash: _sequelize2.default.STRING,
				reset_password: _sequelize2.default.STRING,
				date_reset_password: _sequelize2.default.STRING,
				status: _sequelize2.default.BOOLEAN,
				type: _sequelize2.default.BOOLEAN,
				token_email: _sequelize2.default.STRING,
				activation_at: _sequelize2.default.DATE,
				attempts: _sequelize2.default.INTEGER,
				timeout: _sequelize2.default.DATE
			},
			{
				sequelize
			}
		);
		this.addHook('beforeSave', async (user) => {
			if (user.password) {
				user.password_hash = await _bcryptjs2.default.hash(user.password, 8);
			}
		});
		return this;
	}

	checkPassword(password) {
		return _bcryptjs2.default.compare(password, this.password_hash);
	}
	static associate(models) {
		this.belongsTo(models.File, {
			foreignKey: 'fk_avatar',
			as: 'avatar'
		});
		this.hasOne(models.Addresses, {
			foreignKey: 'fk_users',
			as: 'addresses'
		});
	}
}

exports. default = User;
