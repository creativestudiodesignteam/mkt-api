"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Sales extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        hash: _sequelize2.default.STRING,
        amount: _sequelize2.default.INTEGER,
        quota: _sequelize2.default.INTEGER,
        price: _sequelize2.default.INTEGER,
        payment_type: _sequelize2.default.INTEGER,
        status: _sequelize2.default.BOOLEAN,
        created_at: _sequelize2.default.DATE,
        canceled_at: _sequelize2.default.DATE,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Grids, {
      foreignKey: 'fk_grids',
      as: 'grids',
    });

    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_provider',
      as: 'providers',
    });
    this.belongsTo(models.Payments, {
      foreignKey: 'fk_payments',
      as: 'payments',
    });
    this.belongsTo(models.Fretes, {
      foreignKey: 'fk_fretes',
      as: 'fretes',
    });
  }

}

exports. default = Sales;
