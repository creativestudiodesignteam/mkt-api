"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class Products extends _sequelize.Model {
  static init(sequelize) {
    super.init(
      {
        /* name description */
        name: _sequelize2.default.STRING,
        description: _sequelize2.default.STRING,
        brand: _sequelize2.default.STRING,
        model: _sequelize2.default.STRING,
        sku: _sequelize2.default.STRING,
        condition: _sequelize2.default.STRING,
        status: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'fk_thumb',
      as: 'file',
    });
    this.belongsTo(models.Details, {
      foreignKey: 'fk_details',
      as: 'details',
    });
    this.belongsTo(models.Categories, {
      foreignKey: 'fk_categories',
      as: 'categories',
    });
    /*     this.belongsTo(models.Subcategories, {
          foreignKey: 'fk_subcategories',
          as: 'subcategories',
        }); */
    this.belongsTo(models.User, {
      foreignKey: 'fk_users',
      as: 'users',
    });
  }
}

exports. default = Products;
