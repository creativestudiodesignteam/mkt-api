"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);

class MerchantSplit extends _sequelize.Model {
  static init(sequelize) {

    super.init(
      {
        payment_method_code: _sequelize2.default.INTEGER,
        is_sub_account_tax_payer: _sequelize2.default.BOOLEAN,
      },
      {
        sequelize,
      }
    );
    return this;
  }
  static associate(models) {
    this.belongsTo(models.Providers, {
      foreignKey: 'fk_providers',
      as: 'providers',
    });
  }

}

exports. default = MerchantSplit;
