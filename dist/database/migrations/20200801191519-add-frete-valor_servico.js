"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('fretes', 'prazo');

    return queryInterface.addColumn('fretes', 'prazo_entrega', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('fretes', 'prazo_entrega');
  },
};
