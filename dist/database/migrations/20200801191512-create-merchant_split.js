"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('merchant_split', {
      /* name description */
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      payment_method_code: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      is_sub_account_tax_payer: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      fk_providers: {
        type: Sequelize.INTEGER,
        references: { model: 'providers', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('merchant_split');
  },
};
