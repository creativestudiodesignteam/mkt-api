"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    //remove o campo string
    queryInterface.removeColumn('products', 'description');

    //adiciona o campo text
    return queryInterface.addColumn('products', 'description', {
      type: Sequelize.TEXT,
      allowNull: false
    });
  },

  down: queryInterface => {
    //remove o campo text
    queryInterface.removeColumn('products', 'description');

    //adiciona o campo string
    return queryInterface.addColumn('products', 'description', {
      type: Sequelize.STRING,
      allowNull: false
    });
  },
};
