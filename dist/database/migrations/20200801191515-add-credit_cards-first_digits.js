"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('credit_cards', 'last_digits');

    return queryInterface.addColumn('credit_cards', 'first_digits', {
      type: Sequelize.STRING,
      allowNull: false,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('credit_cards', 'first_digits');
  },
};
