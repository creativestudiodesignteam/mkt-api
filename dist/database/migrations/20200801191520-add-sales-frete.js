"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('sales', 'fk_fretes', {
      type: Sequelize.INTEGER,
      references: { model: 'fretes', key: 'id' },
      onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
      onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
      allowNull: true,
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('sales', 'fk_fretes');
  },
};
