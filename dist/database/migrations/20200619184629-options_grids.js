"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('options_grids', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      fk_options: {
        type: Sequelize.INTEGER,
        references: { model: 'options', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },
      fk_options_names: {
        type: Sequelize.INTEGER,
        references: { model: 'options_names', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },
      fk_users: {
        type: Sequelize.INTEGER,
        references: { model: 'users', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },
      fk_grids: {
        type: Sequelize.INTEGER,
        references: { model: 'grids', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'CASCADE', // Quando arquivo for deletado ele vai setar como null
        allowNull: true,
      },

      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      default: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('options_grids');
  },
};

