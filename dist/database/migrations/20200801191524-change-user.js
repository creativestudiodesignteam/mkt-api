"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('users', 'name');
    queryInterface.removeColumn('users', 'cpf');
    return queryInterface.removeColumn('users', 'telephone');
  },

  down: queryInterface => {
    queryInterface.addColumn('users', 'name', {
      type: Sequelize.STRING,
      allowNull: false
    });
    queryInterface.addColumn('users', 'cpf', {
      type: Sequelize.STRING,
      allowNull: false
    });
    queryInterface.addColumn('users', 'telephone', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
};
