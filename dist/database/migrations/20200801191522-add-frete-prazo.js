"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.removeColumn('fretes', 'prazo_entrega');

    return queryInterface.addColumn('fretes', 'prazo', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('fretes', 'prazo');
  },
};
