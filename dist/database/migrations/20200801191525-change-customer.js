"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {

    queryInterface.addColumn('customers', 'name', {
      type: Sequelize.STRING,
      allowNull: false
    });
    queryInterface.addColumn('customers', 'cpf', {
      type: Sequelize.STRING,
      allowNull: false
    });
    return queryInterface.addColumn('customers', 'telephone', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: queryInterface => {
    queryInterface.removeColumn('customers', 'name');
    queryInterface.removeColumn('customers', 'cpf');
    return queryInterface.removeColumn('customers', 'telephone');
  },
};
