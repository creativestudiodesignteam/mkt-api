"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('sublevels', {
      /* name description */
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        defaultValue: true,
        allowNull: false,
      },
      fk_subcategories: {
        type: Sequelize.INTEGER,
        references: { model: 'subcategories', key: 'id' },
        onUpdate: 'CASCADE', // quando o arquivo for alterado aqui tbm vai alterar
        onDelete: 'SET NULL', // Quando arquivo for deletado ele vai setar como null
        allowNull: false,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('sublevels');
  },
};
