"use strict";module.exports = {
	up: (queryInterface, Sequelize) => {
		queryInterface.addColumn('users', 'attempts', {
			type: Sequelize.INTEGER,
			allowNull: true
		});

		return queryInterface.addColumn('users', 'timeout', {
			type: Sequelize.DATE,
			allowNull: true
		});
	},

	down: (queryInterface) => {
		queryInterface.removeColumn('users', 'attempts');

		return queryInterface.removeColumn('users', 'timeout');
	}
};
