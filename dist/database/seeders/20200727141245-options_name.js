"use strict";module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('options_names', [
      {
        name: '35',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '36',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '37',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '38',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '39',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '40',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '41',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '42',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '43',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        name: '44',
        type: 'sizes',
        fk_options:3,
        status: true,
        fk_users: null,
        default:true,
        created_at: new Date(),
        updated_at: new Date()
      }
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('options_names', null, {});
  }
};
//3
