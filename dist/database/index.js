"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _CreditCards = require('../app/models/CreditCards'); var _CreditCards2 = _interopRequireDefault(_CreditCards);

require('dotenv/config');
var _sequelize = require('sequelize'); var _sequelize2 = _interopRequireDefault(_sequelize);
var _mongoose = require('mongoose'); var _mongoose2 = _interopRequireDefault(_mongoose);
var _database = require('../config/database'); var _database2 = _interopRequireDefault(_database);

var _Users = require('../app/models/Users'); var _Users2 = _interopRequireDefault(_Users);
var _Files = require('../app/models/Files'); var _Files2 = _interopRequireDefault(_Files);
var _Addresses = require('../app/models/Addresses'); var _Addresses2 = _interopRequireDefault(_Addresses);
var _Providers = require('../app/models/Providers'); var _Providers2 = _interopRequireDefault(_Providers);
var _Customers = require('../app/models/Customers'); var _Customers2 = _interopRequireDefault(_Customers);
var _Products = require('../app/models/Products'); var _Products2 = _interopRequireDefault(_Products);
var _Categories = require('../app/models/Categories'); var _Categories2 = _interopRequireDefault(_Categories);
var _Subcategories = require('../app/models/Subcategories'); var _Subcategories2 = _interopRequireDefault(_Subcategories);
var _Pictures = require('../app/models/Pictures'); var _Pictures2 = _interopRequireDefault(_Pictures);
var _Orders = require('../app/models/Orders'); var _Orders2 = _interopRequireDefault(_Orders);
var _Details = require('../app/models/Details'); var _Details2 = _interopRequireDefault(_Details);
var _Options = require('../app/models/Options'); var _Options2 = _interopRequireDefault(_Options);
var _OptionsNames = require('../app/models/OptionsNames'); var _OptionsNames2 = _interopRequireDefault(_OptionsNames);
var _OptionsGrids = require('../app/models/OptionsGrids'); var _OptionsGrids2 = _interopRequireDefault(_OptionsGrids);
var _Grids = require('../app/models/Grids'); var _Grids2 = _interopRequireDefault(_Grids);
var _Galleries = require('../app/models/Galleries'); var _Galleries2 = _interopRequireDefault(_Galleries);
var _Carts = require('../app/models/Carts'); var _Carts2 = _interopRequireDefault(_Carts);
var _Prodsubcats = require('../app/models/Prodsubcats'); var _Prodsubcats2 = _interopRequireDefault(_Prodsubcats);
var _Datasheets = require('../app/models/Datasheets'); var _Datasheets2 = _interopRequireDefault(_Datasheets);
var _Sublevels = require('../app/models/Sublevels'); var _Sublevels2 = _interopRequireDefault(_Sublevels);
var _Levelsubcats = require('../app/models/Levelsubcats'); var _Levelsubcats2 = _interopRequireDefault(_Levelsubcats);
var _Payments = require('../app/models/Payments'); var _Payments2 = _interopRequireDefault(_Payments);
var _Sales = require('../app/models/Sales'); var _Sales2 = _interopRequireDefault(_Sales);
var _Fretes = require('../app/models/Fretes'); var _Fretes2 = _interopRequireDefault(_Fretes);
var _Responsibles = require('../app/models/Responsibles'); var _Responsibles2 = _interopRequireDefault(_Responsibles);
var _ProviderRamos = require('../app/models/ProviderRamos'); var _ProviderRamos2 = _interopRequireDefault(_ProviderRamos);
var _FormContacts = require('../app/models/FormContacts'); var _FormContacts2 = _interopRequireDefault(_FormContacts);



const models = [
    _Users2.default,
    _Files2.default,
    _Addresses2.default,
    _Providers2.default,
    _Customers2.default,
    _Products2.default,
    _Categories2.default,
    _Subcategories2.default,
    _Pictures2.default,
    _Orders2.default,
    _Details2.default,
    _Options2.default,
    _OptionsNames2.default,
    _OptionsGrids2.default,
    _Grids2.default,
    _Galleries2.default,
    _Carts2.default,
    _Prodsubcats2.default,
    _Datasheets2.default,
    _Sublevels2.default,
    _Levelsubcats2.default,
    _Payments2.default,
    _Sales2.default,
    _CreditCards2.default,
    _Fretes2.default,
    _Responsibles2.default,
    _ProviderRamos2.default,
    _FormContacts2.default
];


class Database {
    constructor() {
        this.init();
        this.mongo();
    }

    init() {
        this.connection = new (0, _sequelize2.default)(_database2.default);

        models
            .map(model => model.init(this.connection))
            .map(model => model.associate && model.associate(this.connection.models));
    }

    mongo() {
        this.mongoConnection = _mongoose2.default.connect(
            process.env.MONGO_URL, {
                useNewUrlParser: true,
                useFindAndModify: true,
                useUnifiedTopology: true,
            }
        );
    }
}

exports. default = new Database();