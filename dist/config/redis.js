"use strict";Object.defineProperty(exports, "__esModule", {value: true});require('dotenv')
exports. default = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
};
