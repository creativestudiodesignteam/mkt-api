"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _express = require('express');
var _multer = require('multer'); var _multer2 = _interopRequireDefault(_multer);
var _multer3 = require('./config/multer'); var _multer4 = _interopRequireDefault(_multer3);

var _SessionController = require('./app/controllers/SessionController'); var _SessionController2 = _interopRequireDefault(_SessionController);
var _auth = require('./app/middlewares/auth'); var _auth2 = _interopRequireDefault(_auth);
var _bullboard = require('bull-board'); var _bullboard2 = _interopRequireDefault(_bullboard);
var _Queue = require('./lib/Queue'); var _Queue2 = _interopRequireDefault(_Queue);
/* ==============================CONTROLLERS================================= */

var _AdressesController = require('./app/controllers/AdressesController'); var _AdressesController2 = _interopRequireDefault(_AdressesController);
var _UserController = require('./app/controllers/UserController'); var _UserController2 = _interopRequireDefault(_UserController);
var _FileController = require('./app/controllers/FileController'); var _FileController2 = _interopRequireDefault(_FileController);
var _ProvidersController = require('./app/controllers/ProvidersController'); var _ProvidersController2 = _interopRequireDefault(_ProvidersController);
var _CustomersController = require('./app/controllers/CustomersController'); var _CustomersController2 = _interopRequireDefault(_CustomersController);
var _ProductsController = require('./app/controllers/ProductsController'); var _ProductsController2 = _interopRequireDefault(_ProductsController);
var _CategoriesController = require('./app/controllers/CategoriesController'); var _CategoriesController2 = _interopRequireDefault(_CategoriesController);
var _SubCategoriesController = require('./app/controllers/SubCategoriesController'); var _SubCategoriesController2 = _interopRequireDefault(_SubCategoriesController);
var _GalleriesController = require('./app/controllers/GalleriesController'); var _GalleriesController2 = _interopRequireDefault(_GalleriesController);
var _DetailsController = require('./app/controllers/DetailsController'); var _DetailsController2 = _interopRequireDefault(_DetailsController);
var _CheckoutController = require('./app/controllers/CheckoutController'); var _CheckoutController2 = _interopRequireDefault(_CheckoutController);
var _OptionsControllers = require('./app/controllers/OptionsControllers'); var _OptionsControllers2 = _interopRequireDefault(_OptionsControllers);
var _OptionsNameController = require('./app/controllers/OptionsNameController'); var _OptionsNameController2 = _interopRequireDefault(_OptionsNameController);
var _GridsController = require('./app/controllers/GridsController'); var _GridsController2 = _interopRequireDefault(_GridsController);
var _OptionsGridsController = require('./app/controllers/OptionsGridsController'); var _OptionsGridsController2 = _interopRequireDefault(_OptionsGridsController);
var _CartsController = require('./app/controllers/CartsController'); var _CartsController2 = _interopRequireDefault(_CartsController);
var _ProdsubcatsController = require('./app/controllers/ProdsubcatsController'); var _ProdsubcatsController2 = _interopRequireDefault(_ProdsubcatsController);
var _DatasheetController = require('./app/controllers/DatasheetController'); var _DatasheetController2 = _interopRequireDefault(_DatasheetController);
var _ForgotPasswordController = require('./app/controllers/ForgotPasswordController'); var _ForgotPasswordController2 = _interopRequireDefault(_ForgotPasswordController);
var _Safe2PayController = require('./app/controllers/Safe2PayController'); var _Safe2PayController2 = _interopRequireDefault(_Safe2PayController);
var _SubAccountController = require('./app/controllers/SubAccountController'); var _SubAccountController2 = _interopRequireDefault(_SubAccountController);
var _SalesController = require('./app/controllers/SalesController'); var _SalesController2 = _interopRequireDefault(_SalesController);
var _ProviderSalesController = require('./app/controllers/ProviderSalesController'); var _ProviderSalesController2 = _interopRequireDefault(_ProviderSalesController);
var _CorreiosController = require('./app/controllers/CorreiosController'); var _CorreiosController2 = _interopRequireDefault(_CorreiosController);
var _SublevelsController = require('./app/controllers/SublevelsController'); var _SublevelsController2 = _interopRequireDefault(_SublevelsController);
var _LevelSubCatController = require('./app/controllers/LevelSubCatController'); var _LevelSubCatController2 = _interopRequireDefault(_LevelSubCatController);
var _HomeController = require('./app/controllers/HomeController'); var _HomeController2 = _interopRequireDefault(_HomeController);
var _SearchController = require('./app/controllers/SearchController'); var _SearchController2 = _interopRequireDefault(_SearchController);
var _FormContactController = require('./app/controllers/FormContactController'); var _FormContactController2 = _interopRequireDefault(_FormContactController);
var _BankController = require('./app/controllers/BankController'); var _BankController2 = _interopRequireDefault(_BankController);
var _CardsController = require('./app/controllers/CardsController'); var _CardsController2 = _interopRequireDefault(_CardsController);
var _TesteController = require('./app/controllers/TesteController'); var _TesteController2 = _interopRequireDefault(_TesteController);

const routes = new (0, _express.Router)();

_bullboard2.default.setQueues(_Queue2.default.queues.map(queue => queue.bull));
routes.use('/admin/queues', _bullboard2.default.UI)
/* ========== Config the multer for upload images ========== */
const upload = _multer2.default.call(void 0, _multer4.default);


routes.post('/files', upload.single('file'), _FileController2.default.store);

routes.post('/queue', _TesteController2.default.QueueTest);

routes.post('/forgot_password', _ForgotPasswordController2.default.store);
routes.put('/forgot_password/reset', _ForgotPasswordController2.default.update);
routes.get('/forgot_password/verify_token/:token', _ForgotPasswordController2.default.verify_token);

routes.put('/activate_user', _UserController2.default.activate);
routes.get('/search', _SearchController2.default.search);

routes.get('/bank/codes', _BankController2.default.bankCode);

/* ========== User Register and addresses ========== */
routes.post('/users', _UserController2.default.store);
routes.post('/check_email', _UserController2.default.checkEmail);
routes.post('/check_cnpj', _UserController2.default.checkCNPJ);
routes.post('/customers', _CustomersController2.default.store);
routes.post('/providers', _ProvidersController2.default.store);
routes.post('/addresses', _AdressesController2.default.store);

routes.get('/galleries/:fk_grids', _GalleriesController2.default.findGrid);


routes.get('/categories/all', _CategoriesController2.default.findSubcategoriesByCategories);

routes.get('/home/:fk_categories/:limit', _HomeController2.default.findProductByCategories);
routes.get('/products/categories/:fk_categories', _CategoriesController2.default.findProductByCategories);
routes.get('/v2/products/find/:id', _ProductsController2.default.findByIdV2);
routes.get('/v2/options/products/:fk_products', _OptionsControllers2.default.findByProducts);
routes.get('/products/find/:id', _ProductsController2.default.findById);

routes.get('/prodsubcats/:fk_subcategories', _ProdsubcatsController2.default.index);

routes.get('/subcategories/:fk_categories', _SubCategoriesController2.default.index);
routes.get('/sublevels/:fk_subcategories', _SublevelsController2.default.index);

routes.get('/products/related/:fk_products/:limit', _ProductsController2.default.related);
/* ========== Testing if the user is logged in ========== */
routes.post('/contact', _FormContactController2.default.store);

routes.get('/categories', _CategoriesController2.default.index);
routes.get('/details/:id', _DetailsController2.default.index);

/* ========== Session authentication JWT ========== */
routes.post('/auth', _SessionController2.default.store);

routes.use(_auth2.default);

routes.put('/users', _UserController2.default.update);
routes.get('/users', _UserController2.default.index);
routes.get('/activate_user/resend', _UserController2.default.resendTokenActivate);

routes.post('/subaccount', _SubAccountController2.default.store);

routes.get('/providers', _ProvidersController2.default.index);
routes.post('/providers', _ProvidersController2.default.store);
routes.put('/providers/:id', _ProvidersController2.default.update);
routes.delete('/providers/:id', _ProvidersController2.default.delete);

routes.get('/addresses', _AdressesController2.default.index);
routes.get('/addresses/select', _AdressesController2.default.findBySelect);
routes.put('/addresses/select', _AdressesController2.default.selectUpdate);
routes.put('/addresses/:id', _AdressesController2.default.update);
routes.delete('/addresses/:id', _AdressesController2.default.delete);

routes.get('/customers', _CustomersController2.default.index);
routes.put('/customers/:id', _CustomersController2.default.update);
routes.delete('/customers/:id', _CustomersController2.default.delete);

/* routes.post('/products/pictures/:fk_grids', upload.array('files'), PicturesController.store); */

routes.get('/products', _ProductsController2.default.index);
routes.get('/products/:id', _ProductsController2.default.findByIdReqUser);
routes.post('/products', _ProductsController2.default.store);
routes.put('/products/:id', _ProductsController2.default.update);
routes.delete('/products/:id', _ProductsController2.default.delete);

routes.post('/gallery', upload.array('files'), _GalleriesController2.default.store);

routes.put('/gallery/:id', upload.single('file'), _GalleriesController2.default.update);

routes.delete('/gallery/:id', _GalleriesController2.default.delete);


routes.post('/categories', _CategoriesController2.default.store);
routes.put('/categories/:id', _CategoriesController2.default.update);
routes.delete('/categories/:id', _CategoriesController2.default.delete);


routes.post('/subcategories', _SubCategoriesController2.default.store);
routes.put('/subcategories/:id', _SubCategoriesController2.default.update);
routes.delete('/subcategories/:id', _SubCategoriesController2.default.delete);


routes.post('/sublevels', _SublevelsController2.default.store);
routes.put('/sublevels/:id', _SublevelsController2.default.update);
routes.delete('/sublevels/:id', _SublevelsController2.default.delete);

routes.post('/sublevels', _SublevelsController2.default.store);
routes.put('/sublevels/:id', _SublevelsController2.default.update);
routes.delete('/sublevels/:id', _SublevelsController2.default.delete);


routes.post('/prodsubcats', _ProdsubcatsController2.default.store);
routes.put('/prodsubcats/:id', _ProdsubcatsController2.default.update);
routes.delete('/prodsubcats/:id', _ProdsubcatsController2.default.delete);

routes.get('/levelprodsubcat/:fk_prodsubcat', _LevelSubCatController2.default.index);
routes.post('/levelprodsubcat', _LevelSubCatController2.default.store);
routes.put('/levelprodsubcat/:id', _LevelSubCatController2.default.update);
routes.delete('/levelprodsubcat/:id', _LevelSubCatController2.default.delete);


routes.get('/options', _OptionsControllers2.default.index);
routes.post('/options', _OptionsControllers2.default.store);
routes.put('/options/:id', _OptionsControllers2.default.update);
routes.delete('/options/:id', _OptionsControllers2.default.delete);

routes.get('/options_name/:fk_options', _OptionsNameController2.default.index);
routes.post('/options_name', _OptionsNameController2.default.store);
routes.put('/options_name/:id', _OptionsNameController2.default.update);
routes.delete('/options_name/:id', _OptionsNameController2.default.delete);

routes.get('/options_grids/:fk_grids', _OptionsGridsController2.default.index);
routes.post('/options_grids', _OptionsGridsController2.default.store);
routes.put('/options_grids/:id', _OptionsGridsController2.default.update);
routes.delete('/options_grids/:id', _OptionsGridsController2.default.delete);

routes.get('/grids/:fk_products', _GridsController2.default.index);
routes.post('/grids', _GridsController2.default.store);
routes.put('/grids/:id', _GridsController2.default.update);
routes.delete('/grids/:id', _GridsController2.default.delete);

routes.post('/details', _DetailsController2.default.store);
routes.put('/details/:id', _DetailsController2.default.update);
routes.delete('/details/:id', _DetailsController2.default.delete);

routes.post('/checkout', _CheckoutController2.default.store);

routes.get('/carts', _CartsController2.default.index);
routes.post('/carts', _CartsController2.default.store);
routes.put('/carts/:id', _CartsController2.default.update);
routes.delete('/carts/:id', _CartsController2.default.delete);

routes.get('/datasheet/:fk_products', _DatasheetController2.default.index);
routes.post('/datasheet/:fk_products', _DatasheetController2.default.store);
routes.put('/datasheet/:id', _DatasheetController2.default.update);
routes.delete('/datasheet/:id', _DatasheetController2.default.delete);

routes.post('/safe2pay/webhook', _Safe2PayController2.default.webhook);

routes.get('/creditcard', _CardsController2.default.index);
routes.post('/creditcard', _CardsController2.default.post);
routes.delete('/creditcard/:id', _CardsController2.default.delete);
routes.put('/creditcard/select', _CardsController2.default.cardSelectedUpdate);
routes.get('/installments', _CardsController2.default.installments)

routes.get('/sales/detail/:id', _SalesController2.default.detail);
routes.get('/sales/provider', _SalesController2.default.index);
routes.get('/sales/purchase', _SalesController2.default.indexSales);
routes.delete('/sales/provider/:hash', _SalesController2.default.delete);


routes.get('/buscacep/:cep', _CorreiosController2.default.buscaCep);
routes.get('/calculo_frete/:type', _CorreiosController2.default.calculoFrete);
routes.get('/calculo_prazo_produto/:type', _CorreiosController2.default.calculoPrazoProduto);
routes.post('/salva_frete', _CorreiosController2.default.storeFrete);

routes.get('/provider_sales/:fk_provider', _ProviderSalesController2.default.index);
routes.get('/provider_sales/:hash', _ProviderSalesController2.default.detail);
routes.delete('/provider_sales/:hash', _ProviderSalesController2.default.delete);

routes.get('/search/provider', _SearchController2.default.SearchMyProducts);



exports. default = routes;
