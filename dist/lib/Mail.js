"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }var _nodemailer = require('nodemailer'); var _nodemailer2 = _interopRequireDefault(_nodemailer);
var _path = require('path');
var _expresshandlebars = require('express-handlebars'); var _expresshandlebars2 = _interopRequireDefault(_expresshandlebars);
var _nodemailerexpresshandlebars = require('nodemailer-express-handlebars'); var _nodemailerexpresshandlebars2 = _interopRequireDefault(_nodemailerexpresshandlebars);

var _mailer = require('../config/mailer'); var _mailer2 = _interopRequireDefault(_mailer);

class Mail {
  constructor() {
    const { host, port, secure, auth } = _mailer2.default;

    this.transporter = _nodemailer2.default.createTransport({
      host,
      port,
      secure,
      auth: auth.user ? auth : null,
    });
    this.configureTemplates();
  }

  configureTemplates() {
    const viewPath = _path.resolve.call(void 0, __dirname, '..', 'app', 'views', 'emails');

    const path = `${__dirname.split('dist')[0]}/x/src/common/templates`;

    this.transporter.use(
      'compile',
      _nodemailerexpresshandlebars2.default.call(void 0, {
        viewEngine: _expresshandlebars2.default.create({
          layoutsDir: _path.resolve.call(void 0, viewPath, 'layouts'),
          partialsDir: _path.resolve.call(void 0, viewPath, 'partials'),
          defaultLayout: 'default',
          extname: '.hbs',
        }),
        viewPath,
        extname: '.hbs',
      })
    );
  }

  sendMail(message) {
    return this.transporter.sendMail({
      ..._mailer2.default.default,
      ...message,
    });
  }
}
exports. default = new Mail();
